package se.proxus.events.server;

import se.proxus.events.*;
import net.minecraft.src.*;

public class EventChatSend extends Event {
	
	protected String message;

	public EventChatSend(String message) {
		this.setMessage(message);
	}
	
	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}