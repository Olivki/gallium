package se.proxus.events.player;

import se.proxus.events.*;

public class EventPostUpdate extends Event {
	
	public long getCurrentMilliseconds() {
		return this.getNanoTime() / 1000000;
	}
	
	public long getNanoTime() {
		return System.nanoTime();
	}
}