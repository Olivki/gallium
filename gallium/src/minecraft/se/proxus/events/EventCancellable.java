package se.proxus.events;

import java.util.logging.Level;

import se.proxus.utils.Wrapper;


public class EventCancellable extends Event {
	
	private boolean cancelled;

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
}