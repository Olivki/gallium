package se.proxus.events.render;

import se.proxus.events.*;
import net.minecraft.src.*;

public class EventRenderPlayer extends Event {
	
	protected double x;
	
	protected double y;
	
	protected double z;
	
	protected EntityPlayer player;
	
	protected RenderPlayer render;
	
	protected boolean renderNormal;
	
	public EventRenderPlayer(double x, double y, double z, EntityPlayer player, RenderPlayer render, boolean renderNormal) {
		setX(x);
		setY(y);
		setZ(z);
		setPlayer(player);
		setRender(render);
		setRenderNormal(renderNormal);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public EntityPlayer getPlayer() {
		return player;
	}

	public void setPlayer(EntityPlayer player) {
		this.player = player;
	}

	public RenderPlayer getRender() {
		return render;
	}

	public void setRender(RenderPlayer render) {
		this.render = render;
	}

	public boolean isRenderNormal() {
		return renderNormal;
	}

	public void setRenderNormal(boolean renderNormal) {
		this.renderNormal = renderNormal;
	}
}