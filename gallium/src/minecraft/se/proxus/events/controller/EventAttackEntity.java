package se.proxus.events.controller;

import se.proxus.events.*;
import net.minecraft.src.Entity;


public class EventAttackEntity extends EventCancellable {
	
	private Entity target;
	
	public EventAttackEntity(Entity target) {
		setTarget(target);
	}

	public Entity getTarget() {
		return target;
	}

	public void setTarget(Entity target) {
		this.target = target;
	}
}