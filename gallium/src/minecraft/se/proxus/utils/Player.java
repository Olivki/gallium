package se.proxus.utils;

import se.proxus.mods.list.none.ModFriend;
import se.proxus.screens.GuiConsole;

import net.minecraft.src.*;

public class Player {

	private static Player instance;

	private static EntityLiving target;

	public static double getDistanceToEntity(Entity e) {
		return Math.sqrt((Client.getPlayer().posX - e.posX) * (Client.getPlayer().posX - e.posX) 
				+ (Client.getPlayer().posZ - e.posZ) * (Client.getPlayer().posZ - e.posZ));
	}

	public static double getX() {
		return Client.getPlayer().posX;
	}

	public static void setX(double x) {
		Client.getPlayer().posX = x;
	}

	public static double getY() {
		return Client.getPlayer().posY;
	}

	public static void setY(double y) {
		Client.getPlayer().posY = y;
	}

	public static double getZ() {
		return Client.getPlayer().posZ;
	}

	public static void setZ(double z) {
		Client.getPlayer().posZ = z;
	}

	public static float getJumpMovementFactor() {
		return Client.getPlayer().jumpMovementFactor;
	}

	public static void setJumpMovementFactor(float jumpMovementFactor) {
		Client.getPlayer().jumpMovementFactor = jumpMovementFactor;
	}

	public static boolean isSprinting() {
		return Client.getPlayer().isSprinting();
	}

	public static void setSprinting(boolean sprinting) {
		Client.getPlayer().setSprinting(sprinting);
	}

	public boolean getVelocityChanged() {
		return Client.getPlayer().velocityChanged;
	}

	public static void setVelocityChanged(boolean state) {
		Client.getPlayer().velocityChanged = state;
	}

	public static double getMotionX() {
		return Client.getPlayer().motionX;
	}

	public static void setMotionX(double x) {
		Client.getPlayer().motionX = x;
	}

	public static double getMotionY() {
		return Client.getPlayer().motionY;
	}

	public static void setMotionY(double y) {
		Client.getPlayer().motionY = y;
	}

	public static double getMotionZ() {
		return Client.getPlayer().motionZ;
	}

	public static void setMotionZ(double z) {
		Client.getPlayer().motionZ = z;
	}

	public static float getRotationPitch() {
		return Client.getPlayer().rotationPitch;
	}

	public static void setRotationPitch(float pitch) {
		Client.getPlayer().rotationPitch = pitch;
	}

	public static float getRotationYaw() {
		return Client.getPlayer().rotationYaw;
	}

	public static void setRotationYaw(float yaw) {
		Client.getPlayer().rotationYaw = yaw;
	}

	public static float getRotationYawHead() {
		return Client.getPlayer().rotationYawHead;
	}

	public static void setRotationYawHead(float yawHead) {
		Client.getPlayer().rotationYawHead = yawHead;
	}

	/**
	 * @author Squirrel
	 **/
	public static float getAttackRotationPitch(EntityLiving entity) {
		return (float)((Math.atan2(getDistanceToEntity(entity), entity.posY + 0.8D - getY() + 1) * 180D) 
				/ Math.PI) - 90F;
	}

	/**
	 * @author Squirrel
	 **/
	public static float getAttackRotationYaw(EntityLiving entity) {
		double xDistance = entity.posX - getX();
		double zDistance = entity.posZ - getZ();

		return (float)((Math.atan2(zDistance, xDistance) * 180D) / Math.PI) - 90F;
	}

	public static boolean isOnGround() {
		return Client.getPlayer().onGround;
	}

	public static void setOnGround(boolean onGround) {
		Client.getPlayer().onGround = onGround;
	}

	public static boolean isCollidedHorizontally() {
		return Client.getPlayer().isCollidedHorizontally;
	}

	public static boolean isCollidedVertically() {
		return Client.getPlayer().isCollidedVertically;
	}

	public static String getUsername() {
		return Client.getMinecraft().session.username;
	}

	public static void setUsername(String username) {
		Client.getMinecraft().session.username = username;
	}

	public static void setPosition(double x, double y, double z) {
		Client.getPlayer().setPosition(x, y, z);
	}

	public static void setPositionAndRotation(double x, double y, double z, float yaw, float pitch) {
		Client.getPlayer().setPositionAndRotation(x, y, z, yaw, pitch);
	}

	public static void addMessage(String message) {
		Client.getPlayer().addChatMessage("[" + Colours.DARK_AQUA+ "Gallium" + Colours.RESET + "]: " + message);
	}

	public static void addSettingsMessage(String name, String value) {
		addMessage(Colours.DARK_AQUA + name + Colours.RESET + "has been set to " + Colours.DARK_AQUA + value + Colours.RESET + ".");
	}
	
	public static void sendMessage(String message) {
		Client.sendPacket(new Packet3Chat(message));
	}

	public static void attackEntity(Entity entity) {
		Client.getController().attackEntity(Client.getPlayer(), entity);
	}

	public static boolean isInWater() {
		return Client.getPlayer().isInWater();
	}

	public static EntityLiving getTarget() {
		return target;
	}

	public static void setTarget(EntityLiving _target) {
		target = _target;
	}
	
	public static boolean isSwinging() {
		return Client.getPlayer().isSwingInProgress;
	}
	
	public static void swingItem() {
		Client.getPlayer().swingItem();
	}
	
	public static boolean isJumping() {
		return Client.getPlayer().isJumping;
	}

	/**
	 * @author Ownage
	 * @return The nearest entity.
	 */
	public static EntityPlayer getNearestPlayer() {
		EntityPlayer nearest = null;

		for(Object entity : Client.getWorld().playerEntities) {
			if(entity != null && !(entity instanceof EntityPlayerSP)) {
				EntityPlayer e = (EntityPlayer)entity;

				if(!(e.isDead)) {
					if(nearest == null) {
						nearest = e;
					} else if(getDistanceToEntity(nearest) > getDistanceToEntity(e)) {
						nearest = e;
					}
				}
			}
		}

		return nearest;
	}

	/**
	 * @author Ownage
	 * @return The nearest entity.
	 */
	public static EntityLiving getNearestEntity() {
		EntityLiving nearest = null;

		for(Object entity : Client.getWorld().loadedEntityList) {
			if(entity != null && !(entity instanceof EntityPlayerSP) && entity instanceof EntityLiving) {
				EntityLiving e = (EntityLiving)entity;

				if(!(e.isDead)) {
					if(nearest == null) {
						nearest = e;
					} else if(getDistanceToEntity(nearest) > getDistanceToEntity(e)) {
						nearest = e;
					}
				}
			}
		}

		return nearest;
	}

	public static boolean getTargetChecks(EntityLiving entity) {
		return !(entity.isDead) 
				&& Client.getPlayer().canEntityBeSeen(entity)
				&& getDistanceToEntity(entity) <= 3.5F
				&& entity.ticksExisted > 100
				&& !(((ModFriend)Wrapper.getGallium().getMods().getMod("Friend")).isFriend(entity.getEntityName()))
				/*&& !(entity.posY > Client.getPlayer().boundingBox.maxY + 0.5D)*/;
	}
	
	public static int getDirection() {
		return (int)MathHelper.floor_double((double)(getRotationYaw() * 4.0F / 360.0F) + 0.5D) & 3;
	}

	public static Player getInstance() {
		if(instance == null) {
			instance = new Player();
		}

		return instance;
	}
}