package se.proxus.utils;

import java.awt.Color;

import net.minecraft.src.EnumOS;

public class Colours {
	
	/** FIX FOR MAC USERS **/
	public static final char COLOUR_SYMBOL = Client.getMinecraft().getOs() == EnumOS.MACOS ? '\u00A7' : '�';
	
	/** Code: 0 **/
	public static final String BLACK = COLOUR_SYMBOL + "0";
	
	/** Code: 1 **/
	public static final String DARK_BLUE = COLOUR_SYMBOL + "1";
	
	/** Code: 2 **/
	public static final String DARK_GREEN = COLOUR_SYMBOL + "2";
	
	/** Code: 3 **/
	public static final String DARK_AQUA = COLOUR_SYMBOL + "3";
	
	/** Code: 4 **/
	public static final String DARK_RED = COLOUR_SYMBOL + "4";
	
	/** Code: 5 **/
	public static final String PURPLE = COLOUR_SYMBOL + "5";
	
	/** Code: 6 **/
	public static final String GOLD = COLOUR_SYMBOL + "6";
	
	/** Code: 7 **/
	public static final String GREY = COLOUR_SYMBOL + "7";
	
	/** Code: 8 **/
	public static final String DARK_GREY = COLOUR_SYMBOL + "8";
	
	/** Code: 9 **/
	public static final String INDIGO = COLOUR_SYMBOL + "9";
	
	/** Code: a **/
	public static final String BRIGHT_GREEN = COLOUR_SYMBOL + "a";
	
	/** Code: b **/
	public static final String AQUA = COLOUR_SYMBOL + "b";
	
	/** Code: c **/
	public static final String RED = COLOUR_SYMBOL + "c";
	
	/** Code: d **/
	public static final String PINK = COLOUR_SYMBOL + "d";
	
	/** Code: e **/
	public static final String YELLOW = COLOUR_SYMBOL + "e";
	
	/** Code: f **/
	public static final String WHITE = COLOUR_SYMBOL + "f";
	
	/** Code: k **/
	public static final String RANDOM = COLOUR_SYMBOL + "k";
	
	/** Code: l **/
	public static final String BOLD = COLOUR_SYMBOL + "l";
	
	/** Code: m **/
	public static final String STRIKE = COLOUR_SYMBOL + "m";
	
	/** Code: n **/
	public static final String UNDERLINE = COLOUR_SYMBOL + "n";
	
	/** Code: o **/
	public static final String ITALIC = COLOUR_SYMBOL + "o";
	
	/** Code: r **/
	public static final String RESET = COLOUR_SYMBOL + "r";
	
	public static int getHEX(int r, int g, int b, int a) {
		return (new Color(r, g, b, a)).getRGB() + (new Color(r, g, b, a)).getAlpha();
	}
	
	public static float[] getRGBA(int var1) {
		return (new float[] {
				(float)(var1 >> 16 & 255) / 255.0F,
				(float)(var1 >> 8 & 255) / 255.0F,
				(float)(var1 & 255) / 255.0F,
				(float)(var1 >> 24 & 255) / 255.0F,
				(float)(var1 >> 24 & 0xFF) / 255F
		});
	}
	
	public static char getCode(int index) {
		switch(index) {
		case 0:
			return '0';
			
		case 1:
			return '1';
			
		case 2:
			return '2';
			
		case 3:
			return '3';
			
		case 4:
			return '4';
			
		case 5:
			return '5';
			
		case 6:
			return '6';
			
		case 7:
			return '7';
			
		case 8:
			return '8';
			
		case 9:
			return '9';
			
		case 10:
			return 'a';
			
		case 11:
			return 'b';
			
		case 12:
			return 'c';
			
		case 13:
			return 'd';
			
		case 14:
			return 'e';
			
		case 15:
			return 'f';
			
		case 16:
			return 'k';
			
		case 17:
			return 'l';
			
		case 18:
			return 'm';
			
		case 19:
			return 'n';
			
		case 20:
			return 'o';
			
		case 21:
			return 'r';
			
		default:
			return 'f';
		}
	}
	
    public static int getHex(float a, float r, float g, float b) {
        return ((int)a << 24) + ((int)r << 16) + ((int)g << 8) + (int)b;
    }
    
    public static int getHex(int a, int r, int g, int b) {
        return ((int)a << 24) + ((int)r << 16) + ((int)g << 8) + (int)b;
    }
}