package se.proxus;

import java.awt.Font;
import java.io.File;
import java.util.logging.*;

import se.proxus.betterfonts.StringCache;
import se.proxus.mods.*;
import se.proxus.panels.*;
import se.proxus.utils.*;


public class Gallium {
	
	/**
	 * 20 ticks ~ 1 second
	 **/
	
	public Logger logger;
	
	public ModHandler mods;
	
	public PanelHandler panels;
	
	public File dirMods;
	
	public StringCache font;
	
	public StringCache fontPanel;
	
	public StringCache fontChat;
	
	private static Gallium instance;
	
	public Gallium() {
		instance = this;
		
		onStart();
	}
	
	public void onStart() {
		setLogger(Logger.getLogger("Gallium"));
		getLogger().log(Level.INFO, "Gallium has been started!");
		createFolder(new File(Client.getMinecraft().getMinecraftDir(), "/gallium/"));
		dirMods = createFolder(new File(Client.getMinecraft().getMinecraftDir(), "/gallium/mods/"));
		
		setMods(new ModHandler());
		getMods().initModHandler();
		
		setPanels(new PanelHandler());
		getPanels().initPanelHandler();
	}
	
	public void onMainMenuOpened() {
		if(getFont() == null) {
			setFont(new StringCache(Client.getFontRenderer().getColorCode()));
			getFont().setDefaultFont("DIN 1451 Mittelschrift Normal", 18, true);
			setFontPanel(new StringCache(Client.getFontRenderer().getColorCode()));
			getFontPanel().setDefaultFont("Tahoma", 18, true);
			setFontChat(new StringCache(Client.getFontRenderer().getColorCode()));
			getFontChat().setDefaultFont("Verdana Bold", 17, true);
		}
	}
	
	public File createFolder(File file) {
		file.mkdirs();
		
		getLogger().log(Level.INFO, "Created the file: " + file.getName() + " at: " + file.getAbsolutePath() + "!");
		
		return file;
	}

	public Logger getLogger() {
		return Logger.getLogger("Gallium");
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public final ModHandler getMods() {
		return mods;
	}

	public void setMods(ModHandler mods) {
		this.mods = mods;
	}
	
	public PanelHandler getPanels() {
		return panels;
	}

	public void setPanels(PanelHandler panels) {
		this.panels = panels;
	}

	public File getModsDirectory() {
		return dirMods;
	}
	
	public File getDirMods() {
		return dirMods;
	}

	public void setDirMods(File dirMods) {
		this.dirMods = dirMods;
	}

	public StringCache getFont() {
		return font;
	}

	public void setFont(StringCache font) {
		this.font = font;
	}
	
	public StringCache getFontPanel() {
		return fontPanel;
	}

	public void setFontPanel(StringCache fontPanel) {
		this.fontPanel = fontPanel;
	}

	public StringCache getFontChat() {
		return fontChat;
	}

	public void setFontChat(StringCache fontChat) {
		this.fontChat = fontChat;
	}

	public static Gallium getInstance() {
		if(instance == null) {
			instance = new Gallium();
		}
		
		return instance;
	}
}