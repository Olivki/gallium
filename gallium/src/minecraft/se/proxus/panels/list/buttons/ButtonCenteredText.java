package se.proxus.panels.list.buttons;

import se.proxus.mods.*;
import se.proxus.panels.*;
import se.proxus.utils.Client;
import se.proxus.utils.Colours;
import se.proxus.utils.Wrapper;

public class ButtonCenteredText extends ButtonBase {

	public ButtonCenteredText(String name, int width, int height, PanelBase panel) {
		super(name, 0, 0, width, height, panel);
	}

	@Override
	public void draw(int x, int y) {
		Client.getIngameGui().drawCenteredStringPanel(Client.getFontRenderer(), getName(), getX() + getWidth() / 2, getY() + 2.5F, 0xFFFFFFFF);
	}

	@Override
	public void drawTag(int x, int y) {
		
	}
	
	@Override
	public void mouseClicked(int x, int y, int type) {
		
	}
}