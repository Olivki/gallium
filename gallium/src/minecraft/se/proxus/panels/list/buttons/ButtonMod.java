package se.proxus.panels.list.buttons;

import se.proxus.mods.*;
import se.proxus.panels.*;
import se.proxus.utils.Client;
import se.proxus.utils.Colours;
import se.proxus.utils.Wrapper;

public class ButtonMod extends ButtonBase {

	private ModBase mod;

	public ButtonMod(String name, int width, int height, PanelBase panel, ModBase mod) {
		super(name, 0, 0, width, height, panel);
		setMod(mod);
	}

	@Override
	public void draw(int x, int y) {
		drawGradientBorderedRect(getX(), getY(), getX() + getWidth(), getY() + getHeight(), 0xFFA1A1A1, 
				(getMod().getState() ? 0xFF5FF569 : 0xFFE60000), (getMod().getState() ? 0xFF42AC4A : 0xFF930000));
		Client.getIngameGui().drawCenteredStringPanel(Client.getFontRenderer(), getMod().getName(), getX() + getWidth() / 2, 
				getY() + 2.5F, (getMod().getState() ? 0xFF474747 : 0xFFB7B7B7));
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		if(isHovering(x, y) && type == 0) {
			getMod().toggle();
			Client.playSound(1.0F);
		}
	}

	public ModBase getMod() {
		return mod;
	}

	public void setMod(ModBase mod) {
		this.mod = mod;
	}

	@Override
	public boolean isHovering(int x, int y) {
		return x >= getX() && y >= getY() && x <= getX() + getWidth() && y <= getY() + getHeight();
	}
}