package se.proxus.panels.list.buttons;

import org.lwjgl.input.Mouse;

import se.proxus.mods.*;
import se.proxus.panels.*;
import se.proxus.utils.Client;
import se.proxus.utils.Colours;
import se.proxus.utils.Wrapper;


public class ButtonSlider extends ButtonBase {
	
	protected String value;
	
	protected double max;
	
	protected double amount;
	
	protected int id;
	
	protected boolean dragging;
	
	protected ModBase mod;

	public ButtonSlider(String name, int width, int height, PanelBase panel, ModBase mod, double max, int id) {
		super(name, 0, 0, width, height, panel);
		setMod(mod);
		setMax(max);
		setId(id);
	}

	@Override
	public void draw(int x, int y) {
		handleValues();
		
		drawBorderedRect(getX(), getY(), getX() + getWidth(), getY() + getHeight(), 
				Colours.getHex(((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(0)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(1)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(2)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(3)).intValue()),
				Colours.getHex(((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(4)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(5)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(6)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(7)).intValue()));
		
		float sliderX = (int)(getAmount() * (float)getWidth()) - 1.5F;
		
		drawBorderedRect(getX() + sliderX - 3.0F, getY(), getX() + sliderX + 3.0F, getY() + getHeight(), 
				Colours.getHex(((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(0)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(1)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(2)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(3)).intValue()),
				Colours.getHex(((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(4)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(5)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(6)).intValue(), 
						((Integer)Wrapper.getGallium().mods.getMod("Colours").getSetting(7)).intValue()));
		
		Client.getIngameGui().drawCenteredStringS(Client.getFontRenderer(), getName() + ": " + getValue(), getX() + getWidth() / 2, 
				getY() + 2.5F, 0xFFFFFFFF);
		
		mouseDragged(x, y);
	}
	
	@Override
	public void mouseClicked(int x, int y, int type) {
		if(isHovering(x, y) && type == 0) {
			Client.playSound(1.0F);
			setAmount((float)(((double)x - (double)getX()) / (double)getWidth()));
			setAmtInOptions(Double.valueOf((double)getAmount() * getMax()));
			setDragging(true);
		} else {
			setDragging(false);
		}
	}
	
	public void mouseDragged(int x, int y) {
		if(Mouse.isButtonDown(0) && isDragging()) {
			setAmount((float)(((double)x - (double)getX()) / (double)getWidth()));

			if(x < getX() - 2) {
				setAmount(0.0F);
			}  if(x > getX() + getWidth()) {
				setAmount(1.0F);
			}
			
			setAmtInOptions(Double.valueOf((double)getAmount() * getMax()));
		} else {
			setDragging(false);
		}
	}
	
	public void handleValues() {
		if(mod.getSetting(getId()) instanceof Double) {
			setValue("" + ((int)Math.floor(getDoubleFromOptions() * 10D) / 10D));
		} else if(mod.getSetting(getId()) instanceof Float) {
			setValue("" + ((int)Math.floor(getFloatFromOptions() * 10F) / 10D));
		} else if(mod.getSetting(getId()) instanceof Integer) {
			setValue("" + ((int)Math.floor(getIntegerFromOptions() * 10) / 10D));
		} else if(mod.getSetting(getId()) instanceof Long) {
			setValue("" + ((int)Math.floor(getLongFromOptions() * 10L) / 10D));
		}

		if(mod.getSetting(getId()) instanceof Double) {
			setAmount((float)(getDoubleFromOptions() / getMax()));
		} else if(this.mod.getSetting(getId()) instanceof Float) {
			setAmount((float)((double)getFloatFromOptions() / getMax()));
		} else if(this.mod.getSetting(getId()) instanceof Integer) {
			setAmount((float)((double)getIntegerFromOptions() / getMax()));
		} else if(this.mod.getSetting(getId()) instanceof Long) {
			setAmount((float)((double)getLongFromOptions() / getMax()));
		}
	}
	
	public double getDoubleFromOptions() {
		try {
			return ((Double)mod.getSetting(getId())).doubleValue();
		} catch (Exception exception) {
			return 1.0D;
		}
	}

	public float getFloatFromOptions() {
		try {
			return ((Float)mod.getSetting(getId())).floatValue();
		} catch (Exception exception) {
			return 1.0F;
		}
	}

	public int getIntegerFromOptions() {
		try {
			return ((Integer)mod.getSetting(getId())).intValue();
		} catch (Exception exception) {
			return 1;
		}
	}

	public long getLongFromOptions() {
		try {
			return ((Long)mod.getSetting(getId())).longValue();
		} catch (Exception exception) {
			return 1L;
		}
	}

	public void setAmtInOptions(Object obj) {
		try {
			if(mod.getSetting(getId()) instanceof Double) {
				mod.registerSetting(getId(), (Double)obj, true);
			} else if(mod.getSetting(getId()) instanceof Float) {
				mod.registerSetting(getId(), Float.valueOf(((Double)obj).floatValue()), true);
			} else if(mod.getSetting(getId()) instanceof Integer) {
				mod.registerSetting(getId(), Integer.valueOf(((Double)obj).intValue()), true);
			} else if(mod.getSetting(getId()) instanceof Long) {
				mod.registerSetting(getId(), Long.valueOf(((Double)obj).longValue()), true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDragging() {
		return dragging;
	}

	public void setDragging(boolean dragging) {
		this.dragging = dragging;
	}

	public ModBase getMod() {
		return mod;
	}

	public void setMod(ModBase mod) {
		this.mod = mod;
	}
}