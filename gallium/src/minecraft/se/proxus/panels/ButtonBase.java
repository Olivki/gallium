package se.proxus.panels;

import org.lwjgl.opengl.GL11;

import se.proxus.utils.Client;
import se.proxus.utils.Wrapper;


public abstract class ButtonBase {
	
	protected String name;
	
	protected int x;
	
	protected int y;
	
	protected int width;
	
	protected int height;
	
	protected boolean state;
	
	protected PanelBase panel;
	
	public ButtonBase(String name, int x, int y, int width, int height, PanelBase panel) {
		setName(name);
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
		setPanel(panel);
	}

	public abstract void draw(int x, int y);
	
	public void drawTag(int x, int y) {};
	
	public void mouseClickedBefore(int x, int y, int type) {
		if(isHovering(x, y)) {
			Wrapper.getGallium().getPanels().setCurrentPanel(getPanel(), true);
			mouseClicked(x, y, type);
		}
	}
	
	public abstract void mouseClicked(int x, int y, int type);
	
	public boolean isHovering(int x, int y) {
		return x >= getX() && y >= getY() && x <= getX() + getWidth() && y <= getY() + getHeight();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public PanelBase getPanel() {
		return panel;
	}

	public void setPanel(PanelBase panel) {
		this.panel = panel;
	}
	
	public void drawBorderedRect(float x, float y, float width, float height, int hex1, int hex2) {
		Client.getIngameGui().drawRect(x + 1, y + 1, width - 1, height - 1, hex2);
		Client.getIngameGui().drawVerticalLine(x, y, height - 1, hex1);
		Client.getIngameGui().drawVerticalLine(width - 1, y, height - 1, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, y, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, height - 1, hex1);
	}
	
	public void drawGradientBorderedRect(float x, float y, float width, float height, int hex1, int hex2, int hex3) {
		Client.getIngameGui().drawGradientRect(x + 1, y + 1, width - 1, height - 1, hex2, hex3);
		Client.getIngameGui().drawVerticalLine(x, y, height - 1, hex1);
		Client.getIngameGui().drawVerticalLine(width - 1, y, height - 1, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, y, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, height - 1, hex1);
	}
	
	public void drawBorderedRect(float x, float y, float width, float height, float lineWidth, int hex1, int hex2) {
		Client.getIngameGui().drawRect(x, y, width, height, hex2);
		float f = (float)(hex1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(hex1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(hex1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(hex1 & 0xFF) / 255F;
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(lineWidth);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, height);
		GL11.glVertex2d(width, height);
		GL11.glVertex2d(width, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(width, y);
		GL11.glVertex2d(x, height);
		GL11.glVertex2d(width, height);
		GL11.glEnd();
		GL11.glPopMatrix();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}

	public void drawHollowBorderedRect(int x, int y, int width, int height, float lineWidth, int hex) {
		float f = (float)(hex >> 24 & 0xFF) / 255F;
		float f1 = (float)(hex >> 16 & 0xFF) / 255F;
		float f2 = (float)(hex >> 8 & 0xFF) / 255F;
		float f3 = (float)(hex & 0xFF) / 255F;
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(lineWidth);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, height);
		GL11.glVertex2d(width, height);
		GL11.glVertex2d(width, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(width, y);
		GL11.glVertex2d(x, height);
		GL11.glVertex2d(width, height);
		GL11.glEnd();
		GL11.glPopMatrix();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}
}