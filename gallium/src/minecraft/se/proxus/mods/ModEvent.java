package se.proxus.mods;

import java.util.ArrayList;

import se.proxus.events.*;


public class ModEvent {
	
	protected ModBase mod;
	
	protected ArrayList<Class<? extends Event>> loadedEvents;
	
	public ModEvent(ModBase mod) {
		setMod(mod);
		setLoadedEvents(new ArrayList<Class<? extends Event>>());
	}
	
	public void registerEvent(Class<? extends Event> event) {
		if(!(getLoadedEvents().contains(event))) {
			getLoadedEvents().add(event);
		}
	}
	
	public ArrayList<Class<? extends Event>> getLoadedEvents() {
		return loadedEvents;
	}
	
	public void setLoadedEvents(ArrayList<Class<? extends Event>> loadedEvents) {
		this.loadedEvents = loadedEvents;
	}
	
	public ModBase getMod() {
		return mod;
	}

	public void setMod(ModBase mod) {
		this.mod = mod;
	}
}