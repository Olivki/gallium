package se.proxus.mods.list.combat;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.mods.*;
import se.proxus.utils.*;
import net.minecraft.src.Packet10Flying;


public class ModFastHeal extends ModBase {

	public ModFastHeal() {
		super("Fast Heal", ModType.COMBAT, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				if(!(Player.isInWater()) && Player.isOnGround()) {
					if(Client.getPlayer().getHealth() <= 20 && Client.getPlayer().getFoodStats().getFoodLevel() >= 16) {
						for(int var0 = 0; var0 < 5000; var0++) {
							Client.sendPacket(new Packet10Flying(false));
						}
					}			
				}
			}
		}
	}
}