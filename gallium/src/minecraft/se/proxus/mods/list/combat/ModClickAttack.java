package se.proxus.mods.list.combat;

import se.proxus.events.Event;
import se.proxus.events.controller.EventAttackEntity;
import se.proxus.events.player.*;
import se.proxus.mods.*;
import se.proxus.utils.Client;
import se.proxus.utils.Player;
import se.proxus.utils.Wrapper;

public class ModClickAttack extends ModBase {

	public ModClickAttack() {
		super("Click Attack", ModType.COMBAT, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
		registerEvent(EventPostUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		Player.setTarget(null);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				if(!(Wrapper.getGallium().getMods().getMod("Aimbot").getState())) {
					((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).onUpdate();
				} if(Player.getTarget() != null) {
					if(Player.isSwinging()) {
						if(Wrapper.getGallium().getMods().getMod("AutoTool").getState()) {
							Wrapper.getGallium().getMods().getMod("AutoTool").onEvent(new EventAttackEntity(Player.getTarget()));
						}

						Player.attackEntity(Player.getTarget());
					}
				}
			} if(event instanceof EventPostUpdate) {
				if(!(Wrapper.getGallium().getMods().getMod("Aimbot").getState())) {
					((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).onPostUpdate();
				}
			}
		}
	}
}