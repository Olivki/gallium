package se.proxus.mods.list.combat;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModAntiVelocity extends ModBase {

	public ModAntiVelocity() {
		super("Anti Velocity", ModType.COMBAT, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventVelocity.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventVelocity) {
				((EventVelocity)event).setCancelled(true);
			}
		}
	}
}