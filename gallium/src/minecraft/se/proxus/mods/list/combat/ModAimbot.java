package se.proxus.mods.list.combat;

import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.minecraft.src.StringUtils;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.server.EventChatReceive;
import se.proxus.mods.*;
import se.proxus.utils.Client;
import se.proxus.utils.Colours;
import se.proxus.utils.Player;
import se.proxus.utils.Wrapper;

public class ModAimbot extends ModBase {

	private Team currentTeam = Team.NONE;

	public ModAimbot() {
		super("Aimbot", ModType.COMBAT, true, false);
		registerCommand(new ModCommand(this, "aimbot", ".aimbot [team] [r|b|n]", 
				"Changes your current team.") {
			@Override
			public void onCommand(String msg, String... args) {
				if(msg.length() > getName().replace(" ", "").length()) {
					if(args[0].equalsIgnoreCase("aimbot") && args[1].equalsIgnoreCase("team")) {
						if(args[2].equalsIgnoreCase("r")) {
							setCurrentTeam(Team.RED);
						} else if(args[2].equalsIgnoreCase("b")) {
							setCurrentTeam(Team.BLUE);
						} else if(args[2].equalsIgnoreCase("n")) {
							setCurrentTeam(Team.NONE);
						}
						Player.addSettingsMessage("Team ", getCurrentTeam().getColour() 
								+ getCurrentTeam().getName() + Colours.RESET);
					}
				}
			}
		});
		registerSetting(0, true, false);
		registerCommand(new ModCommand(this, "aimbot", ".aimbot [setting] [silent]", 
				"Changes the setting if you want a silent aimbot or not.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().replace(" ", "").length()) {
						if(args[0].equalsIgnoreCase("aimbot")) {
							if(args[1].equalsIgnoreCase("setting")) {
								if(args[2].equalsIgnoreCase("silent")) {
									registerSetting(0, !(((Boolean)getSetting(0))), true);
									Player.addSettingsMessage("Aimbot Silent ", "" + ((Boolean)getSetting(0)));
								}
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		setArrayName("Aimbot (Player)");
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
		registerEvent(EventPostUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		Player.setTarget(null);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				onUpdate();
			} if(event instanceof EventPostUpdate) {
				onPostUpdate();
			}
		}
	}

	public void onUpdate() {
		if(Player.getTarget() == null) {
			Player.setTarget(Player.getNearestPlayer());
		} else if(!(Client.getWorld().loadedEntityList.contains(Player.getTarget()))) {
			Player.setTarget(null);
		} else if(Player.getDistanceToEntity(Player.getTarget()) >= 3.5F) {
			Player.setTarget(null);
		} else if(Player.getTarget().getEntityName().startsWith(getCurrentTeam().getColour())) {
			Player.setTarget(null);
		} if(Player.getTarget() != null) {
			if(Player.getTargetChecks(Player.getTarget()) &&
					!(Player.getTarget().getEntityName().startsWith(getCurrentTeam().getColour()))) {
				if(((Boolean)getSetting(0))) {
					registerSetting(1, Player.getRotationPitch(), false, false);
					registerSetting(2, Player.getRotationYaw(), false, false);
					registerSetting(3, Player.getRotationYawHead(), false, false);
					Player.setRotationPitch(Player.getAttackRotationPitch(Player.getTarget()));
					Player.setRotationYaw(Player.getAttackRotationYaw(Player.getTarget()));
				} else {
					Player.setRotationPitch(Player.getAttackRotationPitch(Player.getTarget()));
					Player.setRotationYaw(Player.getAttackRotationYaw(Player.getTarget()));	
				}
			} else {
				Player.setTarget(null);
			}
		}
	}

	public void onPostUpdate() {
		if(Player.getTarget() != null) {
			if(Player.getTargetChecks(Player.getTarget())) {
				if(((Boolean)getSetting(0))) {
					Player.setRotationPitch(((Float)getSetting(1)));
					Player.setRotationYaw(((Float)getSetting(2)));
					Player.setRotationYawHead(((Float)getSetting(3)));
				}
			} else {
				Player.setTarget(null);
			}
		}
	}

	public Team getCurrentTeam() {
		return currentTeam;
	}

	public void setCurrentTeam(Team currentTeam) {
		Wrapper.getGallium().getLogger().log(Level.INFO, "Current team has been set to: " + currentTeam.name());
		this.currentTeam = currentTeam;
	}

	public enum Team {
		RED(Colours.RED, "Red"),
		BLUE(Colours.INDIGO, "Blue"),
		NONE(Colours.RANDOM, "None");

		private String colour;
		private String name;

		Team(String colour, String name) {
			setColour(colour);
			setName(name);
		}

		public boolean isOnTeam(String name) {
			return name.startsWith(getColour());
		}

		public int getTeam(String name) {
			if(name.startsWith(RED.getColour())) {
				return 0;
			} else if(name.startsWith(BLUE.getColour())) {
				return 1;
			}
			return -1;
		}

		public String getColour() {
			return colour;
		}

		public void setColour(String colour) {
			this.colour = colour;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
}