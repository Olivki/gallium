package se.proxus.mods.list.player;

import se.proxus.events.Event;
import se.proxus.events.player.EventUpdate;
import se.proxus.mods.ModBase;
import se.proxus.mods.ModCommand;
import se.proxus.mods.ModType;
import se.proxus.utils.Player;

public class ModJesus extends ModBase {

	public ModJesus() {
		super("Jesus", ModType.PLAYER, false, false);
		registerSetting(0, 0.135D, false);
		registerCommand(new ModCommand(this, "jesus", ".jesus [motion] [value]", 
				"Changes the Motion Y for the Jesus.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().replace(" ", "").length()) {
						if(args[0].equalsIgnoreCase("jesus")) {
							if(args[1].equalsIgnoreCase("motion")) {
								registerSetting(0, Double.parseDouble(args[2]), true);
								Player.addSettingsMessage("Jesus Motion ", "" + ((Double)getSetting(0)));
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		registerSetting(1, 4.35F, false);
		registerCommand(new ModCommand(this, "jesus", ".jesus [speed] [value]", 
				"Changes the speed for the Jesus.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().replace(" ", "").length()) {
						if(args[0].equalsIgnoreCase("jesus")) {
							if(args[1].equalsIgnoreCase("speed")) {
								registerSetting(1, Float.parseFloat(args[2]), true);
								Player.addSettingsMessage("Jesus Motion ", "" + ((Float)getSetting(1)));
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				if(Player.isInWater()) {
					Player.setMotionY(((Double)getSetting(0)));
					Player.setJumpMovementFactor(Player.getJumpMovementFactor() * ((Float)getSetting(1)));
					Player.setSprinting(true);

					if(Player.isCollidedHorizontally())
						Player.setMotionY(0.3D);
				}
			}
		}
	}
}