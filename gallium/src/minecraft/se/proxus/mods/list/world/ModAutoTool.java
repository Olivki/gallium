package se.proxus.mods.list.world;

import java.util.logging.Level;

import se.proxus.events.*;
import se.proxus.events.controller.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.Client;
import se.proxus.utils.Wrapper;

import net.minecraft.src.*;


public class ModAutoTool extends ModBase {

	public ModAutoTool() {
		super("AutoTool", ModType.WORLD, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventBlockClicked.class);
		registerEvent(EventAttackEntity.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			try {
				if(event instanceof EventBlockClicked) {
					if(Client.getController().isNotCreative()) {
						int id = Client.getWorld().getBlockId(((EventBlockClicked)event).getX(), ((EventBlockClicked)event).getY(), 
								((EventBlockClicked)event).getZ());
						Block block = Block.blocksList[id];
						ItemStack currentItem = Client.getPlayer().inventory.getCurrentItem();
						ItemStack newItem = null;

						for(int hotbar = 0; hotbar < 9; hotbar++) {
							newItem = Client.getPlayer().inventory.getStackInSlot(hotbar);

							if(newItem != null && newItem.getItem() instanceof ItemTool) {
								if(newItem.getStrVsBlock(block) > (currentItem == null ? 1.0F : currentItem.getStrVsBlock(block))) {
									Client.getPlayer().inventory.currentItem = hotbar;
								}
							}
						}
					}
				} if(event instanceof EventAttackEntity) {
					if(((EventAttackEntity)event).getTarget() instanceof EntityLiving) {
						registerSetting(0, Client.getPlayer().inventory.currentItem, true, false);
						EntityLiving target = (EntityLiving)((EventAttackEntity)event).getTarget();
						ItemStack currentItem = Client.getPlayer().inventory.getCurrentItem();
						ItemStack newItem = null;

						for(int hotbar = 0; hotbar < 9; hotbar++) {
							newItem = Client.getPlayer().inventory.getStackInSlot(hotbar);

							if(newItem != null && newItem.getItem() instanceof ItemSword) {
								if(newItem.getDamageVsEntity(target) > (currentItem == null ? 1 : currentItem.getDamageVsEntity(target))) {
									Client.getPlayer().inventory.currentItem = hotbar;
								}
							}
						}
					}
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		}
	}
}