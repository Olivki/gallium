package se.proxus.mods.list.render;

import org.lwjgl.opengl.GL11;

import se.proxus.events.Event;
import se.proxus.events.render.EventRender3D;
import se.proxus.events.render.EventRenderNametag;
import se.proxus.events.render.EventRenderPlayer;
import se.proxus.mods.*;
import se.proxus.mods.list.combat.ModAimbot;
import se.proxus.mods.list.combat.ModAimbot.Team;
import se.proxus.mods.list.none.ModFriend;
import se.proxus.utils.Client;
import se.proxus.utils.Colours;
import se.proxus.utils.Player;
import se.proxus.utils.Wrapper;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.EntityPlayer;


public class ModTracer extends ModBase {

	public ModTracer() {
		super("Tracer", ModType.RENDER, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventRender3D.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventRender3D) {
				for(Object players : Client.getWorld().playerEntities) {
					EntityPlayer player = (EntityPlayer)players;

					if(player != Client.getPlayer()) {
						if(Player.getTarget() instanceof EntityPlayer && Player.getTarget() == player) {
							registerSetting(0, 0xFFEE6363, false, false);
						} else if(Player.getTarget() != player) {
							registerSetting(0, 0xFFFFFFFF, false, false);
						} if(((ModFriend)Wrapper.getGallium().getMods().getMod("Friend")).isFriend(player.username)) {
							registerSetting(0, 0xFF4F94CD, false, false);
						} if(player.hurtTime > 0) {
							registerSetting(0, 0xFFFFA500, false, false);
						} if(((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).getCurrentTeam() != Team.NONE) {
							switch(((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).getCurrentTeam().getTeam(player.username)) {
							case 0:
								registerSetting(0, 0xFFFF5555, false, false);
								break;
							case 1:
								registerSetting(0, 0xFF5555FF, false, false);
								break;
							default:
								registerSetting(0, 0xFFEE6363, false, false);
								break;
							}
						}

						Client.drawTracerTo(player.posX, player.posY + player.getEyeHeight() - 0.5F, player.posZ, 
								1.0F, Colours.getRGBA(((Integer)getSetting(0)))[0], 
								Colours.getRGBA(((Integer)getSetting(0)))[1], Colours.getRGBA(((Integer)getSetting(0)))[2], 1.7F, player);
					}
				}
			}
		}
	}
}