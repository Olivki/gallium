package se.proxus.mods.list.render;

import org.lwjgl.opengl.GL11;

import se.proxus.events.Event;
import se.proxus.events.render.EventRenderNametag;
import se.proxus.events.render.EventRenderPlayer;
import se.proxus.mods.*;
import se.proxus.mods.list.combat.ModAimbot;
import se.proxus.mods.list.combat.ModAimbot.Team;
import se.proxus.mods.list.none.ModFriend;
import se.proxus.utils.Client;
import se.proxus.utils.Colours;
import se.proxus.utils.Player;
import se.proxus.utils.Wrapper;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.EntityPlayer;


public class ModESP extends ModBase {

	public ModESP() {
		super("ESP", ModType.RENDER, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventRenderNametag.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventRenderNametag) {
				EntityPlayer player = ((EventRenderNametag)event).getPlayer();
				double x = ((EventRenderNametag)event).getX();
				double y = ((EventRenderNametag)event).getY();
				double z = ((EventRenderNametag)event).getZ();

				if(Player.getTarget() instanceof EntityPlayer && Player.getTarget() == player) {
					registerSetting(0, 0xFFEE6363, false, false);
				} else if(Player.getTarget() != player) {
					registerSetting(0, 0xFFFFFFFF, false, false);
				} if(((ModFriend)Wrapper.getGallium().getMods().getMod("Friend")).isFriend(player.username)) {
					registerSetting(0, 0xFF4F94CD, false, false);
				} if(player.hurtTime > 0) {
					registerSetting(0, 0xFFFFA500, false, false);
				} if(((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).getCurrentTeam() != Team.NONE) {
					switch(((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).getCurrentTeam().getTeam(player.username)) {
					case 0:
						registerSetting(0, 0xFFFF5555, false, false);
						break;
					case 1:
						registerSetting(0, 0xFF5555FF, false, false);
						break;
					default:
						registerSetting(0, 0xFFEE6363, false, false);
						break;
					}
				}

				Client.enableDefaults();
				//Client.glColor4Hex(((Integer)getSetting(0)));
				GL11.glColor4f(Colours.getRGBA(((Integer)getSetting(0)))[0], 
						Colours.getRGBA(((Integer)getSetting(0)))[1], Colours.getRGBA(((Integer)getSetting(0)))[2],
						1.0F);
				GL11.glTranslated(x + 0.5D, y, z + 0.5D);
				GL11.glRotatef(-player.rotationYaw, 0, 1, 0);
				GL11.glTranslated(-x - 0.5D, -y, -z - 0.5D);
				GL11.glLineWidth(1.7F);
				Client.renderOutlinedBox(AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + 2.0D, z + 1.0D));
				Client.renderCrossedBox(AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + 2.0D, z + 1.0D));
				Client.disableDefaults();
			}
		}
	}
}