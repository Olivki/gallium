package se.proxus.mods.list.none;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import se.proxus.events.*;
import se.proxus.events.controller.*;
import se.proxus.events.misc.*;
import se.proxus.events.server.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

import net.minecraft.src.*;


public class ModFriend extends ModBase {

	protected ArrayList<ArrayHelper<String, String>> loadedFriends = new ArrayList<ArrayHelper<String, String>>();

	public ModFriend() {
		super("Friend", ModType.NONE, true, true, ModController.COMMAND);
		setState(true);
		registerCommand(new ModCommand(this, "friend", ".friend [add] [username] [alias]", 
				"Adds the selected user to your friendslist.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().length()) {
						if(args[0].equalsIgnoreCase("friend")) {
							if(args[1].equalsIgnoreCase("add")) {
								if(!(isFriend(args[2]))) {
									getLoadedFriends().add(new ArrayHelper(args[2], msg.substring((args[0].length() + args[1].length() 
											+ args[2].length() + 3))));
									Player.addMessage("Added the friend: " + Colours.DARK_AQUA + msg.substring((args[0].length() + 
											args[1].length() + args[2].length() + 3)) + Colours.RESET + "!");
									saveFriends();
								} else {
									Player.addMessage(Colours.DARK_AQUA + args[2] + Colours.RESET + " is already added!");
								}
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		registerCommand(new ModCommand(this, "friend", ".friend [remove] [username]", 
				"Removes the selected user to your friendslist.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() > getName().length()) {
						if(args[0].equalsIgnoreCase("friend")) {
							if(args[1].equalsIgnoreCase("remove")) {
								removeByAlias(msg.substring((args[0].length() + args[1].length() + 2)));
								Player.addMessage("Removed the user: " + Colours.DARK_AQUA 
										+ msg.substring((args[0].length() + args[1].length() + 2)));
								saveFriends();
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
		loadFriends();
	}

	@Override
	public void init() {
		registerEvent(EventChatSend.class);
		registerEvent(EventStringRendered.class);
		registerEvent(EventAttackEntity.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventChatSend) {
				for(ArrayHelper<String, String> friend : getLoadedFriends()) {
					((EventChatSend)event).setMessage(((EventChatSend)event).getMessage().replace(((String)friend.getObject()[1]), 
							((String)friend.getObject()[0])));
				}
			} if(event instanceof EventStringRendered) {
				for(ArrayHelper<String, String> friend : getLoadedFriends()) {
					((EventStringRendered)event).setString(((EventStringRendered)event).getString().replaceAll(((String)friend.getObject()[0]), 
							Colours.DARK_AQUA + ((String)friend.getObject()[1]) + Colours.RESET));
				}
			} if(event instanceof EventAttackEntity) {
				if(((EventAttackEntity)event).getTarget() instanceof EntityPlayer) {
					((EventAttackEntity)event).setCancelled(isFriend(((EntityPlayer)((EventAttackEntity)event).getTarget()).username));
				}
			}
		}
	}

	public ArrayList<ArrayHelper<String, String>> getLoadedFriends() {
		return loadedFriends;
	}

	public void setLoadedFriends(ArrayList<ArrayHelper<String, String>> loadedFriends) {
		this.loadedFriends = loadedFriends;
	}

	public boolean isFriend(String alias) {
		for(ArrayHelper<String, String> friend : getLoadedFriends()) {
			if(((String)friend.getObject()[0]).equalsIgnoreCase(StringUtils.stripControlCodes(alias))) {
				return true;
			}
		}

		return false;
	}

	public void removeByAlias(String alias) {
		for(ArrayHelper friend : getLoadedFriends()) {
			if(((String)friend.getObject()[1]).equalsIgnoreCase(alias)) {
				getLoadedFriends().remove(getLoadedFriends().indexOf(friend));
			}
		}
	}
	
	public void saveFriends() {
		try {
			File friendFile = new File(Client.getMinecraft().getMinecraftDir() + "/Gallium/Friends.gcfg/");
			PrintWriter friendsWriter = new PrintWriter(friendFile);
			
			for(ArrayHelper<String, String> friend : getLoadedFriends()) {
				friendsWriter.println(friend.getObject()[0] + " --> " + friend.getObject()[1]);
			}

			friendsWriter.close();
		} catch(Exception exception) {
			exception.printStackTrace();
		}
	}

	public void loadFriends() {
		File friendFile = new File(Client.getMinecraft().getMinecraftDir() + "/Gallium/Friends.gcfg/");

		if(friendFile.exists()) {
			try {
				BufferedReader friendsReader = new BufferedReader(new FileReader(friendFile));

				try {
					for(String line = ""; (line = friendsReader.readLine()) != null;) {
						String[] split = line.split(" --> ");

						getLoadedFriends().add(new ArrayHelper(split[0], split[1]));
					}
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		} else {
			saveFriends();
		}
	}
}