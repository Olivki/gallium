package se.proxus.mods.list.none;

import se.proxus.events.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModHelp extends ModBase {

	public ModHelp() {
		super("Help", ModType.NONE, true, true, ModController.COMMAND);
		registerCommand(new ModCommand(this, "help", ".help [empty|modname]", 
				"Used for getting help on a certain mod.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(msg.length() == getName().replace(" ", "").length()) {
						for(ModBase mod : Wrapper.getGallium().getMods().getLoadedMods()) {
							for(ModCommand command : mod.getLoadedCommands()) {
								Player.addMessage(command.getUsage() + " - " + command.getDescription());
							}
						}
					} else {
						if(args[0].equalsIgnoreCase("help")) {
							if(msg.length() > getName().length()) {
								for(ModCommand command : Wrapper.getGallium().getMods().getMod(args[1]).getLoadedCommands()) {
									Player.addMessage(command.getUsage() + " - " + command.getDescription() + Colours.RESET);
								}
							}
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
	}

	@Override
	public void init() {

	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {

	}
}