package se.proxus.mods.list.server;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.mods.*;
import se.proxus.utils.Wrapper;

public class ModNoCheat extends ModBase {

	public ModNoCheat() {
		super("NoCheat", ModType.SERVER, true, true);
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				for(ModBase mod : Wrapper.getGallium().mods.getActiveMods()) {
					if(!(mod.getNoCheat())) {
						mod.setState(false);
					}
				}
			}
		}
	}
}