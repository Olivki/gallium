package se.proxus.mods;

import java.util.*;
import java.util.logging.Level;

import se.proxus.events.*;
import se.proxus.mods.list.combat.*;
import se.proxus.mods.list.gui.*;
import se.proxus.mods.list.misc.*;
import se.proxus.mods.list.none.*;
import se.proxus.mods.list.player.*;
import se.proxus.mods.list.render.*;
import se.proxus.mods.list.server.*;
import se.proxus.mods.list.world.*;
import se.proxus.utils.*;


public class ModHandler {

	private ArrayList<ModBase> loadedMods = new ArrayList<ModBase>();

	private HashMap<String, ModBase> loadedModsHash = new HashMap<String, ModBase>();

	private ArrayList<ModBase> activeMods = new ArrayList<ModBase>();

	public void initModHandler() {
		addMod(new ModNoCheat());
		addMod(new ModFriend());
		addMod(new ModAntiVelocity());
		addMod(new ModBrightness());
		addMod(new ModGui());
		addMod(new ModAimbot());
		addMod(new ModChams());
		addMod(new ModFOV());
		addMod(new ModHelp());
		addMod(new ModLogin());
		addMod(new ModItem());
		addMod(new ModPanels());
		addMod(new ModAutoTool());
		addMod(new ModAutoWalk());
		addMod(new ModFlight());
		addMod(new ModFreecam());
		addMod(new ModJesus());
		addMod(new ModNoFall());
		addMod(new ModSneak());
		addMod(new ModSprint());
		addMod(new ModStep());
		addMod(new ModCriticals());
		addMod(new ModAutoAttack());
		addMod(new ModClickAttack());
		addMod(new ModFastHeal());
		addMod(new ModConsole());
		addMod(new ModSpam());
		addMod(new ModESP());
		addMod(new ModTracer());
		addMod(new ModNuker());
		addMod(new ModAntiHeadshot());
		Wrapper.getGallium().getLogger().log(Level.INFO, getLoadedMods().size() + " mod(s) have been loaded!");
	}

	public void addMod(ModBase mod) {
		if(!(getLoadedMods().contains(mod))) {
			getLoadedModsHash().put(mod.getName().toLowerCase(), mod);
			getLoadedMods().add(mod);
		}
	}

	public void handleKeyPressed(String key) {
		for(ModBase mod : getLoadedMods()) {
			if(mod.getController().equals(ModController.TOGGLE)) {
				if(!(mod.getKey().equalsIgnoreCase("NONE"))) {
					if(mod.getKey().equalsIgnoreCase(key)) {
						Client.playSound((mod.getState() ? 1.1F : 0.7F));
						mod.toggle();
					}
				}
			}
		}
	}

	public Event callEvent(Event event) {
		for(ModBase mod : loadedMods) {
			try {
				if(mod.getEvent().getLoadedEvents().contains(event.getClass())) {
					mod.onEvent(event);
				}
			} catch(Exception exception) {
				exception.printStackTrace();
				Wrapper.getGallium().getLogger().log(Level.WARNING, exception.getMessage());
			}
		}

		return event;
	}

	public ModBase getMod(String name) {
		return getLoadedModsHash().get(name.toLowerCase());
	}

	public ArrayList<ModBase> getLoadedMods() {
		return loadedMods;
	}

	public void setLoadedMods(ArrayList<ModBase> loadedMods) {
		this.loadedMods = loadedMods;
	}

	public HashMap<String, ModBase> getLoadedModsHash() {
		return loadedModsHash;
	}

	public void setLoadedModsHash(HashMap<String, ModBase> loadedModsHash) {
		this.loadedModsHash = loadedModsHash;
	}

	public void setActiveMods(ArrayList<ModBase> activeMods) {
		this.activeMods = activeMods;
	}

	public ArrayList<ModBase> getActiveMods() {
		return activeMods;
	}
}