package se.proxus.events.world;

import se.proxus.events.*;

public class EventGamma extends Event {
	
	protected float gamma;

	public float getGamma() {
		return gamma;
	}

	public void setGamma(float gamma) {
		this.gamma = gamma;
	}
}
