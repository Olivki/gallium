package se.proxus.events.world;

import se.proxus.events.*;

public class EventBlockClicked extends Event {

	protected int[] position = new int[6];

	public EventBlockClicked(int x, int y, int z, int side, int id) {
		setX(x);
		setY(y);
		setZ(z);
		setSide(side);
		setID(id);
	}

	public int getX() {
		return position[0];
	}

	public int getY() {
		return position[1];
	}

	public int getZ() {
		return position[2];
	}

	public int getSide() {
		return position[3];
	}
	
	public int getID() {
		return position[4];
	}

	public void setX(int var0) {
		position[0] = var0;
	}

	public void setY(int var0) {
		position[1] = var0;
	}

	public void setZ(int var0) {
		position[2] = var0;
	}

	public void setSide(int var0) {
		position[3] = var0;
	}
	
	public void setID(int var0) {
		position[4] = var0;
	}
}