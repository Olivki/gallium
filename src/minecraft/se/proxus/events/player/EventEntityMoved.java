package se.proxus.events.player;

import se.proxus.events.*;

public class EventEntityMoved extends Event {
	
	protected double x;
	
	protected double y;
	
	protected double z;
	
	public EventEntityMoved(double x, double y, double z) {
		this.setX(x);
		this.setY(y);
		this.setZ(z);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
}