package se.proxus.events.misc;

import se.proxus.events.*;

/** Event for checking when a key gets pressed. **/
public class EventKeyPressed extends Event {
	
	/** The ID of the key that was pressed. **/
	protected int keyID;
	
	/** The name of the key that was pressed. **/
	protected String keyName;
	
	/** The character it would produce. **/
	protected char keyChar;
	
	public EventKeyPressed(int keyID, String keyName, char keyChar) {
		this.setKeyID(keyID);
		this.setKeyName(keyName);
		this.setKeyChar(keyChar);
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public int getKeyID() {
		return keyID;
	}

	public void setKeyID(int keyID) {
		this.keyID = keyID;
	}

	public char getKeyChar() {
		return keyChar;
	}

	public void setKeyChar(char keyChar) {
		this.keyChar = keyChar;
	}
}