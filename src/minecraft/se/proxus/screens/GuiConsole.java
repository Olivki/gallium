package se.proxus.screens;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import se.proxus.mods.ModBase;
import se.proxus.mods.ModCommand;
import se.proxus.utils.Client;
import se.proxus.utils.Colours;
import se.proxus.utils.Player;
import se.proxus.utils.Wrapper;


import net.minecraft.src.*;

public class GuiConsole extends GuiScreen {

	private GuiTextField consoleField;

	@Override
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		setConsoleField(new GuiTextField(fontRenderer, width / 2 - 118, 3, 220, 14));
		getConsoleField().setMaxStringLength(100);
		getConsoleField().setEnableBackgroundDrawing(false);
		getConsoleField().setFocused(true);
		getConsoleField().setText("");
		getConsoleField().setCanLoseFocus(false);
	}

	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	@Override
	public void updateScreen() {
		getConsoleField().updateCursorCounter();
	}

	@Override
	public void keyTyped(char keyChar, int keyId) {
		super.keyTyped(keyChar, keyId);

		if(keyId == 28) {
			if(getConsoleField().getText().length() > 0) {
				for(ModBase mod : mc.gm.mods.getLoadedMods()) {
					for(ModCommand command : mod.getLoadedCommands()) {
						try {
							if(Client.startsWithIgnoreCase(getConsoleField().getText(), command.getCommand().substring(1))) {
								command.onCommand(getConsoleField().getText(), getConsoleField().getText().split(" "));
							}
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}

				mc.displayGuiScreen(null);
			}
		} else {
			getConsoleField().textboxKeyTyped(keyChar, keyId);
		}
	}

	@Override
	public void drawScreen(int x, int y, float ticks) {
		super.drawScreen(x, y, ticks);

		drawGradientBorderedRect(width / 2 - 121, 0, width / 2 - 100 + 221, 14, 0xFFA1A1A1, 0xFFE8E8E8, 0xFFCCCCCC);

		getConsoleField().drawTextBox();

		drawPrediction(getConsoleField().getText(), x, y);
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		super.mouseClicked(x, y, type);

		getConsoleField().mouseClicked(x, y, type);

		if(type == 0) {
			mouseClickedPredict(getConsoleField().getText(), x, y);
		}
	}

	public GuiTextField getConsoleField() {
		return consoleField;
	}

	public void setConsoleField(GuiTextField consoleField) {
		this.consoleField = consoleField;
	}

	public void drawPrediction(String var0, int x, int y) {
		ArrayList<ModCommand> pre = new ArrayList<ModCommand>();

		for(ModBase mod : Wrapper.getGallium().getMods().getLoadedMods()) {
			for(ModCommand command : mod.getLoadedCommands()) {
				if(!(pre.contains(command))) {
					pre.add(0, command);
				}
			}
		}

		ArrayList<ModCommand> toAdd = new ArrayList<ModCommand>();

		toAdd.clear();

		for(int var2 = 0; var2 < pre.size(); var2++) {
			ModCommand command = (ModCommand)pre.get(var2);

			String cmd = command.getUsage().substring(1).replace("[", "").replace("]", "");

			if(Client.startsWithIgnoreCase(cmd, var0) && var0.length() >= 1) {
				toAdd.add(command);
			} else if(var0.length() < 1) {
				toAdd.clear();
				toAdd.add(new ModCommand(null, "Swag", "PPlease type a command to get started.", "Yolo"));
			}
		}

		for(int var4 = 0; var4 < toAdd.size(); var4++) {
			ModCommand command = (ModCommand)toAdd.get(var4);

			drawGradientBorderedRect(width / 2 - 121, 15 + var4 * 15, width / 2 - 100 + 221, 15 + var4 * 15 + 14, 
					0xFFA1A1A1, 0xFFE8E8E8, 0xFFCCCCCC);
			Client.drawString(command.getUsage().substring(1).replace("[", "").replace("]", ""), width / 2 - 118, 17.5F + var4 * 15, 
					(isHovering(x, y, width / 2 - 121, 15 + var4 * 15, width / 2 - 100 + 221, 15 + var4 * 15 + 14) ? 0xFFFFFFA0 : 0xFF474747));
			Client.drawString(Colours.DARK_AQUA + var0.toLowerCase(), width / 2 - 118, 17.5F + var4 * 15, 0xFF474747);
		}
	}

	public void mouseClickedPredict(String var0, int x, int y) {
		ArrayList<ModCommand> pre = new ArrayList<ModCommand>();

		for(ModBase mod : Wrapper.getGallium().getMods().getLoadedMods()) {
			for(ModCommand command : mod.getLoadedCommands()) {
				if(!(pre.contains(command))) {
					pre.add(0, command);
				}
			}
		}

		ArrayList<ModCommand> toAdd = new ArrayList<ModCommand>();

		toAdd.clear();

		for(int var2 = 0; var2 < pre.size(); var2++) {
			ModCommand command = (ModCommand)pre.get(var2);

			String cmd = command.getUsage().substring(1).replace("[", "").replace("]", "");

			if(Client.startsWithIgnoreCase(cmd, var0) && var0.length() >= 1) {
				toAdd.add(command);
			}
		}

		for(int var4 = 0; var4 < toAdd.size(); var4++) {
			ModCommand command = (ModCommand)toAdd.get(var4);

			if(isHovering(x, y, width / 2 - 121, 15 + var4 * 15, width / 2 - 100 + 221, 15 + var4 * 15 + 14)) {
				Client.playSound(1.0F);
				getConsoleField().setText(command.getUsage().substring(1).replace("[", "").replace("]", "").replace("value", "")
						.replace("keybind", "").replace("username", "").replace("name", "").replace("alias", "").replace("text", ""));
				if(!(getConsoleField().getText().contains(" "))) {
					if(getConsoleField().getText().length() > 0) {
						try {
							if(Client.startsWithIgnoreCase(getConsoleField().getText(), command.getCommand().substring(1))) {
								command.onCommand(getConsoleField().getText(), getConsoleField().getText().split(" "));
							}
						} catch(Exception e) {
							e.printStackTrace();
						}
					}

					mc.displayGuiScreen(null);
				}
			}
		}
	}

	public void drawGradientBorderedRect(float x, float y, float width, float height, int hex1, int hex2, int hex3) {
		Client.getIngameGui().drawGradientRect(x + 1, y + 1, width - 1, height - 1, hex2, hex3);
		Client.getIngameGui().drawVerticalLine(x, y, height - 1, hex1);
		Client.getIngameGui().drawVerticalLine(width - 1, y, height - 1, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, y, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, height - 1, hex1);
	}

	public boolean isHovering(int mouseX, int mouseY, int x, int y, int width, int height) {
		return mouseX >= x && mouseY >= y && mouseX <= width && mouseY <= height;
	}
}