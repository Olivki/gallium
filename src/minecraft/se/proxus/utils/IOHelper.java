package se.proxus.utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.lwjgl.Sys;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EnumOS;

import se.proxus.*;

public class IOHelper extends Gallium {

	public static void createFile(File file) {
		file.mkdirs();
	}
	
	public static void openFile(String location) {
		if(Wrapper.getMinecraft().getOs() == EnumOS.MACOS) {
			try {
				System.out.println(location);
				Runtime.getRuntime().exec(new String[] {"/usr/bin/open", location});
				return;
			} catch (IOException var7) {
				var7.printStackTrace();
			}
		} else if(Wrapper.getMinecraft().getOs() == EnumOS.WINDOWS) {
			String var2 = String.format("cmd.exe /C start \"Open file\" \"%s\"", new Object[] {location});

			try {
				Runtime.getRuntime().exec(var2);
				return;
			} catch (IOException var6) {
				var6.printStackTrace();
			}
		}

		boolean var8 = false;

		try {
			Class var3 = Class.forName("java.awt.Desktop");
			Object var4 = var3.getMethod("getDesktop", new Class[0]).invoke((Object)null, new Object[0]);
			var3.getMethod("browse", new Class[] {URI.class}).invoke(var4, new Object[] {(new File(Wrapper.getMinecraft().getMinecraftDir(), location)).toURI()});
		} catch (Throwable var5) {
			var5.printStackTrace();
			var8 = true;
		}

		if(var8) {
			System.out.println("Opening via system class!");
			Sys.openURL("file://" + location);
		}
	}
}