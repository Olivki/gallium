package se.proxus.utils;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Vector;
import java.util.logging.Level;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.minecraft.client.*;
import net.minecraft.src.*;

public class Client {

	public static RenderItem itemRenderer = new RenderItem();

	private static Client instance;

	public static Minecraft getMinecraft() {
		return Minecraft.getMinecraft();
	}

	public static EntityClientPlayerMP getPlayer() {
		return getMinecraft().thePlayer;
	}

	public static PlayerControllerMP getController() {
		return getMinecraft().playerController;
	}

	public static WorldClient getWorld() {
		return getMinecraft().theWorld;
	}

	public static GuiIngame getIngameGui() {
		return getMinecraft().ingameGUI;
	}

	public static RenderEngine getRenderEngine() {
		return getMinecraft().renderEngine;
	}

	public static NetClientHandler getSendQueue() {
		return getMinecraft().getSendQueue();
	}

	public static FontRenderer getFontRenderer() {
		return getMinecraft().fontRenderer;
	}

	public static MovingObjectPosition getMouseOver() {
		return getMinecraft().objectMouseOver;
	}

	public static GameSettings getGameSettings() {
		return getMinecraft().gameSettings;
	}

	public static ScaledResolution getScaledResolution() {
		return new ScaledResolution(getGameSettings(), getWidth(), getHeight());
	}

	public static int getWidth() {
		return getMinecraft().displayWidth;
	}

	public static int getHeight() {
		return getMinecraft().displayHeight;
	}

	public static String getFormattedString(float decimal) {
		DecimalFormat df = new DecimalFormat();
		df.setMinimumFractionDigits(1);
		df.setMaximumFractionDigits(1);

		return df.format(decimal).replace(",", ".");
	}

	public static void sendPacket(Packet packet) {
		getSendQueue().addToSendQueue(packet);
	}

	public static int getScaledWidth() {
		return getScaledResolution().getScaledWidth();
	}

	public static int getScaledHeight() {
		return getScaledResolution().getScaledHeight();
	}

	public static void drawString(String name, float x, float y, int hex) {
		renderString(name, x, y, hex, false);
	}

	public static void drawStringWithShadow(String name, float x, float y, int hex) {
		int shade = (hex & 16579836) >> 2 | hex & -16777216;
		renderString(StringUtils.stripControlCodes(name), x + 0.5F, y + 0.5F, shade, true);
		renderString(name, x, y, hex, false);
	}

	public static void drawStringPanel(String name, float x, float y, int hex) {
		renderStringPanel(name, x, y, hex, false);
	}

	public static void drawStringWithShadowPanel(String name, float x, float y, int hex) {
		int shade = (hex & 16579836) >> 2 | hex & -16777216;
		renderStringPanel(StringUtils.stripControlCodes(name), x + 0.5F, y + 0.5F, shade, true);
		renderStringPanel(name, x, y, hex, false);
	}
	
	public static void drawStringChat(String name, float x, float y, int hex) {
		renderStringChat(name, x, y, hex, false);
	}

	public static void drawStringWithShadowChat(String name, float x, float y, int hex) {
		int shade = (hex & 16579836) >> 2 | hex & -16777216;
		renderStringChat(StringUtils.stripControlCodes(name), x + 0.5F, y + 0.5F, 0xFF000000, true);
		renderStringChat(name, x, y, hex, false);
	}
	
	public static int renderString(String name, float g, float f, int hex, boolean shadow) {
		return Wrapper.getGallium().getFont().renderString(name, g, f, hex, shadow);
	}
	
	public static int renderStringPanel(String name, float g, float f, int hex, boolean shadow) {
		return Wrapper.getGallium().getFontPanel().renderString(name, g, f, hex, shadow);
	}
	
	public static int renderStringChat(String name, float g, float f, int hex, boolean shadow) {
		return Wrapper.getGallium().getFontChat().renderString(name, g, f, hex, shadow);
	}

	public static int getStringWidth(String string) {
		return Wrapper.getGallium().getFont().getStringWidth(string);
	}
	
	public static int getStringWidthPanel(String string) {
		return Wrapper.getGallium().getFontPanel().getStringWidth(string);
	}
	
	public static int getStringWidthChat(String string) {
		return Wrapper.getGallium().getFontChat().getStringWidth(string);
	}

	public static void renderBlocks() {
		int x = (int)Player.getX();
		int y = (int)Player.getY();
		int z = (int)Player.getZ();

		Client.getMinecraft().renderGlobal.markBlockRangeForRenderUpdate(x - 200, y - 200, z - 200, x + 200, y + 200, z + 200);
	}

	public static Chunk getCurrentChunk(int x, int z) {
		return getWorld().getChunkFromBlockCoords(x, z);
	}

	public static float getLightValue() {
		int x = MathHelper.floor_double(getPlayer().posX);
		int y = MathHelper.floor_double(getPlayer().posY);
		int z = MathHelper.floor_double(getPlayer().posZ);
		return getCurrentChunk(x, z).getBlockLightValue(x & 15, y, z & 15, 0);
	}

	public static String getCurrentBiomeName() {
		int x = MathHelper.floor_double(Player.getX());
		int y = MathHelper.floor_double(Player.getY());
		int z = MathHelper.floor_double(Player.getZ());

		if(getWorld() != null && getWorld().blockExists(x, y, z)) {
			return getCurrentChunk(x, z).getBiomeGenForWorldCoords(x & 15, z & 15, 
					getWorld().getWorldChunkManager()).biomeName;
		}

		return "N/A";
	}

	public static final String CRLF="\015\012";

	public static final String __LINE_SEPARATOR =
			System.getProperty("line.separator","\n");

	public static String __ISO_8859_1;

	static {
		String iso=System.getProperty("ISO_8859_1");
		if (iso!=null)
			__ISO_8859_1=iso;
		else {
			try {
				new String(new byte[]{(byte)20},"ISO-8859-1");
				__ISO_8859_1="ISO-8859-1";
			}
			catch(java.io.UnsupportedEncodingException e)
			{
				__ISO_8859_1="ISO8859_1";
			}        
		}
	}

	public final static String __UTF8="UTF-8";


	private static char[] lowercases = {
		'\000','\001','\002','\003','\004','\005','\006','\007',
		'\010','\011','\012','\013','\014','\015','\016','\017',
		'\020','\021','\022','\023','\024','\025','\026','\027',
		'\030','\031','\032','\033','\034','\035','\036','\037',
		'\040','\041','\042','\043','\044','\045','\046','\047',
		'\050','\051','\052','\053','\054','\055','\056','\057',
		'\060','\061','\062','\063','\064','\065','\066','\067',
		'\070','\071','\072','\073','\074','\075','\076','\077',
		'\100','\141','\142','\143','\144','\145','\146','\147',
		'\150','\151','\152','\153','\154','\155','\156','\157',
		'\160','\161','\162','\163','\164','\165','\166','\167',
		'\170','\171','\172','\133','\134','\135','\136','\137',
		'\140','\141','\142','\143','\144','\145','\146','\147',
		'\150','\151','\152','\153','\154','\155','\156','\157',
		'\160','\161','\162','\163','\164','\165','\166','\167',
		'\170','\171','\172','\173','\174','\175','\176','\177'};

	/* ------------------------------------------------------------ */
	public static boolean startsWithIgnoreCase(String s, String w) {
		if (w==null)
			return true;

		if (s==null || s.length()<w.length())
			return false;

		for (int i=0;i<w.length();i++)
		{
			char c1=s.charAt(i);
			char c2=w.charAt(i);
			if (c1!=c2)
			{
				if (c1<=127)
					c1=lowercases[c1];
				if (c2<=127)
					c2=lowercases[c2];
				if (c1!=c2)
					return false;
			}
		}
		return true;
	}

	public static void playSound(float pitch) {
		getMinecraft().sndManager.playSoundFX("random.click", 1.0F, pitch);
	}

	public static long getCurrentMilliseconds() {
		return getNanoTime() / 1000000;
	}

	public static long getNanoTime() {
		return System.nanoTime();
	}

	public static void drawItem(ItemStack item, int x, int y) {
		GL11.glPushMatrix();
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		RenderHelper.enableGUIStandardItemLighting();
		itemRenderer.renderItemAndEffectIntoGUI(Client.getFontRenderer(), Client.getRenderEngine(), item, x, y);
		itemRenderer.renderItemOverlayIntoGUI(Client.getFontRenderer(), Client.getRenderEngine(), item, x, y);
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}

	public static String[] wrapText(String text, int len) {
		if(text == null) {
			return new String [] {};
		}

		if(len <= 0) {
			return new String [] {text};
		}

		if(text.length() <= len) {
			return new String [] {text};
		}

		char[] chars = text.toCharArray();

		Vector lines = new Vector();

		StringBuffer line = new StringBuffer();

		StringBuffer word = new StringBuffer();

		for(int i = 0; i < chars.length; i++) {
			word.append(chars[i]);

			if(chars[i] == ' ') {
				if((line.length() + word.length()) > len) {
					lines.add(line.toString());
					line.delete(0, line.length());
				}

				line.append(word);
				word.delete(0, word.length());
			}
		}

		if(word.length() > 0) {
			if((line.length() + word.length()) > len) {
				lines.add(line.toString());
				line.delete(0, line.length());
			}

			line.append(word);
		}

		if(line.length() > 0) {
			lines.add(line.toString());
		}

		String[] ret = new String[lines.size()];

		int c = 0;

		for(Enumeration e = lines.elements(); e.hasMoreElements(); c++) {
			ret[c] = (String) e.nextElement();
		}

		return ret;
	}

	public static boolean loginToAccount(String username, String password) {
		try {
			URL url = new URL(String.format("http://login.minecraft.net/?user=%s&password=%s&version=69", username, password));
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
			String as[] = in.readLine().split(":");
			getMinecraft().session = new Session(as[2], as[3]);
			return true;
		} catch (Exception e) { 
			Wrapper.getGallium().getLogger().log(Level.INFO, "Failed to log into the account: " + username + ":" + password);
			return false;
		}
	}

	public static void openSite(String url) {
		Desktop desktop = Desktop.getDesktop();

		try {
			URI uri = new URI(url);
			desktop.browse(uri);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static float getTimeFromTicks(int tick) {
		return (tick / 20);
	}

	public static void renderOutlinedBox(AxisAlignedBB var0) {
		Tessellator var1 = Tessellator.instance;

		var1.startDrawing(3);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.draw();

		var1.startDrawing(3);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.draw();

		var1.startDrawing(1);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.draw();
	}

	public static void renderCrossedBox(AxisAlignedBB var0) {
		Tessellator var1 = Tessellator.instance;

		var1.startDrawing(3);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.draw();

		var1.startDrawing(3);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.draw();

		var1.startDrawing(1);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.draw();
	}

	public static void renderBox(AxisAlignedBB var0) {
		Tessellator var1 = Tessellator.instance;

		var1.startDrawingQuads();
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.draw();

		var1.startDrawingQuads();
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.draw();

		var1.startDrawingQuads();
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.draw();

		var1.startDrawingQuads();
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.draw();

		var1.startDrawingQuads();
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.draw();

		var1.startDrawingQuads();
		var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.minX, var0.minY, var0.maxZ);
		var1.addVertex(var0.minX, var0.maxY, var0.minZ);
		var1.addVertex(var0.minX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
		var1.addVertex(var0.maxX, var0.minY, var0.minZ);
		var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
		var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
		var1.draw();
	}

	public static void drawTracerTo(double x, double y, double z, double a, double r, double g, double b, float lWidth, EntityPlayer e) {
		double d3 = x - getFixedPos(e)[0];
		double d4 = y - getFixedPos(e)[1];
		double d5 = z - getFixedPos(e)[2];

		enableDefaults();
		GL11.glColor4d(r, g, b, a);
		GL11.glLineWidth(lWidth);

		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(0.0D, 0.0D);
		GL11.glVertex3d(d3, d4, d5);
		GL11.glEnd();
		disableDefaults();
	}

	public static double[] getFixedPos(EntityPlayer var0) {
		return (new double[] {
				getPlayer().lastTickPosX + (Player.getX() - getPlayer().lastTickPosX) * (double)getMinecraft().getTimer().renderPartialTicks,
				getPlayer().lastTickPosY + (Player.getY() - getPlayer().lastTickPosY) * (double)getMinecraft().getTimer().renderPartialTicks, 
				getPlayer().lastTickPosZ + (Player.getZ() - getPlayer().lastTickPosZ) * (double)getMinecraft().getTimer().renderPartialTicks
		});
	}

	public static void enableDefaults() {
		getMinecraft().entityRenderer.disableLightmap(1.0D);
		GL11.glEnable(3042 /*GL_BLEND*/);
		GL11.glDisable(3553 /*GL_TEXTURE_2D*/);
		GL11.glDisable(2896 /*GL_LIGHTING*/);
		GL11.glDisable(2929 /*GL_DEPTH_TEST*/);
		GL11.glDepthMask(false);
		GL11.glBlendFunc(770, 771);
		GL11.glEnable(2848 /*GL_LINE_SMOOTH*/);
		GL11.glPushMatrix();
	}

	public static void disableDefaults() {
		GL11.glPopMatrix();
		GL11.glDisable(2848 /*GL_LINE_SMOOTH*/);
		GL11.glDepthMask(true);
		GL11.glEnable(2929 /*GL_DEPTH_TEST*/);
		GL11.glEnable(3553 /*GL_TEXTURE_2D*/);
		GL11.glEnable(2896 /*GL_LIGHTING*/);
		GL11.glDisable(3042 /*GL_BLEND*/);
		getMinecraft().entityRenderer.enableLightmap(1.0D);
	}

	public static void glColor4Hex(int var0) {
		GL11.glColor4f(Colours.getRGBA(var0)[0], Colours.getRGBA(var0)[1], Colours.getRGBA(var0)[2], Colours.getRGBA(var0)[3]);
	}

	public static Client getInstance() {
		if(instance == null) {
			instance = new Client();
		}

		return instance;
	}
}