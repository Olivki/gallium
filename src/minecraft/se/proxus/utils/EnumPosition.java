package se.proxus.utils;

public enum EnumPosition {
	
	LEFT(1),
	RIGHT(Wrapper.getScaledResolution().getScaledWidth()),
	TOP(1),
	BOTTOM(Wrapper.getScaledResolution().getScaledHeight()),
	MIDDLEX(Wrapper.getScaledResolution().getScaledWidth() / 2),
	MIDDLEY(Wrapper.getScaledResolution().getScaledHeight() / 2);
	
	int position;
	
	private EnumPosition(int position) {
		setPosition(position);
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
}