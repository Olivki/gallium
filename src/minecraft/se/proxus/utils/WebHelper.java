package se.proxus.utils;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;

import net.minecraft.src.*;
import se.proxus.*;

public class WebHelper extends Gallium {
	
	public static void loginToAccount(String username, String password) {
		try {
			URL url = new URL(String.format("http://login.minecraft.net/?user=%s&password=%s&version=69", username, password));
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
			String as[] = in.readLine().split(":");
			Wrapper.getMinecraft().session = new Session(as[2], as[3]);
		} catch (Exception e) { }
	}
	
	public static void openSite(String url) {
		Desktop desktop = Desktop.getDesktop();

		try {
			URI uri = new URI(url);
			desktop.browse(uri);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}