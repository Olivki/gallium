package se.proxus.utils;

import net.minecraft.src.FontRenderer;
import net.minecraft.src.Tessellator;

import org.lwjgl.opengl.GL11;

import se.proxus.*;

public class Utils2D extends Gallium {
	
	protected void drawHorizontalLine(float x, float f, float y, int par4)
    {
        if (f < x)
        {
            float var5 = x;
            x = f;
            f = var5;
        }

        drawRect(x, y, f + 1, y + 1, par4);
    }

    protected void drawVerticalLine(float x, float y, float f, int par4)
    {
        if (f < y)
        {
            float var5 = y;
            y = f;
            f = var5;
        }

        drawRect(x, y + 1, x + 1, f, par4);
    }

    /**
     * Draws a solid color rectangle with the specified coordinates and color.
     */
    public static void drawRect(float x, float f, float g, float f2, int par4)
    {
        float var5;

        if (x < g)
        {
            var5 = x;
            x = g;
            g = var5;
        }

        if (f < f2)
        {
            var5 = f;
            f = f2;
            f2 = var5;
        }

        float var10 = (float)(par4 >> 24 & 255) / 255.0F;
        float var6 = (float)(par4 >> 16 & 255) / 255.0F;
        float var7 = (float)(par4 >> 8 & 255) / 255.0F;
        float var8 = (float)(par4 & 255) / 255.0F;
        Tessellator var9 = Tessellator.instance;
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glColor4f(var6, var7, var8, var10);
        var9.startDrawingQuads();
        var9.addVertex((double)x, (double)f2, 0.0D);
        var9.addVertex((double)g, (double)f2, 0.0D);
        var9.addVertex((double)g, (double)f, 0.0D);
        var9.addVertex((double)x, (double)f, 0.0D);
        var9.draw();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
    }

    /**
     * Draws a rectangle with a vertical gradient between the specified colors.
     */
    protected void drawGradientRect(float f, float g, float h, float i, int par5, int par6)
    {
        float var7 = (float)(par5 >> 24 & 255) / 255.0F;
        float var8 = (float)(par5 >> 16 & 255) / 255.0F;
        float var9 = (float)(par5 >> 8 & 255) / 255.0F;
        float var10 = (float)(par5 & 255) / 255.0F;
        float var11 = (float)(par6 >> 24 & 255) / 255.0F;
        float var12 = (float)(par6 >> 16 & 255) / 255.0F;
        float var13 = (float)(par6 >> 8 & 255) / 255.0F;
        float var14 = (float)(par6 & 255) / 255.0F;
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        Tessellator var15 = Tessellator.instance;
        var15.startDrawingQuads();
        var15.setColorRGBA_F(var8, var9, var10, var7);
        var15.addVertex((double)h, (double)g, (double)0.0F);
        var15.addVertex((double)f, (double)g, (double)0.0F);
        var15.setColorRGBA_F(var12, var13, var14, var11);
        var15.addVertex((double)f, (double)i, (double)0.0F);
        var15.addVertex((double)h, (double)i, (double)0.0F);
        var15.draw();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }
    
    public void drawCenteredString(FontRenderer par1FontRenderer, String par2Str, float x, float f, int par5) {
        par1FontRenderer.drawStringWithShadow(par2Str, x - par1FontRenderer.getStringWidth(par2Str) / 2, f, par5);
    }
    
	public void drawBaseBorderedRect(float x, float y, float x2, float y2, int col1, int col2) {
		drawRect(x + 1, y + 1, x2 - 1, y2 - 1, col2);
		drawVerticalLine(x, y, y2 - 1, col1);
		drawVerticalLine(x2 - 1, y, y2 - 1, col1);
		drawHorizontalLine(x, x2 - 1, y, col1);
		drawHorizontalLine(x, x2 - 1, y2 -1, col1);
	}
	
	public void drawBaseHollowRect(float x, float y, float x2, float y2, int col1) {
		drawVerticalLine(x, y, y2 - 1, col1);
		drawVerticalLine(x2 - 1, y, y2 - 1, col1);
		drawHorizontalLine(x, x2 - 1, y, col1);
		drawHorizontalLine(x, x2 - 1, y2 -1, col1);
	}
	
	public void drawBaseGradientRect(float x, float y, float x2, float y2, int col1, int col2, int col3) {
		drawGradientRect(x + 1, y + 1, x2 - 1, y2 - 1, col2, col3);
		drawVerticalLine(x, y, y2 - 1, col1);
		drawVerticalLine(x2 - 1, y, y2 - 1, col1);
		drawHorizontalLine(x, x2 - 1, y, col1);
		drawHorizontalLine(x, x2 - 1, y2 -1, col1);
	}
	
	public void drawBorderedRect(float x, float y, float x2, float y2, int col1, int col2) {
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawBaseBorderedRect(x * 2, y * 2, x2 * 2, y2 * 2, col1, col2);
		GL11.glScalef(2.0F, 2.0F, 2.0F);
	}
	
	public void drawHollowRect(float x, float y, float x2, float y2, int col1) {
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawBaseHollowRect(x * 2, y * 2, x2 * 2, y2 * 2, col1);
		GL11.glScalef(2.0F, 2.0F, 2.0F);
	}
	
	public void drawGradientBorderedRect(float x, float y, float x2, float y2, int col1, int col2, int col3) {
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawBaseGradientRect(x * 2, y * 2, x2 * 2, y2 * 2, col1, col2, col3);
		GL11.glScalef(2.0F, 2.0F, 2.0F);
	}
	
	public void drawTinyString(String text, float x, float y, int type, int hex) {
		x *= 2;
		y *= 2;
		
		GL11.glPushMatrix();
		GL11.glScaled(0.5D, 0.5D, 0.5D);
		switch(type) {
		case 0:
			Wrapper.getFontRenderer().drawString(text, x, y, hex);
			break;
			
		case 1:
			Wrapper.getFontRenderer().drawStringWithShadow(text, x, y, hex);
			break;
		}
		GL11.glPopMatrix();
	}
	
	public void drawCenteredTinyString(String text, float x, float y, int type, int hex) {
		x *= 2;
		y *= 2;
		
		GL11.glPushMatrix();
		GL11.glScaled(0.5D, 0.5D, 0.5D);
		switch(type) {
		case 0:
			drawCenteredString(Wrapper.getFontRenderer(), text, x, y, hex);
			break;
		}
		GL11.glPopMatrix();
	}
	
	public void drawOutlinedString(String text, float x, float y, int hex) {
		Wrapper.getFontRenderer().renderString(text, x + 0.8F, y, hex, true);
		Wrapper.getFontRenderer().renderString(text, x, y + 0.8F, hex, true);
		Wrapper.getFontRenderer().renderString(text, x - 0.8F, y, hex, true);
		Wrapper.getFontRenderer().renderString(text, x, y - 0.8F, hex, true);
		Wrapper.getFontRenderer().drawString(text, x, y, hex);
	}
	
    /**
     * Draws a textured rectangle at the stored z-value. Args: x, y, u, v, width, height
     */
    public void drawTexturedModalRect(int par1, int par2, int par3, int par4, int par5, int par6)
    {
        float var7 = 0.00390625F;
        float var8 = 0.00390625F;
        Tessellator var9 = Tessellator.instance;
        var9.startDrawingQuads();
        var9.addVertexWithUV((double)(par1 + 0), (double)(par2 + par6), (double)0, (double)((float)(par3 + 0) * var7), (double)((float)(par4 + par6) * var8));
        var9.addVertexWithUV((double)(par1 + par5), (double)(par2 + par6), (double)0, (double)((float)(par3 + par5) * var7), (double)((float)(par4 + par6) * var8));
        var9.addVertexWithUV((double)(par1 + par5), (double)(par2 + 0), (double)0, (double)((float)(par3 + par5) * var7), (double)((float)(par4 + 0) * var8));
        var9.addVertexWithUV((double)(par1 + 0), (double)(par2 + 0), (double)0, (double)((float)(par3 + 0) * var7), (double)((float)(par4 + 0) * var8));
        var9.draw();
    }
	
	public void drawImage(float x, float y, int imgW, int imgH, String img, float alpha) {
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, Wrapper.getRenderEngine().getTexture(img));
		GL11.glColor4f(1.0F, 1.0F, 1.0F, alpha);
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, 0);
		drawTexturedModalRect(0, 0, 0, 0, imgW, imgH);
		GL11.glPopMatrix();
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL11.GL_BLEND);
	}
	
	public static void drawBorderedRect(float x, float y, float x2, float y2, float l1, int col1, int col2) {
		drawRect(x, y, x2, y2, col2);

		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(l1);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}
}