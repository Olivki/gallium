package se.proxus.utils;

import se.proxus.Gallium;

public class Wrapper {
	
	public static Client getClient() {
		return Client.getInstance();
	}
	
	public static Player getPlayer() {
		return Player.getInstance();
	}
	
	public static Gallium getGallium() {
		return Gallium.getInstance();
	}
}