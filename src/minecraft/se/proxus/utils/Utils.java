package se.proxus.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

import net.minecraft.src.*;
import se.proxus.*;

public class Utils extends Gallium {

	public void log(String type, String message) {
		System.out.println("[" + type + "] [Gallium]: " + message);
	}

	public void addMessage(String message) {
		Wrapper.getPlayer().addChatMessage(Colours.WHITE + "[" + Colours.RED + "Gallium" 
				+ Colours.WHITE + "]: " + message);
	}

	public void sendPacket(Packet packet) {
		Wrapper.getSendQueue().addToSendQueue(packet);
	}

	public void sendMessage(String message) {
		sendPacket(new Packet3Chat(message));
	}

	public double getDistanceToEntity(Entity e, EntityPlayerSP ep) {
		return Math.sqrt((ep.posX - e.posX) * (ep.posX - e.posX) + (ep.posZ - e.posZ) * (ep.posZ - e.posZ));
	}

	public Object getFixedObject(String var0) {
		Object var1 = null;

		try {
			if(var0.endsWith("F")) {
				var1 = Float.parseFloat(var0.replace("F", ""));
			} if(var0.endsWith("I")) {
				var1 = Integer.parseInt(var0.replace("I", ""));
			} if(var0.endsWith("B")) {
				var1 = Boolean.parseBoolean(var0.replace("B", ""));
			} if(var0.endsWith("L")) {
				var1 = Long.parseLong(var0.replace("L", ""));
			} if(var0.endsWith("D")) {
				var1 = Double.parseDouble(var0.replace("D", ""));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return var1;
	}

	public String setFixedObject(Object var0) {
		String var1 = null;

		try {
			if(var0 instanceof Float) {
				var1 = var0 + "F";
			} if(var0 instanceof Integer) {
				var1 = var0 + "I";
			} if(var0 instanceof Boolean) {
				var1 = var0 + "B";
			} if(var0 instanceof Long) {
				var1 = var0 + "L";
			} if(var0 instanceof Double) {
				var1 = var0 + "D";
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return var1;
	}
	
	public void playSound() {
		Wrapper.getMinecraft().sndManager.playSoundFX("random.click", 1.0F, 1.0F);
	}
	
	public int getDirection() {
		return (int)MathHelper.floor_double((double)(Wrapper.getPlayer().rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
	}

	public void jump(EntityLiving entity) {
		entity.motionY = 0.41999998688697815D;

		if(entity.isPotionActive(Potion.jump)) {
			entity.motionY += (double)((float)(entity.getActivePotionEffect(Potion.jump).getAmplifier() + 1) * 0.1F);
		}

		if(entity.isSprinting()) {
			float var1 = entity.rotationYaw * 0.017453292F;
			entity.motionX -= (double)(MathHelper.sin(var1) * 0.2F);
			entity.motionZ += (double)(MathHelper.cos(var1) * 0.2F);
		}

		entity.isAirBorne = true;

		if(Reflector.hasClass(9)) {
			Reflector.callVoid(97, new Object[] {this});
		}
	}
	
	public String getDistance(Entity e, EntityPlayerSP ep) {
		DecimalFormat df = new DecimalFormat();
		df.setMinimumFractionDigits(1);
		df.setMaximumFractionDigits(1);

		return df.format(getDistanceToEntity(e, ep)).replace(",", ".");
	}
	
	public String [] wrapText(String text, int len) {
		if(text == null) {
			return new String [] {};
		}

		if(len <= 0) {
			return new String [] {text};
		}

		if(text.length() <= len) {
			return new String [] {text};
		}

		char[] chars = text.toCharArray();

		Vector lines = new Vector();

		StringBuffer line = new StringBuffer();

		StringBuffer word = new StringBuffer();

		for(int i = 0; i < chars.length; i++) {
			word.append(chars[i]);

			if(chars[i] == ' ') {
				if((line.length() + word.length()) > len) {
					lines.add(line.toString());
					line.delete(0, line.length());
				}

				line.append(word);
				word.delete(0, word.length());
			}
		}

		if(word.length() > 0) {
			if((line.length() + word.length()) > len) {
				lines.add(line.toString());
				line.delete(0, line.length());
			}

			line.append(word);
		}

		if(line.length() > 0) {
			lines.add(line.toString());
		}

		String[] ret = new String[lines.size()];

		int c = 0;

		for(Enumeration e = lines.elements(); e.hasMoreElements(); c++) {
			ret[c] = (String) e.nextElement();
		}

		return ret;
	}
}