package se.proxus.panels;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import se.proxus.mods.*;
import se.proxus.panels.list.buttons.ButtonMod;
import se.proxus.utils.*;


public class PanelBase {

	protected String name;

	protected int x;

	protected int y;

	protected int width;

	protected int height;

	protected int dragX;

	protected int dragY;

	protected int rectHeight;

	protected boolean open;

	protected boolean pinned;

	protected boolean dragging;

	protected ArrayList<ButtonBase> loadedButtons;

	public PanelBase(String name, int x, int y, int width, int height) {
		setName(name);
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
		setLoadedButtons(new ArrayList<ButtonBase>());
		initButtons();
	}

	public PanelBase(String name, int x, int y) {
		this(name, x, y, 118, 14);
	}

	public void initButtons() {
		for(ModBase mod : Wrapper.getGallium().getMods().getLoadedMods()) {
			if(mod.getType().getName().equalsIgnoreCase(getName())) {
				addButton(new ButtonMod(mod.getName(), getWidth() - 4, 12, this, mod));
			}
		}
	}

	public void draw(int x, int y) {
		mouseDragged(x, y);

		if(getX() <= 0) {
			setX(0);
		} if(getY() <= 0) {
			setY(0);
		} if(getY() >= (Client.getScaledHeight() - getHeight()) && !(isOpen())) {
			setY(Client.getScaledHeight() - getHeight());
		} if(getY() >= (Client.getScaledHeight() - getHeight() - getRectHeight()) - 1 && isOpen()) {
			setY(Client.getScaledHeight() - getHeight() - getRectHeight() - 1);
		} if(getX() >= (Client.getScaledWidth() - getWidth())) {
			setX(Client.getScaledWidth() - getWidth());
		}

		if(isOpen()) {
			drawGradientBorderedRect(getX(), getY() + getHeight() + 1, getX() + getWidth(), getY() + getHeight() + getRectHeight() + 1, 
					0xFFA1A1A1, 0xFFE8E8E8, 0xFFCCCCCC);
		} if(Client.getMinecraft().currentScreen instanceof PanelHandler) {
			drawGradientBorderedRect(getX(), getY(), getX() + getWidth(), getY() + getHeight(), 0xFFA1A1A1, 0xFFE8E8E8, 0xFFCCCCCC);
			Client.drawStringPanel(getName(), getX() + 3, getY() + 3, 0xFF474747);
			drawBorderedRect(getX() + getWidth() - 12, getY() + 2, getX() + getWidth() - 2, getY() + getHeight() - 2, 1.5F, 0xFF363636, 
					(isPinned() ? 0xFF8C8C8C : 0xFF757575));
		}

		if(isOpen()) {
			setRectHeight(getLoadedButtons().size() * 14 + 2);
			for(int var0 = 0; var0 < getLoadedButtons().size(); var0++) {
				ButtonBase button = (ButtonBase)getLoadedButtons().get(var0);

				button.setX(getX() + 2);
				button.setY(getY() + getHeight() + 3 + var0 * 14);
				button.draw(x, y);
			}
		}
	}

	public void mouseClicked(int x, int y, int type) {
		if(isHovering(x, y) && !(isObstructed(x, y)) && type == 0) {
			setDragX(x - getX());
			setDragY(y - getY());
			setDragging(true);
			Wrapper.getGallium().getPanels().setCurrentPanel(this, true);
			Client.playSound(1.0F);
		} if(isHovering(x, y) && !(isObstructed(x, y)) && type == 1) {
			setOpen(!(isOpen()));
			Wrapper.getGallium().getPanels().setCurrentPanel(this, true);
			Client.playSound(1.0F);
		} if(isHoveringOverPin(x, y) && !(isObstructed(x, y)) 
				&& type == 0) {
			setPinned(!(isPinned()));
			Wrapper.getGallium().getPanels().setCurrentPanel(this, true);
			Client.playSound(1.0F);
		} if(isOpen()) {
			for(ButtonBase button : getLoadedButtons()) {
				button.mouseClickedBefore(x, y, type);
			}
		}
	}

	public void mouseDragged(int x, int y) {
		if(isDragging() && Mouse.isButtonDown(0)) {
			setX(x - getDragX());
			setY(y - getDragY());
		} else {
			setDragging(false);
		}
	}

	/**
	 * @author WTG
	 **/
	public boolean isObstructed(int x, int y) {
		for(PanelBase panel : Wrapper.getGallium().getPanels().getLoadedPanels()) {
			if(panel == this) continue;

			if(!(panel.isOpen())){
				if(x >= panel.getX() && x <= panel.getX() + panel.getWidth() && y >= panel.getY() && y <= panel.getY() + 12 
						&& getIndex(panel.getName()) > getIndex(getName())) {
					return true;
				}
			} else {
				if(x >= panel.getX() && x <= panel.getX() + panel.getWidth() && y >= panel.getY() && y <= panel.getY() + panel.getHeight() 
						&& getIndex(panel.getName()) > 
				getIndex(getName())) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @author WTG
	 **/
	public int getIndex(String name) {
		for(int var1 = 0 ; var1 < Wrapper.getGallium().getPanels().getLoadedPanels().size(); var1++){
			PanelBase var2 = Wrapper.getGallium().getPanels().getLoadedPanels().get(var1);

			if(name.equalsIgnoreCase(var2.getName())) {
				return var1;
			}
		}
		return -1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getDragX() {
		return dragX;
	}

	public void setDragX(int dragX) {
		this.dragX = dragX;
	}

	public int getDragY() {
		return dragY;
	}

	public void setDragY(int dragY) {
		this.dragY = dragY;
	}

	public int getRectHeight() {
		return rectHeight;
	}

	public void setRectHeight(int rectHeight) {
		this.rectHeight = rectHeight;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean isPinned() {
		return pinned;
	}

	public void setPinned(boolean pinned) {
		this.pinned = pinned;
	}

	public boolean isDragging() {
		return dragging;
	}

	public void setDragging(boolean dragging) {
		this.dragging = dragging;
	}

	public ArrayList<ButtonBase> getLoadedButtons() {
		return loadedButtons;
	}

	public void setLoadedButtons(ArrayList<ButtonBase> loadedButtons) {
		this.loadedButtons = loadedButtons;
	}

	public ButtonBase addButton(ButtonBase button) {
		if(!(getLoadedButtons().contains(button))) {
			getLoadedButtons().add(button);
		}

		return button;
	}

	public boolean isHovering(int x, int y) {
		return x >= getX() && y >= getY() && x <= getX() + getWidth() - 14 && y <= getY() + getHeight();
	}

	public boolean isHoveringOverPin(int x, int y) {
		return x >= getX() + getWidth() - 12 && y >= getY() + 2 && x <= getX() + getWidth() - 2 && y <= getY() + getHeight() - 2;
	}

	public void drawBorderedRect(float x, float y, float width, float height, int hex1, int hex2) {
		Client.getIngameGui().drawRect(x + 1, y + 1, width - 1, height - 1, hex2);
		Client.getIngameGui().drawVerticalLine(x, y, height - 1, hex1);
		Client.getIngameGui().drawVerticalLine(width - 1, y, height - 1, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, y, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, height - 1, hex1);
	}

	public void drawGradientBorderedRect(float x, float y, float width, float height, int hex1, int hex2, int hex3) {
		Client.getIngameGui().drawGradientRect(x + 1, y + 1, width - 1, height - 1, hex2, hex3);
		Client.getIngameGui().drawVerticalLine(x, y, height - 1, hex1);
		Client.getIngameGui().drawVerticalLine(width - 1, y, height - 1, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, y, hex1);
		Client.getIngameGui().drawHorizontalLine(x, width - 1, height - 1, hex1);
	}

	public static void drawBorderedRect(float x, float y, float width, float height, float lineWidth, int hex1, int hex2) {
		Client.getIngameGui().drawRect(x, y, width, height, hex2);
		float f = (float)(hex1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(hex1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(hex1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(hex1 & 0xFF) / 255F;
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(lineWidth);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, height);
		GL11.glVertex2d(width, height);
		GL11.glVertex2d(width, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(width, y);
		GL11.glVertex2d(x, height);
		GL11.glVertex2d(width, height);
		GL11.glEnd();
		GL11.glPopMatrix();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}

	public void drawHollowBorderedRect(int x, int y, int width, int height, float lineWidth, int hex) {
		float f = (float)(hex >> 24 & 0xFF) / 255F;
		float f1 = (float)(hex >> 16 & 0xFF) / 255F;
		float f2 = (float)(hex >> 8 & 0xFF) / 255F;
		float f3 = (float)(hex & 0xFF) / 255F;
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(lineWidth);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, height);
		GL11.glVertex2d(width, height);
		GL11.glVertex2d(width, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(width, y);
		GL11.glVertex2d(x, height);
		GL11.glVertex2d(width, height);
		GL11.glEnd();
		GL11.glPopMatrix();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}
}