package se.proxus.panels;

import java.util.ArrayList;

import se.proxus.mods.BaseMod;
import se.proxus.mods.ModType;
import se.proxus.panels.list.buttons.ButtonMod;
import se.proxus.utils.Colours;

public class PanelColors {

	public int PANEL_BORDER_LIGHT = 0xFF2F2F2F;

	public int PANEL_BORDER_DARK = 0xFF121212;

	public int PANEL_BORDER = 0xFF5C5C5C;

	public int PANEL_INNER = 0xFF323232;

	public int PANEL_TOP_INNER = 0xFF373737;

	public int PANEL_RECTANGLE = 0x40000000;

	public int PANEL_NAME = 0xFFFFFFFF;

	public int PANEL_GRADIENT_BORDER = 0xFF000000;

	public int PANEL_GRADIENT_TOP = 0xFF222222;

	public int PANEL_GRADIENT_BOTTOM = 0xFF444445;

	private PanelInfo info;

	private BasePanel panel;

	public PanelColors(PanelInfo info, BasePanel panel) {
		this.info = info;
		this.panel = panel;
	}

	public void setColors(int var0, int var1) {
		
	}

	public int getActiveMods() {
		if(this.isModPanel()) {
			int activeMods = 0;

			for(BaseButton button : this.getPanel().getButtonArray()) {
				if(button instanceof ButtonMod) {
					ButtonMod mod = (ButtonMod)button;

					if(mod.getMod().getState()) {
						activeMods++;
					}
				}
			}

			return activeMods;
		}

		return -1;
	}

	public boolean isModPanel() {
		for(ModType type : ModType.values()) {
			if(this.getPanel().getName().equalsIgnoreCase(type.getName())) {
				return true;
			}
		}

		return false;
	}

	public PanelInfo getInfo() {
		return this.info;
	}

	public void setInfo(PanelInfo info) {
		this.info = info;
	}

	public BasePanel getPanel() {
		return this.panel;
	}

	public void setPanel(BasePanel panel) {
		this.panel = panel;
	}
}