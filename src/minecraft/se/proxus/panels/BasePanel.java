package se.proxus.panels;

import java.util.ArrayList;

import net.minecraft.src.*;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import se.proxus.mods.*;
import se.proxus.panels.list.buttons.*;
import se.proxus.utils.Colours;
import se.proxus.utils.Utils2D;
import se.proxus.utils.Wrapper;

public abstract class BasePanel extends Utils2D {

	protected String name;

	protected PanelInfo info;

	protected PanelColors colors;

	protected ArrayList<BaseButton> buttonArray;

	protected ButtonPanelTop[] buttons = new ButtonPanelTop[1];

	protected PanelConfig config;

	protected int gridIndex;

	protected int gridY;

	public BasePanel(String name, PanelInfo info) {
		setName(name);
		setInfo(info);
		setConfig(new PanelConfig(this, this));
		setColors(new PanelColors(getInfo(), this));
		setButtonArray(new ArrayList<BaseButton>());
		setButtonPin(new ButtonPanelTop("*", new ButtonInfo(getInfo().getX() + getInfo().getWidth() - 5,
				getInfo().getY() + 2, 3, 3), this, getInfo().isPinned()));
		getConfig().loadConfig();
		setupButtons();
	}

	public void setupButtons() {
		setButtonPin(new ButtonPanelTop("*", new ButtonInfo(getInfo().getX() + getInfo().getWidth() - 5,
				getInfo().getY() + 2, 3, 3), this, getInfo().isPinned()));

		for(BaseMod mod : Wrapper.getGallium().mods.loadedMods) {
			if(mod.getType().getName().equalsIgnoreCase(getName()) && mod != null) {
				addButton(new ButtonMod(new ButtonInfo(0, 0, getInfo().getWidth() - 4, 16), mod, this));
			}
		}
	}

	public void draw(int x, int y, FontRenderer font) {	
		/** DRAGGING START **/
		mouseDragged(x, y);
		/** DRAGGING STOP **/

		/** COLORS START **/
		getColors().setColors(x, y);
		/** COLORS STOP **/

		/** DRAWING START **/
		//drawRect(getInfo().getX() + 1, getInfo().getY() + 1, getInfo().getX() + getInfo().getWidth() + 0.5F, 
				//getInfo().getY() + getInfo().getHeight() + getInfo().getRectHeight() + 0.5F - (getInfo().isOpen() ? 0.5F : 0), 0xFF000000);

		/*drawBorderedRect(getInfo().getX(), getInfo().getY(), getInfo().getX() + getInfo().getWidth(), 
				getInfo().getY() + getInfo().getHeight() + getInfo().getRectHeight() - (getInfo().isOpen() ? 0.5F : 0), getColors().PANEL_BORDER, 
				getColors().PANEL_INNER);*/
		
		/*drawHollowRect(getInfo().getX(), getInfo().getY(), getInfo().getX() + getInfo().getWidth(), 
				getInfo().getY() + getInfo().getHeight(), 0x30000000);*/
		
		drawGradientBorderedRect(getInfo().getX(), getInfo().getY(), getInfo().getX() + getInfo().getWidth(), 
				getInfo().getY() + getInfo().getHeight() + (getInfo().isOpen() ? getInfo().getRectHeight() : -1), 0xFF1C4475, 0xFFDFDFDF, 0xFFCACACA);
		
		GL11.glPushMatrix();
		GL11.glScaled(0.5D, 0.5D, 0.5D);
		drawImage(getInfo().getX() * 2, getInfo().getY() * 2, 256, 100, 
				"/gallium/panel.png", 1.0F);
		GL11.glPopMatrix();

		//drawTinyString(getName(), getInfo().getX() + 1, getInfo().getY() + 1, 0, getColors().PANEL_NAME);
		ttf.drawStringWithShadow(getName(), getInfo().getX() + 2, getInfo().getY() + 3, getColors().PANEL_NAME);
		/** DRAWING STOP **/

		/** BUTTONS START **/
		handleButtons(x, y, font);
		/** BUTTONS STOP **/
	}

	public void mouseClicked(int x, int y, int type) {
		/** DRAGGING START **/
		if(isHovering(x, y) && !(isObstructed(x, y)) && type == 0) {
			Wrapper.getGallium().utils.playSound();
			getInfo().setDragX(x - getInfo().getX());
			getInfo().setDragY(y - getInfo().getY());
			getInfo().setDragging(true);
			Wrapper.getGallium().panels.setCurrentPanel(this, true);
			getConfig().saveConfig();
		}
		/** DRAGGING STOP **/

		/** BUTTONS START **/
		if(getInfo().isOpen()) {
			for(BaseButton var3 : getButtonArray()) {
				var3.mouseClickedBefore(x, y, type);
			}
		}

		handleTopButtonsPressed(x, y, type);
		/** BUTTONS STOP **/

		/** OPENING START **/
		if(isHovering(x, y) && !(isObstructed(x, y)) && type == 1) {
			Wrapper.getGallium().utils.playSound();
			getInfo().setOpened(!(getInfo().isOpen()));
			handleOpening(x, y);
			Wrapper.getGallium().panels.setCurrentPanel(this, true);
			getConfig().saveConfig();
		}
		/** OPENING STOP **/
	}

	public void handleTopButtonsPressed(int x, int y, int type) {
		getButtonPin().mouseClickedBefore(x, y, type);
	}

	public void mouseDragged(int x, int y) {
		/** DRAGGING START **/
		if(Mouse.isButtonDown(0) && getInfo().isDragging()) {
			getInfo().setX(x - getInfo().getDragX());
			getInfo().setY(y - getInfo().getDragY());
			getConfig().saveConfig();
		} else {
			getInfo().setDragging(false);
		}
		/** DRAGGING STOP **/

		/** LOGIC START **/
		handleLogic(x, y);
		/** LOGIC STOP **/
	}

	public void keyTyped(char keyChar, int key) {
		for(BaseButton buttons : getButtonArray()) {
			buttons.keyTyped(keyChar, key);
		} if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) && Keyboard.isKeyDown(Keyboard.KEY_M)) {
			getButtonArray().clear();
			setupButtons();
			utils.log("INFO", "Cleared all the panel buttons and re-added them.");
		}
	}

	public void handleLogic(int x, int y) {
		ScaledResolution var2 = Wrapper.getScaledResolution();

		if(getInfo().getX() <= 1) {
			getInfo().setX(1);
		} if(getInfo().getY() <= 1) {
			getInfo().setY(1);
		} if(getInfo().getY() >= (var2.getScaledHeight() - getInfo().getHeight()) && !(getInfo().isOpen())) {
			getInfo().setY(var2.getScaledHeight() - getInfo().getHeight());
		} if(getInfo().getY() >= (var2.getScaledHeight() - getInfo().getHeight() - getInfo().getRectHeight() - 1) 
				&& getInfo().isOpen()) {
			getInfo().setY(var2.getScaledHeight() - getInfo().getHeight() - getInfo().getRectHeight() - 1);
		} if(getInfo().getX() >= (var2.getScaledWidth() - getInfo().getWidth() - 1)) {
			getInfo().setX(var2.getScaledWidth() - getInfo().getWidth() - 1);
		}
	}

	public void handleOpening(int x, int y) {

	}

	public void handleButtons(int x, int y, FontRenderer font) {
		if(getInfo().isOpen()) {
			getInfo().setRectHeight(getButtonArray().size() * 16 + 2);

			for(int var0 = 0; var0 < getButtonArray().size(); var0++) {
				BaseButton button = (BaseButton)getButtonArray().get(var0);

				button.getInfo().setX(getInfo().getX() + 4);
				button.getInfo().setY(getInfo().getY() + getInfo().getHeight() + var0 * 16);
				button.draw(x, y, font);
			}
		} else {
			getInfo().setRectHeight(0);
		}

		if(getButtonPin() != null) {
			handleTopButtons(x, y, font);
		}
	}

	public void handleTopButtons(int x, int y, FontRenderer font) {
		getButtonPin().getInfo().setX(getInfo().getX() + getInfo().getWidth() - 5);
		getButtonPin().getInfo().setY(getInfo().getY() + 1);
		getButtonPin().getInfo().setState(getInfo().isPinned());

		getButtonPin().draw(x, y, font);
	}

	public void handleButtonPressed(BaseButton button) {

	}

	public void addButton(BaseButton button) {
		if(!(getButtonArray().contains(button))) {
			getButtonArray().add(getButtonArray().size(), button);
		}
	}

	/***
	 * @author WTG
	 ***/
	public boolean isObstructed(int x, int y) {
		for(BasePanel panel : Wrapper.getGallium().panels.panelArray) {
			if(panel == this) continue;

			if(!(panel.getInfo().isOpen())){
				if(x >= panel.getInfo().getX() && x <= panel.getInfo().getX() + panel.getInfo().getWidth() && y >= panel.getInfo().getY() && y <= panel.getInfo().getY() + 12 && getIndex(panel.getName()) > getIndex(getName())) {
					return true;
				}
			} else {
				if(x >= panel.getInfo().getX() && x <= panel.getInfo().getX() + panel.getInfo().getWidth() && y >= panel.getInfo().getY() && y <= panel.getInfo().getY() + panel.getInfo().getHeight() && getIndex(panel.getName()) > getIndex(getName())) {
					return true;
				}
			}
		}

		return false;
	}

	/***
	 * @author WTG
	 ***/
	public int getIndex(String name) {
		for(int var1 = 0 ; var1 < Wrapper.getGallium().panels.panelArray.size(); var1++){
			BasePanel var2 = Wrapper.getGallium().panels.panelArray.get(var1);

			if(name.equalsIgnoreCase(var2.getName())) {
				return var1;
			}
		}
		return -1;
	}

	public boolean isHovering(int x, int y) {
		return x >= getInfo().getX() && y >= getInfo().getY() && x <= getInfo().getX() + getInfo().getWidth() - 18 && y <= getInfo().getY() + getInfo().getHeight();
	}

	public String getName() {
		return name;
	}

	public PanelInfo getInfo() {
		return info;
	}

	public PanelColors getColors() {
		return colors;
	}

	public ArrayList<BaseButton> getButtonArray() {
		return buttonArray;
	}

	public ButtonPanelTop getButtonPin() {
		return buttons[0];
	}

	public PanelConfig getConfig() {
		return config;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setInfo(PanelInfo info) {
		this.info = info;
	}

	public void setColors(PanelColors colors) {
		this.colors = colors;
	}

	public void setButtonArray(ArrayList<BaseButton> buttonArray) {
		this.buttonArray = buttonArray;
	}

	public void setButtonPin(ButtonPanelTop buttonpaneltop) {
		buttons[0] = buttonpaneltop;
	}

	public void setConfig(PanelConfig config) {
		this.config = config;
	}

	public int getGridIndex() {
		return gridIndex;
	}

	public void setGridIndex(int gridIndex) {
		this.gridIndex = gridIndex;
	}

	public int getGridY() {
		return gridY;
	}

	public void setGridY(int gridY) {
		this.gridY = gridY;
	}
}