package se.proxus.panels;

import net.minecraft.src.*;
import se.proxus.utils.Utils2D;

public abstract class BaseButton extends Utils2D {
	
	protected String name;

	protected ButtonInfo info;
	
	protected ButtonColors colors;
	
	protected BasePanel panel;

	public BaseButton(String name, ButtonInfo info, BasePanel panel) {
		this.setName(name);
		this.setInfo(info);
		this.setPanel(panel);
		this.setColors(new ButtonColors(this.getInfo(), this));
		this.getInfo().setButton(this);
	}
	
	public abstract void draw(int x, int y, FontRenderer font);
	
	public void mouseClickedBefore(int x, int y, int type) {
		if(!(this.isObstructed(x, y)) && type == 0) {
			this.mouseClicked(x, y, type);
			this.getPanel().getConfig().saveConfig();
		}
	}
	
	public abstract void mouseClicked(int x, int y, int type);
	
	public boolean isHovering(int var0, int var1) {
		return var0 >= this.getInfo().getX() && var1 >= this.getInfo().getY() && var0 <= this.getInfo().getX() + this.getInfo().getWidth() && var1 <= this.getInfo().getY() + this.getInfo().getHeight();
	}
	
	public void keyTyped(char keyChar, int key) {
		
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ButtonInfo getInfo() {
		return this.info;
	}

	public void setInfo(ButtonInfo info) {
		this.info = info;
	}

	public ButtonColors getColors() {
		return this.colors;
	}

	public void setColors(ButtonColors colors) {
		this.colors = colors;
	}
	
	public BasePanel getPanel() {
		return this.panel;
	}

	public void setPanel(BasePanel panel) {
		this.panel = panel;
	}
	
	public boolean isObstructed(int var0, int var1) {
		for(BaseButton var2 : this.getPanel().getButtonArray()) {
			if(this != var2) {
				if(var0 >= var2.getInfo().getX() && var0 <= var2.getInfo().getX() + var2.getInfo().getWidth() && var1 >= var2.getInfo().getY() && var1 <= var2.getInfo().getY() + 12 && getIndex(var2.getName()) > getIndex(this.getName())) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public int getIndex(String var0) {
		for(int var1 = 0; var1 < this.getPanel().getButtonArray().size(); var1++) {
			BaseButton var2 = (BaseButton)this.getPanel().getButtonArray().get(var1);
			
			if(var2.getName().equalsIgnoreCase(var0)) {
				return var1;
			}
		}
		
		return -1;
	}
}