package se.proxus.panels;

public class ButtonColors {

	public int BUTTON_BORDER_LIGHT = 0xFF2F2F2F;

	public int BUTTON_BORDER_DARK = 0xFF121212;

	public int BUTTON_BORDER = 0xFF5C5C5C;

	public int BUTTON_INNER = 0xFF323232;

	public int BUTTON_DARKER_INNER = 0xFF373737;

	public int BUTTON_NAME = 0xFFFFFFFF;

	public int BUTTON_RECTANGLE = 0xFF696969;

	public int BUTTON_GRADIENT_TOP = 0xFFD61E1E;

	public int BUTTON_GRADIENT_BOTTOM = 0xFFB5090D;

	private ButtonInfo info;

	private BaseButton button;

	public ButtonColors(ButtonInfo info, BaseButton button) {
		setInfo(info);
		setButton(button);
	}

	public void setColors(int x, int y) {	
		BUTTON_NAME = getButton().getInfo().getState() ? 0xFF00CC00 : 0xFFFF3300;
		BUTTON_RECTANGLE = getButton().getInfo().getState() ? 0xFFB8B8B8 : 0xFF696969;
		BUTTON_GRADIENT_TOP = 0xFF666666;
	}

	public ButtonInfo getInfo() {
		return info;
	}

	public void setInfo(ButtonInfo info) {
		this.info = info;
	}

	public BaseButton getButton() {
		return button;
	}

	public void setButton(BaseButton button) {
		this.button = button;
	}
}