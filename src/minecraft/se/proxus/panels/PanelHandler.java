package se.proxus.panels;

import java.util.ArrayList;
import java.util.logging.Level;

import se.proxus.mods.*;
import se.proxus.panels.list.panels.PanelPanels;
import se.proxus.utils.*;


import net.minecraft.src.*;

public class PanelHandler extends GuiScreen {

	private ArrayList<PanelBase> loadedPanels = new ArrayList<PanelBase>();

	private ArrayList<PanelBase> loadedQueuePanels = new ArrayList<PanelBase>();

	private PanelBase currentPanel;

	public void initPanelHandler() {
		try {
			for(int var0 = ModType.values().length - 1; var0 > -ModType.values().length - 1; --var0) {
				ModType type = ModType.values()[var0];
				if(type != ModType.NONE) {
					addQueuePanel(new PanelBase(type.getName(), 2, 2));
				}
			}
		} catch(Exception exception) {
			exception.printStackTrace();
		}

		addPanel(new PanelPanels(2, 2));

		Wrapper.getGallium().getLogger().log(Level.INFO, (getLoadedPanels().size() + getLoadedQueuePanels().size()) 
				+ " panel(s) have been loaded!");
	}

	@Override
	public void drawScreen(int x, int y, float ticks) {
		for(PanelBase panel : getLoadedPanels()) {
			panel.draw(x, y);
		}
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		try {
			for(PanelBase panel : getLoadedPanels()) {
				panel.mouseClicked(x, y, type);
			}
		} catch(Exception exception) {

		}
	}

	public PanelBase addPanel(PanelBase panel) {
		if(!(getLoadedPanels().contains(panel))) {
			getLoadedPanels().add(getLoadedPanels().size(), panel);
			Wrapper.getGallium().getLogger().log(Level.INFO, "Added the panel: " + panel.getName() + "!");
		}

		return panel;
	}

	public PanelBase removePanel(PanelBase panel) {
		if(getLoadedPanels().contains(panel)) {
			getLoadedPanels().remove(getLoadedPanels().indexOf(panel));
			Wrapper.getGallium().getLogger().log(Level.INFO, "Removed the panel: " + panel.getName() + "!");
		}

		return panel;
	}

	public PanelBase addQueuePanel(PanelBase panel) {
		if(!(getLoadedQueuePanels().contains(panel))) {
			getLoadedQueuePanels().add(getLoadedPanels().size(), panel);
			Wrapper.getGallium().getLogger().log(Level.INFO, "Added the queue panel: " + panel.getName() + "!");
		}

		return panel;
	}

	public PanelBase setCurrentPanel(PanelBase panel, boolean sendToTop) {
		if(sendToTop) {
			getLoadedPanels().remove(getLoadedPanels().indexOf(panel));
			getLoadedPanels().add(getLoadedPanels().size(), panel);
		}

		setCurrentPanel(panel);

		return panel;
	}

	public ArrayList<PanelBase> getLoadedPanels() {
		return loadedPanels;
	}

	public void setLoadedPanels(ArrayList<PanelBase> loadedPanels) {
		this.loadedPanels = loadedPanels;
	}

	public ArrayList<PanelBase> getLoadedQueuePanels() {
		return loadedQueuePanels;
	}

	public void setLoadedQueuePanels(ArrayList<PanelBase> loadedQueuePanels) {
		this.loadedQueuePanels = loadedQueuePanels;
	}

	public PanelBase getCurrentPanel() {
		return currentPanel;
	}

	public void setCurrentPanel(PanelBase currentPanel) {
		this.currentPanel = currentPanel;
	}
}