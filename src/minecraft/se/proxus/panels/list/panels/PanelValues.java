package se.proxus.panels.list.panels;

import java.io.File;
import java.util.ArrayList;

import se.proxus.panels.*;
import se.proxus.panels.list.buttons.*;
import se.proxus.utils.Wrapper;

public class PanelValues extends BasePanel {

	public PanelValues(PanelInfo info) {
		super("Values", info);
	}

	@Override
	public void setupButtons() {
		addButton(new ButtonSlider("Timer Speed", new ButtonInfo(0, 0, getInfo().getWidth() - 4, 9), this, 
				mods.getMod(18), 50, 0));
	}
}