package se.proxus.panels.list.panels;

import se.proxus.panels.*;
import se.proxus.panels.list.buttons.*;
import se.proxus.utils.Wrapper;

public class PanelPanels extends PanelBase {

	public PanelPanels(int x, int y) {
		super("Panels", x, y);
	}
	
	@Override
	public void initButtons() {
		for(PanelBase panel : Wrapper.getGallium().getPanels().getLoadedQueuePanels()) {
			addButton(new ButtonPanel(panel.getName(), getWidth() - 4, 12, this, panel));
		}
	}
}