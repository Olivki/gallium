package se.proxus.panels.list.panels;

import java.io.File;
import java.util.ArrayList;

import se.proxus.panels.*;
import se.proxus.panels.list.buttons.*;
import se.proxus.utils.Wrapper;

public class Panels extends BasePanel {

	public Panels(PanelInfo info) {
		super("Panels", info);
	}

	@Override
	public void setupButtons() {
		for(BasePanel panel : Wrapper.getGallium().panels.panelQueueArray) {
			addButton(new ButtonPanel(new ButtonInfo(0, 0, getInfo().getWidth() - 4, 15), this, panel));
		}
	}
}