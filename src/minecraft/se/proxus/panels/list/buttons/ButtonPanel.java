package se.proxus.panels.list.buttons;

import se.proxus.mods.*;
import se.proxus.panels.*;
import se.proxus.utils.Client;
import se.proxus.utils.Colours;
import se.proxus.utils.Wrapper;

public class ButtonPanel extends ButtonBase {

	private PanelBase extraPanel;

	public ButtonPanel(String name, int width, int height, PanelBase panel, PanelBase extraPanel) {
		super(name, 0, 0, width, height, panel);
		setExtraPanel(extraPanel);
	}

	@Override
	public void draw(int x, int y) {
		drawGradientBorderedRect(getX(), getY(), getX() + getWidth(), getY() + getHeight(), 0xFFA1A1A1, 
				(Wrapper.getGallium().getPanels().getLoadedPanels().contains(getExtraPanel()) ? 0xFF5FF569 : 0xFFE60000), 
				(Wrapper.getGallium().getPanels().getLoadedPanels().contains(getExtraPanel()) ? 0xFF42AC4A : 0xFF930000));
		Client.getIngameGui().drawCenteredStringPanel(Client.getFontRenderer(), getExtraPanel().getName(), getX() + getWidth() / 2, 
				getY() + 2.5F, (Wrapper.getGallium().getPanels().getLoadedPanels().contains(getExtraPanel()) ? 0xFF474747 : 0xFFB7B7B7));
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		if(isHovering(x, y) && type == 0) {
			if(!(Wrapper.getGallium().getPanels().getLoadedPanels().contains(getExtraPanel()))) {
				Wrapper.getGallium().getPanels().addPanel(getExtraPanel());
			} else if(Wrapper.getGallium().getPanels().getLoadedPanels().contains(getExtraPanel())) {
				Wrapper.getGallium().getPanels().removePanel(getExtraPanel());
			}
			Client.playSound(1.0F);
		}
	}

	public PanelBase getExtraPanel() {
		return extraPanel;
	}

	public void setExtraPanel(PanelBase panel) {
		this.extraPanel = panel;
	}

	@Override
	public boolean isHovering(int x, int y) {
		return x >= getX() && y >= getY() && x <= getX() + getWidth() && y <= getY() + getHeight();
	}
}