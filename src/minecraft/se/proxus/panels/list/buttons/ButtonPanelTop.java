package se.proxus.panels.list.buttons;

import net.minecraft.src.FontRenderer;
import se.proxus.*;
import se.proxus.panels.*;
import se.proxus.panels.list.panels.*;
import se.proxus.utils.Wrapper;

public class ButtonPanelTop extends BaseButton {

	protected int BUTTON_REMOVE = 0xFFCD0000;

	protected BasePanel panel;

	protected boolean newState;

	public ButtonPanelTop(String name, ButtonInfo info, BasePanel panel, boolean newState) {
		super(name, info, panel);
	}

	@Override
	public void draw(int x, int y, FontRenderer font) {
		/** COLORS START **/
		getColors().setColors(x, y);
		/** COLORS STOP **/

		/** DRAWING START **/
		drawHollowRect(getInfo().getX() + 0.5F, getInfo().getY() + 0.5F, getInfo().getX() + getInfo().getWidth() + 0.5F, 
				getInfo().getY() + getInfo().getHeight() + 0.5F, getColors().BUTTON_RECTANGLE);
		drawHollowRect(getInfo().getX() + 0.5F, getInfo().getY() + 1.0F, getInfo().getX() + getInfo().getWidth() + 0.5F, 
				getInfo().getY() + getInfo().getHeight() + 0.5F, getColors().BUTTON_RECTANGLE);
		/** DRAWING STOP **/
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		if(isHovering(x, y) && type == 0) {
			if(getName().equalsIgnoreCase("X") && !(getPanel() instanceof Panels)) {
				Gallium.utils.playSound();
				getInfo().setState(!(getInfo().getState()));
				Wrapper.getGallium().panels.removePanel(getPanel());
				Wrapper.getGallium().panels.setCurrentPanel(Wrapper.getGallium().panels.getPanel("Panels"), true);
			} else if(!(getName().equalsIgnoreCase("X"))) {
				if(getName().equalsIgnoreCase("_")) {
					Gallium.utils.playSound();
					getInfo().setState(!(getInfo().getState()));
					getPanel().getInfo().setOpened(getInfo().getState());
					Wrapper.getGallium().panels.setCurrentPanel(getPanel(), true);
				} if(getName().equalsIgnoreCase("*")) {
					Gallium.utils.playSound();
					getInfo().setState(!(getInfo().getState()));
					getPanel().getInfo().setPinned(getInfo().getState());
					Wrapper.getGallium().panels.setCurrentPanel(getPanel(), true);
				}
			}
		}
	}

	public boolean getNewState() {
		return newState;
	}
	
	public void setNewState(boolean newState) {
		this.newState = newState;
	}

	public boolean isHovering(int x, int y) {
		return x >= getInfo().getX() - 1 && y >= getInfo().getY() - 2 && x <= getInfo().getX() + getInfo().getWidth() + 2 && y 
				<= getInfo().getY() + getInfo().getHeight() + 1;
	}
}