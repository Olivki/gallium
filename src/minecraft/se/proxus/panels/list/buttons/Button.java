package se.proxus.panels.list.buttons;

import org.lwjgl.opengl.GL11;

import net.minecraft.src.*;
import se.proxus.*;
import se.proxus.mods.*;
import se.proxus.panels.*;

public class Button extends BaseButton {

	public Button(String name, ButtonInfo info, BasePanel panel) {
		super(name, info, panel);
	}

	@Override
	public void draw(int x, int y, FontRenderer font) {
		/** COLORS START **/
		getColors().setColors(x, y);
		/** COLORS STOP **/

		/** DRAWING START **/	
		/*drawBorderedRect(getInfo().getX() + getInfo().getWidth() - 18, getInfo().getY() + 1, getInfo().getX() 
				+ getInfo().getWidth() - 1.5F, getInfo().getY() + getInfo().getHeight() - 1, getColors().BUTTON_BORDER, getColors().BUTTON_INNER);

		drawTinyString(getInfo().getState() ? "On" : "Off", getInfo().getX() + getInfo().getWidth() - 9.5F 
				- font.getStringWidth(getInfo().getState() ? "On" : "Off") / 4, getInfo().getY() + 2.5F, 1, getColors().BUTTON_NAME);*/
		
		GL11.glPushMatrix();
		GL11.glScaled(0.5D, 0.5D, 0.5D);
		drawImage(getInfo().getX() * 2 - 6 /*+ getInfo().getWidth() + 79*/, getInfo().getY() * 2, 256, 100,
				getInfo().getState() ? "/gallium/buttonOn.png" : "/gallium/buttonOff.png", 1.0F);
		GL11.glPopMatrix();
		
		ttf.drawStringWithShadow(getName().replace("_", " "), getInfo().getX(), getInfo().getY() + 4, 0xFFFFFFFF);
		/** DRAWING STOP **/
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		/** CLICKING START **/
		if(isHovering(x, y) && type == 0) {
			Gallium.utils.playSound();
			onClicked();
		}
		/** CLICKING STOP **/
	}

	public void onClicked() {
		getPanel().handleButtonPressed(this);
	};

	public boolean isHovering(int x, int y) {	
		return x >= getInfo().getX() + getInfo().getWidth() - 17 && y >= getInfo().getY() + 1 && 
				x <= getInfo().getX() + getInfo().getWidth() && y <= getInfo().getY() + getInfo().getHeight() - 1;
	}
}