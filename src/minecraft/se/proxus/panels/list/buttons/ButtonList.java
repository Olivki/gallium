package se.proxus.panels.list.buttons;

import java.util.ArrayList;

import net.minecraft.src.FontRenderer;

import se.proxus.panels.*;
import se.proxus.utils.Wrapper;

public class ButtonList extends BaseButton {

	protected boolean open;

	protected int rectHeight;

	protected ArrayList<BaseButton> buttonArray;

	public ButtonList(String name, ButtonInfo info, BasePanel panel) {
		super(name, info, panel);
		setButtonArray(new ArrayList<BaseButton>());
	}

	@Override
	public void draw(int x, int y, FontRenderer font) {
		/** COLORS START **/
		getColors().setColors(x, y);
		/** COLORS STOP **/

		/** DRAWING START **/
		if(isOpen()) {
			drawRect(getInfo().getX() - 2, getInfo().getY() + 1, getInfo().getX() + getInfo().getWidth(), getInfo().getY() 
					+ getInfo().getHeight() + getRectHeight() - 1, getColors().BUTTON_BORDER);
		}

		drawRect(getInfo().getX() - 2, getInfo().getY() + 1, getInfo().getX() 
				+ getInfo().getWidth(), getInfo().getY() + getInfo().getHeight() - 1, getColors().BUTTON_BORDER);

		font.drawStringWithShadow(getName().replace("_", " "), getInfo().getX(), getInfo().getY() + 3, 
				getColors().BUTTON_NAME);

		if(isOpen()) {
			getPanel().getInfo().setRectHeight(getPanel().getInfo().getRectHeight() + getRectHeight());
		}
		/** DRAWING STOP **/

		/** BUTTONS START **/
		handleButtons(x, y, font);
		/** BUTTONS STOP **/
	}

	@Override
	public void mouseClicked(int x, int y, int type) {
		if(isHovering(x, y) && !(isObstructed(x, y))) {
			getInfo().setState(!(getInfo().getState()));
			setOpen(getInfo().getState());
			setRectHeight((isOpen() ? getButtonArray().size() * 12 + 2 : 0));		
			Wrapper.getGallium().utils.playSound();
		} if(isOpen()) {
			for(BaseButton var3 : getButtonArray()) {
				var3.mouseClickedBefore(x, y, type);
			}
		}
	}

	public void handleButtons(int x, int y, FontRenderer font) {
		if(isOpen()) {
			for(int var3 = 0; var3 < getButtonArray().size(); var3++) {
				BaseButton var4 = (BaseButton)getButtonArray().get(var3);

				var4.getInfo().setX(getInfo().getX() + 4);
				var4.getInfo().setY(getInfo().getY() + getInfo().getHeight() + var3 * 12);
				var4.draw(x, y, font);
			}
		}
	}

	public void addButton(BaseButton button) {
		if(!(getButtonArray().contains(button))) {
			getButtonArray().add(getButtonArray().size(), button);
		}
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public int getRectHeight() {
		return rectHeight;
	}

	public void setRectHeight(int rectHeight) {
		this.rectHeight = rectHeight;
	}

	public ArrayList<BaseButton> getButtonArray() {
		return buttonArray;
	}

	public void setButtonArray(ArrayList<BaseButton> buttonArray) {
		this.buttonArray = buttonArray;
	}

	@Override
	public boolean isHovering(int x, int y) {
		return x >= getInfo().getX() - 2 && y >= getInfo().getY() && x <= getInfo().getX() + getInfo().getWidth() && y <= getInfo().getY() 
				+ getInfo().getHeight();
	}
}