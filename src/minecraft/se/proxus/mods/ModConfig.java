package se.proxus.mods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import se.proxus.*;

import net.minecraft.client.Minecraft;

public class ModConfig {

	public BaseMod mod;

	public Gallium pe;

	public File config;

	public PrintWriter configWriter;

	public BufferedReader configReader;

	public ModConfig(BaseMod mod, Gallium pe) {
		this.config = new File(Minecraft.getMinecraftDir() + "/Gallium/Mods/", mod.getName() + ".cfg");
		this.mod = mod;
		this.pe = pe;
	}

	public void saveConfig() {
		try {
			this.setupWriter();

			this.configWriter.println("Name:" + this.mod.getName());
			this.configWriter.println("Type:" + this.mod.getType().getName());
			this.configWriter.println("Key:" + this.mod.getInfo().getKey());
			this.configWriter.println("State:" + this.mod.getState());
			this.configWriter.println("Description:");

			for(int var0 = 0; var0 < this.mod.getInfo().getDescription().length; var0++) {
				this.configWriter.println(this.mod.getInfo().getDescription()[var0]);
			}

			Set set = (Set)this.mod.options.entrySet();
			Iterator it = set.iterator();

			while(it.hasNext()) {
				Map.Entry entry = (Map.Entry)it.next();

				this.configWriter.println("OP" + entry.getKey() + ":" + this.pe.utils.setFixedObject(entry.getValue()));
			}

			this.configWriter.close();
		} catch(Exception var0) {
			var0.printStackTrace();
		}
	}


	public void loadConfig() {
		try {			
			if(this.config.exists()) {
				this.setupReader();
			}

			for(String var0 = ""; (var0 = this.configReader.readLine()) != null;) {
				try {
					String[] var1 = var0.split(":");

					Set set = (Set)this.mod.options.entrySet();
					Iterator it = set.iterator();

					while(it.hasNext()) {
						Map.Entry entry = (Map.Entry)it.next();

						if(var1[0].startsWith("OP")) {
							if(Integer.parseInt(var1[0].substring(2)) == ((Integer)entry.getKey()).intValue()) {
								this.mod.setOption(Integer.parseInt(var1[0].substring(2)), this.pe.utils.getFixedObject(var1[1]), false);
							}
						}
					} if(var1[0].equalsIgnoreCase("Key")) {
						this.mod.getInfo().setKey(var1[1].toUpperCase(), false);
					} if(var1[0].equalsIgnoreCase("State")) {
						this.mod.setState(Boolean.parseBoolean(var1[1]), false);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		} catch(Exception var0) {
			var0.printStackTrace();
		}
	}


	public void setupWriter() {
		try {
			this.configWriter = new PrintWriter(new FileWriter(this.config));
		} catch(IOException var0) {
			var0.printStackTrace();
		}
	}

	public void setupReader() {
		try {
			this.configReader = new BufferedReader(new FileReader(this.config));
		} catch(IOException var0) {
			var0.printStackTrace();
		}
	}
}