package se.proxus.mods.list;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModFlight extends BaseMod {

	public ModFlight() {
		super("Flight", new ModInfo(new String[]{"Makes you fly like a blackman."}, 
				"Oliver", "NONE", false), ModType.PLAYER, false);
		getInfo().setMod(this);
		setOption(0, Double.valueOf(2.5D), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName() 
				+ " [" + getOption(0) + "]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventEntityMoved.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventEntityMoved) {
				EventEntityMoved eventEntityMoved = (EventEntityMoved)event;
				
				Wrapper.getPlayer().motionY = 0;
				Wrapper.getPlayer().fallDistance = 0;
				
				eventEntityMoved.setY(0.0D + (Wrapper.getGameSettings().keyBindJump.pressed ? 0.5D : 0.0D) 
						- (Wrapper.getGameSettings().keyBindSneak.pressed ? 0.5D : 0.0D));
				eventEntityMoved.setX(eventEntityMoved.getX() * ((Double)getOption(0)).doubleValue());
				eventEntityMoved.setZ(eventEntityMoved.getZ() * ((Double)getOption(0)).doubleValue());
			}
		}
	}
}