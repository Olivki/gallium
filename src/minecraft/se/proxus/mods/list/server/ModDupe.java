package se.proxus.mods.list.server;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.server.EventChatReceive;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModDupe extends BaseMod {

	protected EntityPlayer target;

	public ModDupe() {
		super("Dupe", new ModInfo(new String[]{"DUPING!1111"}, 
				"Oliver", "NONE", true), ModType.SERVER, false);
		getInfo().setMod(this);
		setOption(0, Long.valueOf(0L), false);
		setOption(1, Long.valueOf(2000L), false);
		setOption(2, Long.valueOf(-1L), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		getEvent().registerEvent(EventChatReceive.class);
		getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventChatReceive) {
				EventChatReceive eventChat = (EventChatReceive)event;

				if(StringUtils.stripControlCodes(eventChat.getMessage()).equalsIgnoreCase("An error has occurred. See console.")) {
					if(Wrapper.getPlayer().inventory.getCurrentItem() != null) {
						eventChat.setMessage(Colours.WHITE + "[" + Colours.RED + "Gallium" 
								+ Colours.WHITE + "]: Succesfully duped " + Colours.RED + Wrapper.getPlayer().inventory.getCurrentItem().getDisplayName() 
								+ Colours.WHITE + "!");
					}
				}
			} if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;

				setOption(0, eventUpdate.getCurrentMilliseconds(), false);

				if(Wrapper.getPlayer().inventory.getCurrentItem() != null) {
					if(Wrapper.getPlayer().inventory.getCurrentItem().stackSize == 1 && Wrapper.getPlayer().inventory.getCurrentItem().itemID == Block.bedrock.blockID) {
						if((((Long)getOption(0)).longValue() - ((Long)getOption(2)).longValue() >= ((Long)getOption(1)).longValue()
								|| ((Long)getOption(2)).longValue() == -1L)) {
							utils.sendMessage("/stack");
							setOption(2, eventUpdate.getCurrentMilliseconds(), false);
						}
					}
				}
			}
		}
	}
}