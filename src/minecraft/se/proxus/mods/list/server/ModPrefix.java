package se.proxus.mods.list.server;

import java.util.Random;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.server.EventChatSend;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModPrefix extends BaseMod {

	public ModPrefix() {
		super("Prefix", new ModInfo(new String[]{"Makes you get a nice prefix."}, 
				"Oliver", "NONE", true), ModType.SERVER, false);
		getInfo().setMod(this);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventChatSend.class);
	}

	@Override
	public void onEnabled() {
		
	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventChatSend) {
				EventChatSend eventChat = (EventChatSend)event;

				if(!(eventChat.getMessage().startsWith("/"))) {
					eventChat.setMessage("[&cOliver Burger&f]: " + eventChat.getMessage());
				}
			}
		}
	}
}