package se.proxus.mods.list.server;

import net.minecraft.src.Block;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Reflector;
import se.proxus.events.*;
import se.proxus.events.player.EventUpdate;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModMoneyMaker extends BaseMod {

	public ModMoneyMaker() {
		super("Money_Maker", new ModInfo(new String[]{"Used to gain money quickly on the NCP test server."}, 
				"Oliver", "NONE", true), ModType.SERVER, false);
		getInfo().setMod(this);
		setOption(0, Long.valueOf(0L), false);
		setOption(1, Long.valueOf(2000L), false);
		setOption(2, Long.valueOf(-1L), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;

				setOption(0, eventUpdate.getCurrentMilliseconds(), false);
				
				if((((Long)getOption(0)).longValue() - ((Long)getOption(2)).longValue() >= ((Long)getOption(1)).longValue()
						|| ((Long)getOption(2)).longValue() == -1L)) {
					utils.sendMessage("/stack");
					utils.sendMessage("/sell hand 63");
					setOption(2, eventUpdate.getCurrentMilliseconds(), false);
				}
			}
		}
	}
}