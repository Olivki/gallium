package se.proxus.mods.list.server;

import net.minecraft.src.Block;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Reflector;
import se.proxus.events.*;
import se.proxus.events.player.EventUpdate;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModRemoteView extends BaseMod {

	public ModRemoteView() {
		super("Remote_View", new ModInfo(new String[]{"Sets your view entity to another player."}, 
				"Oliver", "NONE", true), ModType.SERVER, false);
		getInfo().setMod(this);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName() + " [-]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		getInfo().setArrayName(getType().getColor() + getName() + " [-]");
		Wrapper.getMinecraft().renderViewEntity = Wrapper.getPlayer();
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				getInfo().setArrayName(getType().getColor() + getName() + " [" +  Wrapper.getMinecraft().renderViewEntity.getEntityName() + "]");

				if(Wrapper.getMinecraft().renderViewEntity != Wrapper.getPlayer())  {
					if(Wrapper.getMinecraft().renderViewEntity.isDead) {
						Wrapper.getMinecraft().renderViewEntity = Wrapper.getPlayer();
					}
				}
			}
		}
	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		super.onCommand(msg, arg);

		if(msg.length() > getName().replace("_", "").length()) {
			if(getState()) {
				if(arg[0].equalsIgnoreCase(getName().replace("_", "")) && arg[1].equalsIgnoreCase("set")) {
					if(Wrapper.getWorld().getPlayerEntityByName(arg[2]) == null) {
						utils.addMessage("Couldn't find that player.");
					} else {
						Wrapper.getMinecraft().renderViewEntity = Wrapper.getWorld().getPlayerEntityByName(arg[2]);
					}
					return true;
				}
			}
		}

		return false;
	}
}