package se.proxus.mods.list.world;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModMiner extends BaseMod {

	public ModMiner() {
		super("Miner", new ModInfo(new String[]{"Makes you mine faster."}, 
				"Oliver", "NONE", true), ModType.WORLD, false);
		getInfo().setMod(this);
		setOption(0, Float.valueOf(0.3F), false);
		setOption(1, Integer.valueOf(5), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName() 
				+ " [" + getOption(0) + ", " + getOption(1) + "]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventBlockDamaged.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(event instanceof EventBlockDamaged) {
			EventBlockDamaged eventBlockDamaged = (EventBlockDamaged)event;

			if(getState()) {
				eventBlockDamaged.setSpeed(1.0F - ((Float)getOption(0)).floatValue());
				eventBlockDamaged.setDelay(((Integer)getOption(1)).intValue());
			} else {
				eventBlockDamaged.setSpeed(1.0F);
				eventBlockDamaged.setDelay(5);
			}
		}
	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		super.onCommand(msg, arg);

		if(msg.length() > getName().replace("_", "").length()) {
			if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("speed")) {
				if(Float.parseFloat(arg[2]) > 1.0F) {
					utils.addMessage("The value has to be lower then " + Colours.RED 
							+ "1.1" + Colours.WHITE + ".");
				} else {
					setOption(0, Float.parseFloat(arg[2]), true);
					utils.addMessage(Colours.RED + getName() + " Speed" + Colours.WHITE 
							+ " has been set to " + Colours.RED 
							+ ((Float)getOption(0)).floatValue() + Colours.WHITE + ".");
				}
				return true;
			} if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("delay")) {
				if(Integer.parseInt(arg[2]) > 5) {
					utils.addMessage("The value has to be lower then " + Colours.RED 
							+ "6" + Colours.WHITE + ".");
				} else {
					setOption(1, Integer.parseInt(arg[2]), true);
					utils.addMessage(Colours.RED + getName() + " Delay" + Colours.WHITE 
							+ " has been set to " + Colours.RED 
							+ ((Integer)getOption(1)).intValue() + Colours.WHITE + ".");
				}
				return true;
			}
		}

		return false;
	}
}