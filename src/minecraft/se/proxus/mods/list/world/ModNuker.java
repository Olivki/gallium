package se.proxus.mods.list.world;

import org.lwjgl.opengl.GL11;

import se.proxus.events.Event;
import se.proxus.events.controller.EventAttackEntity;
import se.proxus.events.player.EventUpdate;
import se.proxus.events.render.EventRender3D;
import se.proxus.events.world.EventBlockClicked;
import se.proxus.mods.*;
import se.proxus.utils.*;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.Packet14BlockDig;


public class ModNuker extends ModBase {

	private Location currentBlock;

	public ModNuker() {
		super("Nuker", ModType.WORLD, true, false);
		registerSetting(0, 0, false);
		registerCommand(new ModCommand(this, "nuker", ".nuker [set] [id]", "Sets the nuker's current block id to nuke.") {
			@Override
			public void onCommand(String msg, String... args) {

			}
		});
		registerSetting(1, 0, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventRender3D.class);
		registerEvent(EventBlockClicked.class);
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onDisabled() {
		registerSetting(0, 0, true);
		setCurrentBlock(null);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventRender3D) {
				if(getCurrentBlock() != null && ((Integer)getSetting(0)) != 0) {
					double x = (int)getCurrentBlock().getX() - Client.getFixedPos(null)[0];
					double y = (int)getCurrentBlock().getY() - Client.getFixedPos(null)[1];
					double z = (int)getCurrentBlock().getZ() - Client.getFixedPos(null)[2];

					Client.enableDefaults();
					GL11.glLineWidth(2.0F);
					Client.glColor4Hex(0x5000AA00);
					Client.renderBox(AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + 1.0D, z + 1.0D));
					Client.glColor4Hex(0xFF00AA00);
					Client.renderOutlinedBox(AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + 1.0D, z + 1.0D));
					Client.disableDefaults();
				}
			} if(event instanceof EventBlockClicked) {
				if(((EventBlockClicked)event).getID() != 7 && ((EventBlockClicked)event).getID() != 0 
						&& ((EventBlockClicked)event).getID() != ((Integer)getSetting(0))) {
					registerSetting(0, ((EventBlockClicked)event).getID(), false);
					Player.addSettingsMessage("Nuker ID ", "" + ((Integer)getSetting(0)));
					setCurrentBlock(null);
				}
			} if(event instanceof EventUpdate) {
				if(getCurrentBlock() == null) {
					for(byte x = 4; x > -4; x--) {
						for(byte y = 4; y > -3; y--) {
							for(byte z = 4; z > -4; z--) {
								double nx = Player.getX() + (double)x;
								double ny = Player.getY() + (double)y;
								double nz = Player.getZ() + (double)z;
								if(Client.getWorld().getBlockId((int)nx, (int)ny, (int)nz) != 0 
										&& Client.getWorld().getBlockId((int)nx, (int)ny, (int)nz) == ((Integer)getSetting(0))
										&& Client.getPlayer().getDistance(nx, ny, nz) <= 3.5F) {
									setCurrentBlock(new Location(nx, ny, nz));
									if(Wrapper.getGallium().getMods().getMod("AutoTool").getState()) {
										Wrapper.getGallium().getMods().getMod("AutoTool").onEvent(
												new EventBlockClicked((int)nx, (int)ny, (int)nz, 0, 
														Client.getWorld().getBlockId((int)nx, (int)ny, (int)nz)));
									}
									break;
								}
							}
						}
					}
				} else if(getCurrentBlock() != null && ((Integer)getSetting(0)) != 0) {
					registerSetting(1, ((Integer)getSetting(1)) + 1, false, false);
					if((Client.getTimeFromTicks(((Integer)getSetting(1))) >= 1)) {
						Client.sendPacket(new Packet14BlockDig(0, (int)getCurrentBlock().getX(), (int)getCurrentBlock().getY(), 
								(int)getCurrentBlock().getZ(), 2));
						if((Client.getTimeFromTicks(((Integer)getSetting(1))) >= 1.5F)) {
							Client.sendPacket(new Packet14BlockDig(2, (int)getCurrentBlock().getX(), (int)getCurrentBlock().getY(), 
									(int)getCurrentBlock().getZ(), 2));
							registerSetting(1, 0, false, false);
						}
					}

					if(Client.getWorld().getBlockId((int)getCurrentBlock().getX(), (int)getCurrentBlock().getY(), 
							(int)getCurrentBlock().getZ()) == 0 || Client.getPlayer().getDistance(getCurrentBlock().getX(), 
									getCurrentBlock().getY(), getCurrentBlock().getZ()) >= 3.5F) {
						setCurrentBlock(null);
					}
				} 
			}
		}
	}

	public Location getCurrentBlock() {
		return currentBlock;
	}

	public void setCurrentBlock(Location currentBlock) {
		this.currentBlock = currentBlock;
	}
}