package se.proxus.mods.list;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModMiner extends BaseMod {

	public ModMiner() {
		super("Miner", new ModInfo(new String[]{"Makes you mine faster."}, 
				"Oliver", "NONE", true), ModType.WORLD, false);
		getInfo().setMod(this);
		setOption(0, Float.valueOf(0.3F), false);
		setOption(1, Integer.valueOf(5), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName() 
				+ " [" + getOption(0) + ", " + getOption(1) + "]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventBlockDamaged.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		((EventBlockDamaged)getEvent().getEvent(0)).setSpeed(1.0F);
		((EventBlockDamaged)getEvent().getEvent(1)).setDelay(5);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventBlockDamaged) {
				EventBlockDamaged eventBlockDamaged = (EventBlockDamaged)event;

				eventBlockDamaged.setSpeed(1.0F - ((Float)getOption(0)).floatValue());
				eventBlockDamaged.setDelay(((Integer)getOption(1)).intValue());
			}
		}
	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("speed")) {
			if(Float.parseFloat(arg[2]) > 1.0F) {
				utils.addMessage("The value has to be lower then " + Colours.YELLOW 
						+ "1.1" + Colours.WHITE + ".");
			} else {
				setOption(0, Float.parseFloat(arg[2]), true);
				utils.addMessage(Colours.YELLOW + getName() + " Speed" + Colours.WHITE 
						+ " has been set to " + Colours.YELLOW 
						+ ((Float)getOption(0)).floatValue() + Colours.WHITE + ".");
			}
			return true;
		} if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("delay")) {
			if(Integer.parseInt(arg[2]) > 5) {
				utils.addMessage("The value has to be lower then " + Colours.YELLOW 
						+ "6" + Colours.WHITE + ".");
			} else {
				setOption(1, Integer.parseInt(arg[2]), true);
				utils.addMessage(Colours.YELLOW + getName() + " Delay" + Colours.WHITE 
						+ " has been set to " + Colours.YELLOW 
						+ ((Integer)getOption(1)).intValue() + Colours.WHITE + ".");
			}
			return true;
		}

		return super.onCommand(msg, arg);
	}
}