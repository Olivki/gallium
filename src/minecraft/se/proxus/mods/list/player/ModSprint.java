package se.proxus.mods.list.player;

import se.proxus.events.Event;
import se.proxus.events.player.EventUpdate;
import se.proxus.mods.ModBase;
import se.proxus.mods.ModType;
import se.proxus.utils.*;

public class ModSprint extends ModBase {

	public ModSprint() {
		super("Sprint", ModType.PLAYER, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				if((Client.getPlayer().movementInput.moveForward > 0.0F) && !(Client.getPlayer().isSneaking())) {
					Player.setSprinting(true);
				}
			}
		}
	}
}