package se.proxus.mods.list.player;

import se.proxus.events.Event;
import se.proxus.mods.ModBase;
import se.proxus.mods.ModType;

public class ModFlight extends ModBase {

	public ModFlight() {
		super("Fly", ModType.PLAYER, false, false);
		loadSettings();
	}

	@Override
	public void init() {
		
		
	}

	@Override
	public void onEnabled() {
		
		
	}

	@Override
	public void onDisabled() {
		
		
	}

	@Override
	public void onEvent(Event event) {
		
		
	}
}