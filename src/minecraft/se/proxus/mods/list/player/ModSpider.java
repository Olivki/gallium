package se.proxus.mods.list.player;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModSpider extends BaseMod {

	protected EntityPlayer target;

	public ModSpider() {
		super("Spider", new ModInfo(new String[]{"You climb stuff."}, 
				"Oliver", "NONE", true), ModType.PLAYER, false);
		getInfo().setMod(this);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;

				if(Wrapper.getPlayer().isCollidedHorizontally) {
					Wrapper.getPlayer().motionY = 0.3D;
					Wrapper.getPlayer().onGround = true;
				}
			}
		}
	}
}