package se.proxus.mods.list.player;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModTest extends BaseMod {
	
	protected int current;
	
	protected char colour;

	public ModTest() {
		super("Test", new ModInfo(new String[]{"Debug stuff."}, 
				"Oliver", "NONE", true), ModType.PLAYER, false);
		getInfo().setMod(this);
		setOption(0, Long.valueOf(0L), false);
		setOption(1, Long.valueOf(4000L), false);
		setOption(2, Long.valueOf(-1L), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		current = 0;
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;

				setOption(0, eventUpdate.getCurrentMilliseconds(), false);

				if((((Long)getOption(0)).longValue() - ((Long)getOption(2)).longValue() >= ((Long)getOption(1)).longValue()
						|| ((Long)getOption(2)).longValue() == -1L)) {
					if(current >= 22) {
						current = 0;
					}
					
					current++;
					
					utils.sendMessage("&" + Colours.getCode(current) + "Colour &f[&c" + Colours.getCode(current) + "&f]");
					setOption(2, eventUpdate.getCurrentMilliseconds(), false);
				}
			}
		}
	}

	public char getColour() {
		return colour;
	}

	public void setColour(char colour) {
		this.colour = colour;
	}
}