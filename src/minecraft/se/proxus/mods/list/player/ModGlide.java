package se.proxus.mods.list.player;

import net.minecraft.src.Block;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Reflector;
import se.proxus.events.*;
import se.proxus.events.player.EventUpdate;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModGlide extends BaseMod {

	public ModGlide() {
		super("Glide", new ModInfo(new String[]{"Makes you slowly glide."}, 
				"Oliver", "NONE", true), ModType.PLAYER, false);
		getInfo().setMod(this);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName() 
				+ " [-]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				if(Wrapper.getPlayer().fallDistance >= 1) {
					Wrapper.getPlayer().motionY = -0.2D; 
					getInfo().setArrayName(getType().getColor() + getName() + " [" + Wrapper.getPlayer().fallDistance + "]");
				}
			}
		}
	}
}