package se.proxus.mods.list.none;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import se.proxus.events.*;
import se.proxus.events.misc.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModMacros extends BaseMod {

	protected ArrayList<ArrayHelper<String, String>> loadedMacros;

	public ModMacros() {
		super("Macros", new ModInfo(new String[]{"Macros, bitch"}, 
				"Oliver", "NONE", true), ModType.NONE, true);
		getInfo().setMod(this);
		getInfo().setToggleable(false);
		getConfig().loadConfig();
		setLoadedMacros(new ArrayList<ArrayHelper<String, String>>());
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventKeyPressed.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventKeyPressed) {
				EventKeyPressed eventKeyPressed = (EventKeyPressed)event;

				for(ArrayHelper macros : getLoadedMacros()) {
					utils.addMessage("yolo");
					if(eventKeyPressed.getKeyName().equalsIgnoreCase((String)macros.getObject()[0])) {
						utils.sendMessage((String)macros.getObject()[1]);
					}
				}
			}
		}
	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		super.onCommand(msg, arg);

		if(msg.length() > getName().replace("_", "").length()) {
			if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("add")) {
				if(!(getLoadedMacros().contains(new ArrayHelper(arg[2].toUpperCase(), arg[3])))) {
					getLoadedMacros().add(new ArrayHelper(arg[2].toUpperCase(), msg.substring(("macros add " + arg[2]).length() + 1)));
					utils.addMessage("Added the macro " + Colours.RED + arg[2].toUpperCase() + ", " 
							+ msg.substring(("macros add " + arg[2]).length() + 1) + Colours.WHITE + ".");
				}

				return true;
			} if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("remove")) {
				if(getLoadedMacros().contains(new ArrayHelper(arg[2].toUpperCase(), arg[3]))) {
					getLoadedMacros().remove(getLoadedMacros().indexOf(new ArrayHelper(arg[2].toUpperCase(), arg[3])));
					utils.addMessage("Removed the macro " + Colours.RED + arg[2].toUpperCase() + ", " 
							+ arg[3] + Colours.WHITE + ".");
				}

				return true;
			}
		}

		return false;
	}

	public ArrayList<ArrayHelper<String, String>> getLoadedMacros() {
		return loadedMacros;
	}

	public void setLoadedMacros(ArrayList<ArrayHelper<String, String>> loadedMacros) {
		this.loadedMacros = loadedMacros;
	}
}