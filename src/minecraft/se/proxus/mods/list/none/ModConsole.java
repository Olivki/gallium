package se.proxus.mods.list.none;

import se.proxus.events.Event;
import se.proxus.mods.*;
import se.proxus.screens.GuiConsole;
import se.proxus.utils.Client;
import se.proxus.utils.Wrapper;

public class ModConsole extends ModBase {

	public ModConsole() {
		super("Console", ModType.NONE, true, true);
		setKey("LCONTROL");
		loadSettings();
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onEnabled() {
		Client.getMinecraft().displayGuiScreen(new GuiConsole());
		toggle();
	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		
	}
}