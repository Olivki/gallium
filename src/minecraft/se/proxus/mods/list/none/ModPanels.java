package se.proxus.mods.list.none;

import se.proxus.events.Event;
import se.proxus.mods.*;
import se.proxus.utils.Client;
import se.proxus.utils.Wrapper;

public class ModPanels extends ModBase {

	public ModPanels() {
		super("Panels", ModType.NONE, true, true);
		setKey("RSHIFT");
		loadSettings();
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onEnabled() {
		Client.getMinecraft().displayGuiScreen(Wrapper.getGallium().getPanels());
		toggle();
	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		
	}
}