package se.proxus.mods.list.none;

import se.proxus.events.*;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModCommands extends BaseMod {

	public ModCommands() {
		super("Commands", new ModInfo(new String[]{"Handles the commands."}, 
				"Oliver", "NONE", true), ModType.NONE, true);
		getInfo().setMod(this);
		setState(true, false);
		getInfo().setToggleable(false);
		getConfig().loadConfig();
	}

	@Override
	public void initMod() {

	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {

	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		super.onCommand(msg, arg);

		if(arg[0].equalsIgnoreCase("mods")) {
			String names = "Mods[" + Colours.RED + mods.loadedMods.size() + Colours.WHITE + ":" + Colours.RED + mods.getActiveMods().size() 
					+ Colours.WHITE + "]: ";

			for(int var0 = 0; var0 < mods.loadedMods.size(); var0++) {
				BaseMod mod = (BaseMod)mods.loadedMods.get(var0);

				names += mod.getName().toLowerCase().replace("_", "") 
						+ (var0 == mods.loadedMods.size() - 1 ? "." : ", ");
			}

			utils.addMessage(names);
			return true;
		} if(arg[0].equalsIgnoreCase("cc")) {
			Wrapper.getIngameGui().getChatGUI().getChatLines().clear();
			return true;
		} if(arg[0].equalsIgnoreCase("panic")) {
			for(BaseMod mod : mods.activeMods) {
				if(mod.getInfo().isToggleable()) {
					mod.setState(false, true);
				}
			}

			return true;
		}

		return false;
	}
}