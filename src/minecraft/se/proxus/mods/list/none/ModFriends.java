package se.proxus.mods.list.none;

import java.util.ArrayList;

import net.minecraft.src.Block;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Reflector;
import net.minecraft.src.StringUtils;
import se.proxus.events.*;
import se.proxus.events.player.EventUpdate;
import se.proxus.events.render.*;
import se.proxus.events.server.EventChatReceive;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModFriends extends BaseMod {

	protected ArrayList<ArrayHelper<String, String>> loadedFriends;

	public ModFriends() {
		super("Friends", new ModInfo(new String[]{"Friends and stuff."}, 
				"Oliver", "NONE", true), ModType.NONE, true);
		getInfo().setMod(this);
		setState(true, false);
		getInfo().setToggleable(false);
		getConfig().loadConfig();
		setLoadedFriends(new ArrayList<ArrayHelper<String, String>>());
		getInfo().setArrayName(getType().getColor() + getName()
				+ " [" +  getLoadedFriends().size() + "]");
	}

	@Override
	public void initMod() {
		getEvent().registerEvent(EventChatReceive.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventChatReceive) {
				EventChatReceive eventChat = (EventChatReceive)event;

				String msg = eventChat.getMessage();

				try {
					for(int var0 = 0; var0 < getLoadedFriends().size(); var0++) {
						ArrayHelper<String, String> friend = (ArrayHelper)getLoadedFriends().get(var0);

						msg = msg.replace(((String)friend.getObject()[0]), Colours.DARK_AQUA + ((String)friend.getObject()[1]) 
								+ Colours.WHITE);
					}
				} catch(Exception e) {
					
				}

				eventChat.setMessage(msg);
			}
		}
	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		super.onCommand(msg, arg);

		if(msg.length() > getName().replace("_", "").length()) {
			if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("add")) {
				if(!(getLoadedFriends().contains(new ArrayHelper(arg[2], arg[3])))) {
					getLoadedFriends().add(new ArrayHelper(arg[2], arg[3]));
					utils.addMessage("Added the friend " + Colours.RED + arg[2] + Colours.WHITE + ", "
							+ Colours.DARK_AQUA + arg[3] + Colours.WHITE + "!");
					getInfo().setArrayName(getType().getColor() + getName()
							+ " [" +  getLoadedFriends().size() + "]");
				} else {
					utils.addMessage(Colours.RED + arg[2] + Colours.WHITE + ", "
							+ Colours.RED + arg[3] + Colours.WHITE + " is already added!");
				}

				return true;
			} if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("remove")) {
				if(isFriend(arg[2].toLowerCase())) {
					deleteByAlias(arg[2].toLowerCase());
					utils.addMessage("Removed the friend " + Colours.RED + arg[2] + Colours.WHITE + "!");
					getInfo().setArrayName(getType().getColor() + getName()
							+ " [" +  getLoadedFriends().size() + "]");
				} else {
					utils.addMessage(Colours.DARK_AQUA + arg[2] + Colours.WHITE + " isn't a friend.");
				}

				return true;
			} if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("list")) {
				String friends = "Friends: ";

				for(int var0 = 0; var0 < getLoadedFriends().size(); var0++) {
					ArrayHelper friend = (ArrayHelper)getLoadedFriends().get(var0);

					friends += Colours.RED + friend.getObject()[0] + Colours.WHITE + " > " + Colours.DARK_AQUA 
							+ friend.getObject()[1] + Colours.WHITE + (var0 == getLoadedFriends().size() - 1 ? "." : ", ");
				}

				utils.addMessage(friends);

				return true;
			} if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("clear")) {
				utils.addMessage("Cleared " + Colours.RED + getLoadedFriends().size() + Colours.WHITE + " friends.");
				getLoadedFriends().clear();
				return true;
			}
		}

		return false;
	}

	public ArrayList<ArrayHelper<String, String>> getLoadedFriends() {
		return loadedFriends;
	}

	public void setLoadedFriends(ArrayList<ArrayHelper<String, String>> loadedFriends) {
		this.loadedFriends = loadedFriends;
	}

	public boolean isFriend(String alias) {
		for(ArrayHelper friend : getLoadedFriends()) {
			if(((String)friend.getObject()[1]).equalsIgnoreCase(alias)) {
				return true;
			}
		}

		return false;
	}

	public void deleteByAlias(String alias) {
		for(ArrayHelper friend : getLoadedFriends()) {
			if(((String)friend.getObject()[1]).equalsIgnoreCase(alias)) {
				getLoadedFriends().remove(getLoadedFriends().indexOf(friend));
			}
		}
	}
}