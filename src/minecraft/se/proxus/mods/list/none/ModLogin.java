package se.proxus.mods.list.none;

import se.proxus.events.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModLogin extends ModBase {

	public ModLogin() {
		super("Login", ModType.NONE, true, true, ModController.COMMAND);
		registerCommand(new ModCommand(this, "login", ".login [username] [password]", 
				"Login you into your account.") {
			@Override
			public void onCommand(String msg, String... args) {
				try {
					if(args[0].equalsIgnoreCase("login")) {
						if(msg.contains(":")) {
							if(Client.loginToAccount(msg.split(":")[0].replace("login ", ""), msg.split(":")[1])) {
								Player.addMessage("Logged into the account!");
							} else {
								Player.addMessage("Failed to log into the account: " + msg.split(":")[0].replace("login ", ""));
							}
						} else if(Client.loginToAccount(args[1], args[2])) {
							Player.addMessage("Logged into the account!");
						} else {
							Player.addMessage("Failed to log into the account: " + args[1]);
						}
					}
				} catch(Exception exception) {
					Player.addMessage("Syntax: " + getUsage());
				}
			}
		});
	}

	@Override
	public void init() {

	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {

	}
}