package se.proxus.mods.list;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModAutoTool extends BaseMod {

	public ModAutoTool() {
		super("AutoTool", new ModInfo(new String[]{"Automagically picks the best tool."}, 
				"Oliver", "NONE", true), ModType.WORLD, false);
		getInfo().setMod(this);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventBlockClicked.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventBlockClicked) {
				EventBlockClicked eventBlockClicked = (EventBlockClicked)event;
				int id = Wrapper.getWorld().getBlockId(eventBlockClicked.getX(), 
						eventBlockClicked.getY(), eventBlockClicked.getZ());
				Block block = Block.blocksList[id];

				for(int var0 = 0; var0 < 9; var0++) {
					ItemStack item = Wrapper.getPlayer().inventory.getStackInSlot(var0);

					if(item != null) {
						if(item.getStrVsBlock(block) > (Wrapper.getPlayer().inventory.getCurrentItem() == null 
								? 1.0F : Wrapper.getPlayer().inventory.getCurrentItem().getStrVsBlock(block))) {
							Wrapper.getPlayer().inventory.currentItem = var0;
						}
					}
				}
			}
		}
	}
}