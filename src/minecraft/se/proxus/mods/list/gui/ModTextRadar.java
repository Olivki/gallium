package se.proxus.mods.list.gui;

import net.minecraft.src.Block;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Reflector;
import se.proxus.events.*;
import se.proxus.events.player.EventUpdate;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModTextRadar extends BaseMod {

	public ModTextRadar() {
		super("Text_Radar", new ModInfo(new String[]{"Used to list all the close players."}, 
				"Oliver", "NONE", true), ModType.GUI, true);
		getInfo().setMod(this);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventRender2D.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventRender2D) {
				for(int var0 = 0; var0 < Wrapper.getWorld().playerEntities.size(); var0++) {
					EntityPlayer entity = (EntityPlayer)Wrapper.getWorld().playerEntities.get(var0);
					
					if(entity != Wrapper.getPlayer()) {
						Wrapper.getFontRenderer().drawStringWithShadow(entity.username + " [" + utils.getDistance(entity, Wrapper.getPlayer())
								+ "]", 2, 2 + var0 * 10, 0xFFFFFFFF);
					}
				}
			}
		}
	}
}