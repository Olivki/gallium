package se.proxus.mods.list;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModKillAura extends BaseMod {

	public ModKillAura() {
		super("Kill_Aura", new ModInfo(new String[]{"Hits people."}, 
				"Oliver", "NONE", true), ModType.COMBAT, false);
		getInfo().setMod(this);
		setOption(0, Long.valueOf(0L), false);
		setOption(1, Long.valueOf(71L), false);
		setOption(2, Long.valueOf(-1L), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName()
				+ " [" + getOption(1) + ", 3.5]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;

				setOption(0, eventUpdate.getCurrentMilliseconds(), false);

				if(getNearestPlayer() != null && !(getNearestPlayer().isDead) 
						&& getNearestPlayer().hurtTime <= 0 
						&& Wrapper.getPlayer().canEntityBeSeen(getNearestPlayer())
						&& utils.getDistanceToEntity(getNearestPlayer(), Wrapper.getPlayer()) <= 3.5F) {
					faceEntity(getNearestPlayer());
					
					if((((Long)getOption(0)).longValue() - ((Long)getOption(2)).longValue() >= ((Long)getOption(1)).longValue() 
							|| ((Long)getOption(2)).longValue() == -1L)) {

						if(!(Wrapper.getPlayer().isSwingInProgress)) {
							utils.sendPacket(new Packet18Animation(Wrapper.getPlayer(), 1));
						}

						Wrapper.getController().attackEntity(Wrapper.getPlayer(), getNearestPlayer());
						setOption(2, eventUpdate.getCurrentMilliseconds(), false);
					}
				}
			}
		}
	}

	/**
	 * @author Ownage
	 * @return The nearest entity.
	 */
	private EntityPlayer getNearestPlayer() {
		EntityPlayer nearest = null;

		for(Object entity : Wrapper.getWorld().playerEntities) {
			if(entity != null && !(entity instanceof EntityPlayerSP)) {
				EntityPlayer e = (EntityPlayer)entity;

				if(!(e.isDead)) {
					if(nearest == null) {
						nearest = e;
					} else if(nearest.getDistanceToEntity(Wrapper.getPlayer()) 
							> e.getDistanceToEntity(Wrapper.getPlayer())) {
						nearest = e;
					}
				}
			}
		}

		return nearest;
	}

	private void faceEntity(EntityLiving entity) {
		double var0 = entity.posX - Wrapper.getX();
		double var1 = entity.posZ - Wrapper.getZ();
		double var2 = 0.0D;

		var2 = Wrapper.getY() + (double)Wrapper.getPlayer().getEyeHeight() - (entity.posY + (double)entity.getEyeHeight());

		double var3 = (double)MathHelper.sqrt_double(var0 * var0 + var1 * var1);
		float var4 = (float)(Math.atan2(var1, var0) * 180.0D / Math.PI) - 90.0F;
		float var5 = (float)((Math.atan2(var2, var3) * 180.0D) / Math.PI);
		
		Wrapper.getPlayer().rotationPitch = var5;
		Wrapper.getPlayer().rotationYaw = var4;
	}

	private float updateRotation(float var0, float var1, float var2) {
		float var3 = 0.0F;

		for(var3 = var1 - var0; var3 < -180F; var3 += 360F) { }

		for(; var3 >= 180F; var3 -= 180F) { }

		if(var3 > var2) {
			var3 = var2;
		}

		if(var3 < -var2) {
			var3 = -var2;
		}

		return var0 + var3;
	}
}