package se.proxus.mods.list;

import se.proxus.events.*;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModGui extends BaseMod {

	public ModGui() {
		super("Gui", new ModInfo(new String[]{"Renders the gui."}, "Oliver", "NONE", true), ModType.GUI, true);
		getInfo().setMod(this);
		setState(true, false);
		setOption(0, Boolean.valueOf(true), false);
		setOption(1, Boolean.valueOf(true), false);
		getConfig().loadConfig();
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventRender2D.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {

	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventRender2D) {
				Wrapper.getFontRenderer().drawStringWithShadow("Minecraft 1.4.6", 2, 2, 0xFFFFFFFF);

				if(((Boolean)getOption(0)).booleanValue()) {
					for(int var0 = 0; var0 < mods.getActiveMods().size(); var0++) {
						BaseMod mod = (BaseMod)mods.getActiveMods().get(var0);

						if(((Boolean)getOption(1)).booleanValue()) {
							Wrapper.getFontRenderer().drawStringWithShadow(mod.getInfo().getArrayName().replace("_", " "), 
									EnumPosition.RIGHT.getPosition() - Wrapper.getFontRenderer().getStringWidth(
											mod.getInfo().getArrayName().replace("_", " ")) - 1, 2 + var0 * 10, 0xFFFFFFFF); 
						} else {
							Wrapper.getFontRenderer().drawStringWithShadow(mod.getType().getColor() + mod.getName().replace("_", " "), 
									EnumPosition.RIGHT.getPosition() - Wrapper.getFontRenderer().getStringWidth(mod.getType().getColor() 
											+ mod.getName().replace("_", " ")) - 1, 2 + var0 * 10, 0xFFFFFFFF); 
						}
					}
				}
			}
		}
	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("arraylist")) {
			setOption(0, !(((Boolean)getOption(0)).booleanValue()), true);
			utils.addMessage(Colours.YELLOW + getName() + " ArrayList" + Colours.WHITE 
					+ " has been set to " + Colours.YELLOW 
					+ ((Boolean)getOption(0)).booleanValue() + Colours.WHITE + ".");
			return true;
		} if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("info")) {
			setOption(1, !(((Boolean)getOption(1)).booleanValue()), true);
			utils.addMessage(Colours.YELLOW + getName() + " Info" + Colours.WHITE 
					+ " has been set to " + Colours.YELLOW 
					+ ((Boolean)getOption(1)).booleanValue() + Colours.WHITE + ".");
			return true;
		}

		return super.onCommand(msg, arg);
	}
}