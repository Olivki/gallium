package se.proxus.mods.list.render;

import se.proxus.events.Event;
import se.proxus.mods.*;
import se.proxus.utils.Player;
import se.proxus.utils.Wrapper;

public class ModChams extends ModBase {

	public ModChams() {
		super("Chams", ModType.RENDER, true, false);
		setArrayName("Chams (Player)");
		loadSettings();
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onEnabled() {
		
	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		
	}
}