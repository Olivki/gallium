package se.proxus.mods.list.render;

import net.minecraft.src.RenderManager;

import org.lwjgl.opengl.GL11;

import se.proxus.events.*;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModNametags extends BaseMod {

	public ModNametags() {
		super("Nametags", new ModInfo(new String[]{"Renders larger nametags."}, 
				"Oliver", "NONE", true), ModType.RENDER, false);
		getInfo().setMod(this);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		
	}

	@Override
	public void onEvent(Event event) {
		
	}
}