package se.proxus.mods.list.misc;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModTimer extends BaseMod {

	public ModTimer() {
		super("Timer", new ModInfo(new String[]{"Changed the games timer speed."}, 
				"Oliver", "NONE", false), ModType.MISC, false);
		getInfo().setMod(this);
		setOption(0, Float.valueOf(20.0F), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName() 
				+ " [" + getOption(0) + "]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		Wrapper.getMinecraft().getTimer().timerSpeed = 1.0F;
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;
				
				Wrapper.getMinecraft().getTimer().timerSpeed = ((Float)getOption(0)).floatValue();
			}
		}
	}
}