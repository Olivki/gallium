package se.proxus.mods.list.misc;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModFOV extends ModBase {

	public ModFOV() {
		super("FOV", ModType.MISC, true, true);
		registerSetting(0, 1.0F, true);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {
		registerSetting(0, Client.getGameSettings().fovSetting, true);
	}

	@Override
	public void onDisabled() {
		Client.getGameSettings().fovSetting = ((Float)getSetting(0)).floatValue();
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				Client.getGameSettings().fovSetting = (Client.getPlayer().isAirBorne ? 1.6F : 1.8F);
			}
		}
	}
}