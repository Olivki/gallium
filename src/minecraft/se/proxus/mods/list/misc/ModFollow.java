package se.proxus.mods.list.misc;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModFollow extends BaseMod {

	protected EntityPlayer target;

	public ModFollow() {
		super("Follow", new ModInfo(new String[]{"Follows the closest player."}, 
				"Oliver", "NONE", true), ModType.MISC, false);
		getInfo().setMod(this);
		setOption(0, Boolean.valueOf(false), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName()
				+ " [-]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		Wrapper.getGameSettings().keyBindForward.pressed = false;
		setTarget(null);
		Wrapper.getPlayer().setSprinting(false);
		getInfo().setArrayName(getType().getColor() + getName() + " [-]");
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;

				if(!((Boolean)getOption(0)).booleanValue()) {
					if(getTarget() == null) {
						setTarget(getNearestPlayer());
					} if(!(Wrapper.getWorld().playerEntities.contains(getNearestPlayer()))) {
						setTarget(null);
						getInfo().setArrayName(getType().getColor() + getName() + " [-]");
					} if(getTarget() != null) {
						if(utils.getDistanceToEntity(getTarget(), Wrapper.getPlayer()) >= 18F) {
							setTarget(null);
							getInfo().setArrayName(getType().getColor() + getName() + " [-]");
						}
					}
				} if(getTarget() != null) {
					followPlayer(getTarget());
				}
			}
		}
	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		super.onCommand(msg, arg);

		if(msg.length() > getName().replace("_", "").length()) {
			if(arg[0].equalsIgnoreCase(getName()) && arg[1].equalsIgnoreCase("set")) {
				setOption(0, !(((Boolean)getOption(0)).booleanValue()), false);
				setTarget(Wrapper.getWorld().getPlayerEntityByName(arg[2]));
				utils.addMessage(Colours.RED + getName() + " Target" + Colours.WHITE 
						+ " has been set to " + Colours.RED 
						+ arg[2] + Colours.WHITE + ".");
				return true;
			}
		}

		return false;
	}

	private void followPlayer(EntityPlayer entity) {
		if(entity != null && !(entity.isDead) 
				&& entity.hurtTime <= 0 
				&& Wrapper.getPlayer().canEntityBeSeen(entity)
				&& utils.getDistanceToEntity(entity, Wrapper.getPlayer()) <= 15F) {
			Wrapper.getPlayer().faceEntity(entity, 10.0F, 40F);

			if(utils.getDistanceToEntity(entity, Wrapper.getPlayer()) <= 1.0F) {
				Wrapper.getGameSettings().keyBindForward.pressed = false;
				Wrapper.getPlayer().setSprinting(false);
			} if(!(utils.getDistanceToEntity(entity, Wrapper.getPlayer()) <= 1.5F)) {
				Wrapper.getGameSettings().keyBindForward.pressed = true;
				Wrapper.getPlayer().setSprinting(true);
			} else {
				Wrapper.getGameSettings().keyBindForward.pressed = false;
				Wrapper.getPlayer().setSprinting(false);
			}

			getInfo().setArrayName(getType().getColor() + getName() + " [" + entity.username + "]");
		}
	}

	/**
	 * @author Ownage
	 * @return The nearest entity.
	 */
	private EntityPlayer getNearestPlayer() {
		EntityPlayer nearest = null;

		for(Object entity : Wrapper.getWorld().playerEntities) {
			if(entity != null && !(entity instanceof EntityPlayerSP)) {
				EntityPlayer e = (EntityPlayer)entity;

				if(!(e.isDead)) {
					if(nearest == null) {
						nearest = e;
					} else if(nearest.getDistanceToEntity(Wrapper.getPlayer()) 
							> e.getDistanceToEntity(Wrapper.getPlayer())) {
						nearest = e;
					}
				}
			}
		}

		return nearest;
	}

	public EntityPlayer getTarget() {
		return target;
	}

	public void setTarget(EntityPlayer target) {
		this.target = target;
	}
}