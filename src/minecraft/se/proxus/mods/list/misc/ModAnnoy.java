package se.proxus.mods.list.misc;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModAnnoy extends BaseMod {
	
	protected EntityPlayer target;

	public ModAnnoy() {
		super("Annoy", new ModInfo(new String[]{"'Anoys' the closest player."}, 
				"Oliver", "NONE", true), ModType.MISC, false);
		getInfo().setMod(this);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName());
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		Wrapper.getGameSettings().keyBindForward.pressed = false;
		setTarget(null);
		getInfo().setArrayName(getType().getColor() + getName() + " [-]");
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;

				if(getTarget() == null) {
					setTarget(getNearestPlayer());
				} else if(!(Wrapper.getWorld().playerEntities.contains(getNearestPlayer()))) {
					setTarget(null);
					getInfo().setArrayName(getType().getColor() + getName() + " [-]");
				} if(getTarget() != null && !(getTarget().isDead) 
						&& getTarget().hurtTime <= 0 
						&& Wrapper.getPlayer().canEntityBeSeen(getTarget())
						&& utils.getDistanceToEntity(getTarget(), Wrapper.getPlayer()) <= 15F) {
					Wrapper.getPlayer().faceEntity(getTarget(), 10.0F, 40F);
					
					Wrapper.getGameSettings().keyBindForward.pressed = true;
					
					getInfo().setArrayName(getType().getColor() + getName() + " [" + getTarget().username + "]");
				}
			}
		}
	}

	/**
	 * @author Ownage
	 * @return The nearest entity.
	 */
	private EntityPlayer getNearestPlayer() {
		EntityPlayer nearest = null;

		for(Object entity : Wrapper.getWorld().playerEntities) {
			if(entity != null && !(entity instanceof EntityPlayerSP)) {
				EntityPlayer e = (EntityPlayer)entity;

				if(!(e.isDead)) {
					if(nearest == null) {
						nearest = e;
					} else if(nearest.getDistanceToEntity(Wrapper.getPlayer()) 
							> e.getDistanceToEntity(Wrapper.getPlayer())) {
						nearest = e;
					}
				}
			}
		}

		return nearest;
	}

	public EntityPlayer getTarget() {
		return target;
	}

	public void setTarget(EntityPlayer target) {
		this.target = target;
	}
}