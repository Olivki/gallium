package se.proxus.mods.list.combat;

import net.minecraft.src.*;
import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.events.render.*;
import se.proxus.events.world.*;
import se.proxus.mods.*;
import se.proxus.utils.*;

public class ModKillAura extends BaseMod {

	protected EntityPlayer target;

	public ModKillAura() {
		super("Kill_Aura", new ModInfo(new String[]{"Hits people."}, 
				"Oliver", "NONE", true), ModType.COMBAT, false);
		getInfo().setMod(this);
		setOption(0, Long.valueOf(0L), false);
		setOption(1, Long.valueOf(69L), false);
		setOption(2, Long.valueOf(-1L), false);
		setOption(3, Boolean.valueOf(true), false);
		getConfig().loadConfig();
		getInfo().setArrayName(getType().getColor() + getName()
				+ " [" + getOption(1) + ", 3.5]");
	}

	@Override
	public void initMod() {
		this.getEvent().registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		setTarget(null);
		setOption(0, Long.valueOf(0L), false);
		setOption(2, Long.valueOf(-1L), false);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				EventUpdate eventUpdate = (EventUpdate)event;

				setOption(0, eventUpdate.getCurrentMilliseconds(), false);

				if(getTarget() == null) {
					setTarget(getNearestPlayer());
				} if(!(Wrapper.getWorld().playerEntities.contains(getNearestPlayer()))) {
					setTarget(null);
				} if(getTarget() != null) {
					if(getTarget().isDead
							|| !(Wrapper.getPlayer().canEntityBeSeen(getTarget())
									|| utils.getDistanceToEntity(getTarget(), Wrapper.getPlayer()) >= 3.5F)) {
						setTarget(null);
					}
				} if(getTarget() != null && !(getTarget().isDead) 
						&& getTarget().hurtTime <= 0 
						&& Wrapper.getPlayer().canEntityBeSeen(getTarget())
						&& utils.getDistanceToEntity(getTarget(), Wrapper.getPlayer()) <= 3.5F) {
					faceEntity(getTarget());
					//Wrapper.getPlayer().faceEntity(getTarget(), 10.0F, 40F);

					if((((Long)getOption(0)).longValue() - ((Long)getOption(2)).longValue() >= ((Long)getOption(1)).longValue()
							|| ((Long)getOption(2)).longValue() == -1L)) {

						if(!(Wrapper.getPlayer().isSwingInProgress)) {
							utils.sendPacket(new Packet18Animation(Wrapper.getPlayer(), 1));
						} if(((Boolean)getOption(3)).booleanValue()) {
							if(Wrapper.getPlayer().onGround && !(Wrapper.getPlayer().isJumping)) {
								Wrapper.getPlayer().setPosition(Wrapper.getX(), Wrapper.getY() + 0.8D, Wrapper.getZ());
							}
						}

						Wrapper.getController().attackEntity(Wrapper.getPlayer(), getTarget());
						setOption(2, eventUpdate.getCurrentMilliseconds(), false);
					}
				}
			}
		}
	}

	@Override
	public boolean onCommand(String msg, String[] arg) {
		super.onCommand(msg, arg);

		if(msg.length() > getName().replace("_", "").length()) {
			if(arg[0].equalsIgnoreCase(getName().replace("_", "")) && arg[1].equalsIgnoreCase("crits")) {
				setOption(3, !(((Boolean)getOption(3)).booleanValue()), true);
				utils.addMessage(Colours.RED + getName().replace("_", " ") + " Crits" + Colours.WHITE + " has been set to " + Colours.RED 
						+ ((Boolean)getOption(3)).booleanValue() + Colours.WHITE + ".");
				getInfo().setArrayName(getType().getColor() + getName()
						+ " [" + getOption(1) + ", 3.5]");
				return true;
			} if(arg[0].equalsIgnoreCase(getName().replace("_", "")) && arg[1].equalsIgnoreCase("threshhold")) {
				setOption(1, Long.parseLong(arg[2]), true);
				utils.addMessage(Colours.RED + getName().replace("_", " ") + " Threshhold" + Colours.WHITE + " has been set to " + Colours.RED 
						+ ((Long)getOption(1)).longValue() + Colours.WHITE + ".");
				getInfo().setArrayName(getType().getColor() + getName()
						+ " [" + getOption(1) + ", 3.5]");
				return true;
			}
		}

		return false;
	}

	/**
	 * @author Ownage
	 * @return The nearest entity.
	 */
	private EntityPlayer getNearestPlayer() {
		EntityPlayer nearest = null;

		for(Object entity : Wrapper.getWorld().playerEntities) {
			if(entity != null && !(entity instanceof EntityPlayerSP)) {
				EntityPlayer e = (EntityPlayer)entity;

				if(!(e.isDead)) {
					if(nearest == null) {
						nearest = e;
					} else if(nearest.getDistanceToEntity(Wrapper.getPlayer()) 
							> e.getDistanceToEntity(Wrapper.getPlayer())) {
						nearest = e;
					}
				}
			}
		}

		return nearest;
	}

	private void faceEntity(EntityLiving entity) {
		double var0 = entity.posX - Wrapper.getX();
		double var1 = entity.posZ - Wrapper.getZ();
		double var2 = 0.0D;

		var2 = Wrapper.getY() + (double)Wrapper.getPlayer().getEyeHeight() - (entity.posY + (double)entity.getEyeHeight());

		double var3 = (double)MathHelper.sqrt_double(var0 * var0 + var1 * var1);
		float var4 = (float)(Math.atan2(var1, var0) * 180.0D / Math.PI) - 90.0F;
		float var5 = (float)((Math.atan2(var2, var3) * 180.0D) / Math.PI);

		Wrapper.getPlayer().rotationPitch = var5;
		Wrapper.getPlayer().rotationYaw = var4;
	}

	private float updateRotation(float var0, float var1, float var2) {
		float var3 = 0.0F;

		for(var3 = var1 - var0; var3 < -180F; var3 += 360F) { }

		for(; var3 >= 180F; var3 -= 180F) { }

		if(var3 > var2) {
			var3 = var2;
		}

		if(var3 < -var2) {
			var3 = -var2;
		}

		return var0 + var3;
	}

	public EntityPlayer getTarget() {
		return target;
	}

	public void setTarget(EntityPlayer target) {
		this.target = target;
	}
}