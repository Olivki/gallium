package se.proxus.mods.list.combat;

import se.proxus.events.*;
import se.proxus.events.controller.*;
import se.proxus.events.player.*;
import se.proxus.mods.*;
import se.proxus.utils.*;
import net.minecraft.src.*;


public class ModAutoAttack extends ModBase {

	public ModAutoAttack() {
		super("Auto Attack", ModType.COMBAT, true, false);
		registerSetting(0, 0L, false);
		registerSetting(1, 69L, false);
		registerSetting(2, -1L, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
		registerEvent(EventPostUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		registerSetting(0, 0L, false);
		registerSetting(2, -1L, false);
		Player.setTarget(null);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				registerSetting(0, Client.getCurrentMilliseconds(), false, false);
				if(!(Wrapper.getGallium().getMods().getMod("Aimbot").getState())) {
					((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).onUpdate();
				} if(Player.getTarget() != null) {			
					if((((Long)getSetting(0)).longValue() - ((Long)getSetting(2)).longValue() >= ((Long)getSetting(1)).longValue()
							|| ((Long)getSetting(2)).longValue() == -1L)) {
						if(!(Player.isSwinging())) {
							Client.sendPacket(new Packet18Animation(Client.getPlayer(), 1));
						} if(Wrapper.getGallium().getMods().getMod("AutoTool").getState()) {
							Wrapper.getGallium().getMods().getMod("AutoTool").onEvent(new EventAttackEntity(Player.getTarget()));
						}
						
						Player.attackEntity(Player.getTarget());
						registerSetting(2, Client.getCurrentMilliseconds(), false, false);
					}
				}
			} if(event instanceof EventPostUpdate) {
				if(!(Wrapper.getGallium().getMods().getMod("Aimbot").getState())) {
					((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).onPostUpdate();
				}
			}
		}
	}
}