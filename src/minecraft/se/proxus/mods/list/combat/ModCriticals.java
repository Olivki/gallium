package se.proxus.mods.list.combat;

import se.proxus.events.*;
import se.proxus.events.controller.*;
import se.proxus.mods.*;
import se.proxus.mods.list.combat.ModAimbot.Team;
import se.proxus.mods.list.none.ModFriend;
import se.proxus.utils.*;
import net.minecraft.src.*;


public class ModCriticals extends ModBase {

	public ModCriticals() {
		super("Criticals", ModType.COMBAT, true, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventAttackEntity.class);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventAttackEntity) {
				if(((EventAttackEntity)event).getTarget() instanceof EntityLiving) {
					if(Player.isOnGround() && !(Player.isJumping()) && !(((ModFriend)Wrapper.getGallium().getMods().getMod("Friend"))
							.isFriend(((EventAttackEntity)event).getTarget().getEntityName())) && canCrit()
							&& !(((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).getCurrentTeam() != Team.NONE
							&& ((ModAimbot)Wrapper.getGallium().getMods().getMod("Aimbot")).getCurrentTeam().isOnTeam(
									((EventAttackEntity)event).getTarget().getEntityName()))) {
						Wrapper.getPlayer().setPosition(Player.getX(), Player.getY() + 0.8D, Player.getZ());
					}
				}
			}
		}
	}
	
	public boolean canCrit() {
		int x = MathHelper.floor_double(Player.getX());
		int y = MathHelper.floor_double(Player.getY() + 1);
		int z = MathHelper.floor_double(Player.getZ());
		int id = Client.getWorld().getBlockId(x, y, z);
		return Reflector.hasMethod(50) ? Reflector.callBoolean(98, new Object[] {Block.blocksList[id], Client.getWorld(), 
				Integer.valueOf(x), Integer.valueOf(y), Integer.valueOf(z)}): id == 0;
	}
}