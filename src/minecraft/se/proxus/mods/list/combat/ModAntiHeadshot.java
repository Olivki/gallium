package se.proxus.mods.list.combat;

import se.proxus.events.*;
import se.proxus.events.player.*;
import se.proxus.mods.*;
import se.proxus.utils.*;
import net.minecraft.src.*;

public class ModAntiHeadshot extends ModBase {

	private Location lastTeleport;

	public ModAntiHeadshot() {
		super("Anti Headshot", ModType.COMBAT, true, false);
		registerSetting(0, 0, false);
		loadSettings();
	}

	@Override
	public void init() {
		registerEvent(EventUpdate.class);
	}

	@Override
	public void onEnabled() {

	}

	@Override
	public void onDisabled() {
		setLastTeleport(null);
	}

	@Override
	public void onEvent(Event event) {
		if(getState()) {
			if(event instanceof EventUpdate) {
				registerSetting(0, ((Integer)getSetting(0)) + 1, false, false);
				if(((Integer)getSetting(0)) <= 9) {
					if(getLastTeleport() != null) {
						Player.setPositionAndRotation(getLastTeleport().getX(), getLastTeleport().getY(), 
								getLastTeleport().getZ(), Player.getRotationYaw(), Player.getRotationPitch());
						return;
					}
				} for(Object o : Client.getWorld().getLoadedEntityList()) {
					if(o instanceof EntityArrow) {
						EntityArrow arrow = (EntityArrow)o;
						if(Player.getDistanceToEntity(arrow) > 5 
								&& Player.getDistanceToEntity(arrow) < 12) {
							setLastTeleport(new Location(Player.getX(), Player.getY() + 2, Player.getZ()));
							Player.setPositionAndRotation(getLastTeleport().getX(), getLastTeleport().getY(), 
									getLastTeleport().getZ(), Player.getRotationYaw(), Player.getRotationPitch());
							registerSetting(0, 0, false, false);
							break;
						}
					}
				}
			}
		}
	}

	public Location getLastTeleport() {
		return lastTeleport;
	}

	public void setLastTeleport(Location lastTeleport) {
		this.lastTeleport = lastTeleport;
	}
}