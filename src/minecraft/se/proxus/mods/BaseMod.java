package se.proxus.mods;

import java.util.ArrayList;
import java.util.HashMap;

import net.minecraft.src.*;

import se.proxus.*;
import se.proxus.events.*;
import se.proxus.utils.*;

public abstract class BaseMod extends Gallium {

	protected String name;

	protected boolean state;

	protected boolean hidden;

	protected ModType type;

	protected ModInfo info;

	protected ModConfig config;

	protected HashMap<Integer, Object> options;

	protected ModEvent event;

	public BaseMod(String name, ModInfo info, ModType type, boolean hidden) {
		setName(name);
		setType(type);
		setInfo(info);
		setHidden(hidden);
		setConfig(new ModConfig(this, Wrapper.getGallium()));
		setOptions(new HashMap<Integer, Object>());
		setEvent(new ModEvent(this));
		initMod();
	}

	public abstract void initMod();

	public void toggle() {
		if(this.getInfo().isToggleable()) {
			state =! state;

			if(state) {
				onEnable();
			} else {
				onDisable();
			}

			getConfig().saveConfig();

			utils.log("MOD", getName() + ": " + getState());
		}
	}

	public void onEnable() {
		mods.activeMods.add(this);

		onEnabled();
	}

	public void onDisable() {
		if(mods.activeMods.contains(this)) {
			mods.activeMods.remove(mods.activeMods.indexOf(this));
		}

		onDisabled();
	}

	public abstract void onEnabled();

	public abstract void onDisabled();

	public abstract void onEvent(Event event);

	public boolean onCommand(String msg, String[] arg) {
		if(this.getInfo().isToggleable()) {
			if(arg[0].equalsIgnoreCase(getName().replace("_", "")) && msg.length() == getName().replace("_", "").length()) {
				toggle();
				utils.addMessage(Colours.RED + getName().replace("_", " ") + Colours.WHITE + " has been set to " 
						+ Colours.RED + getState() + Colours.WHITE + ".");

				if(mods.getMod(28).getState()) {
					utils.sendMessage("&f[&cGallium&f]: " + Colours.RED.replace("�", "&") + getName().replace("_", " ") 
							+ Colours.WHITE.replace("�", "&") + " has been set to " + Colours.RED.replace("�", "&") + getState() 
							+ Colours.WHITE.replace("�", "&") + ".");
				}
				return true;
			} if(arg[0].equalsIgnoreCase(getName().replace("_", "")) && arg[1].equalsIgnoreCase("key")) {
				if(arg[2].equalsIgnoreCase("set")) {
					getInfo().setKey(arg[3].toUpperCase(), true);
					utils.addMessage(Colours.RED + getName().replace("_", " ") + Colours.WHITE + " has been set to " 
							+ Colours.RED + getInfo().getKey() + Colours.WHITE + ".");
					return true;
				}
			}
		}

		return false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getState() {
		return state;
	}

	public BaseMod setState(boolean state, boolean save) {
		this.state = state;

		if(save) {
			getConfig().saveConfig();
		}

		if(getState()) {
			onEnable();
		} else {
			onDisable();
		}

		return this;
	}

	public ModConfig getConfig() {
		return config;
	}

	public void setConfig(ModConfig config) {
		this.config = config;
	}

	public ModType getType() {
		return type;
	}

	public void setType(ModType type) {
		this.type = type;
	}

	public HashMap<Integer, Object> getOptions() {
		return options;
	}

	public Object getOption(int id) {
		return options.get(id);
	}

	public BaseMod setOption(int id, Object obj, boolean save) {
		options.put(id, obj);

		if(save) {
			config.saveConfig();
		}

		return this;
	}

	public ModInfo getInfo() {
		return info;
	}

	public void setInfo(ModInfo info) {
		this.info = info;
	}

	public ModEvent getEvent() {
		return event;
	}

	public void setEvent(ModEvent event) {
		this.event = event;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public void setOptions(HashMap<Integer, Object> options) {
		this.options = options;
	}
}