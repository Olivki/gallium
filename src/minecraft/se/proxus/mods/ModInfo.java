package se.proxus.mods;

import java.util.ArrayList;

import se.proxus.*;

public class ModInfo {

	protected String[] description;

	protected String author;

	protected String key;
	
	protected String arrayName;

	protected boolean nocheat;

	protected boolean toggleable;
	
	protected BaseMod mod;

	protected ArrayList<ModCommand> loadedCommands;

	public ModInfo(String[] description, String author, String key, boolean nocheat) {
		setDescription(description);
		setAuthor(author);
		setKey(key.toUpperCase());
		setNoCheat(nocheat);
		setLoadedCommands(new ArrayList<ModCommand>());
		setToggleable(true);
		setArrayName("");
	}

	public String[] getDescription() {
		return description;
	}

	public String getAuthor() {
		return author;
	}

	public String getKey() {
		return key;
	}

	public boolean getNoCheat() {
		return nocheat;
	}

	public ArrayList<ModCommand> getLoadedCommands() {
		return loadedCommands;
	}

	public void setKey(String var0) {
		this.key = var0;
	}

	public void setKey(String key, boolean save) {
		if(!(key.equalsIgnoreCase(getKey()))) {
			this.key = key;

			Gallium.utils.log("MOD", mod.getName() + " key: " + key);

			if(save) {
				mod.config.saveConfig();
			}
		}
	}

	public void setLoadedCommands(ArrayList<ModCommand> loadedCommands) {
		this.loadedCommands = loadedCommands;
	}

	public void setMod(BaseMod mod) {
		this.mod = mod;
	}

	public BaseMod getMod() {
		return mod;
	}

	public void setDescription(String[] description) {
		this.description = description;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setNoCheat(boolean nocheat) {
		this.nocheat = nocheat;
	}

	public boolean isToggleable() {
		return toggleable;
	}

	public void setToggleable(boolean toggleable) {
		this.toggleable = toggleable;
	}

	public String getArrayName() {
		return arrayName;
	}

	public void setArrayName(String arrayName) {
		this.arrayName = arrayName;
	}

	public void addCommand(ModCommand command) {
		if(!(getLoadedCommands().contains(command))) {
			getLoadedCommands().add(command);
		}
	}

	public ModCommand getCommand(String name) {
		for(ModCommand var1 : this.getLoadedCommands()) {
			if(var1.getName().equalsIgnoreCase(name)) {
				return var1;
			}
		}

		return null;
	}
}