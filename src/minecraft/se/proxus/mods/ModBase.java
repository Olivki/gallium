package se.proxus.mods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.lwjgl.input.Keyboard;

import se.proxus.events.Event;
import se.proxus.utils.Colours;
import se.proxus.utils.Player;
import se.proxus.utils.Wrapper;


public abstract class ModBase {

	protected String name;

	protected String author;

	protected String key;

	protected String arrayName;

	protected String description;

	protected boolean state;

	protected boolean hidden;

	protected boolean nocheat;

	protected ModType type;

	protected ModController controller;

	protected ModEvent event;

	protected HashMap<Integer, Object> loadedSettings;

	protected ArrayList<ModCommand> loadedCommands;

	public ModBase(String name, ModType type, boolean nocheat, boolean hidden, ModController controller) {
		setName(name);
		setKey("NONE");
		setArrayName(type.getColour() + name);
		setAuthor("Oliver Berg");
		setDescription("This mod doesn't have a description!");
		setType(type);
		setController(controller);
		setNoCheat(nocheat);
		setHidden(hidden);
		setSettings(new HashMap<Integer, Object>());
		setLoadedCommands(new ArrayList<ModCommand>());
		setEvent(new ModEvent(this));
		if(getController().equals(ModController.TOGGLE)) {
			registerCommand(new ModCommand(this, getName().replace(" ", ""), '.' + getName().replace(" ", ""),
					"Toggles the current selected mod.") {
				@Override
				public void onCommand(String msg, String... args) {
					try {
						if(msg.length() == getName().replace(" ", "").length()) {
							getMod().toggle();
							Player.addSettingsMessage(getMod().getName() + " ", "" + getMod().getState());
						} 
					} catch(Exception exception) {
						Player.addMessage("Syntax: " + getUsage());
					}
				}
			});
			registerCommand(new ModCommand(this, getName().replace(" ", ""), '.' + getName().replace(" ", "") + " [key] [bind] [keybind]",
					"Binds the selected mods keybind.") {
				@Override
				public void onCommand(String msg, String... args) {
					try {
						if(msg.length() > getName().replace(" ", "").length()) {
							if(args[1].equalsIgnoreCase("key")) {
								if(args[2].equalsIgnoreCase("bind")) {
									getMod().setKey(args[3].toUpperCase());
									Player.addSettingsMessage(getName() + " Keybind ", args[3].toUpperCase());
								}
							}
						}
					} catch(Exception exception) {
						Player.addMessage("Syntax: " + getUsage());
					}
				}
			});
			registerCommand(new ModCommand(this, getName().replace(" ", ""), '.' + getName().replace(" ", "") + " [key] [unbind]",
					"Unbinds the selected mods keybind.") {
				@Override
				public void onCommand(String msg, String... args) {
					try {
						if(msg.length() > getName().replace(" ", "").length()) {
							if(args[1].equalsIgnoreCase("key")) {
								if(args[2].equalsIgnoreCase("unbind")) {
									getMod().setKey("NONE");
									Player.addSettingsMessage(getName() + " Keybind ", "NONE");
								}
							}
						}
					} catch(Exception exception) {
						Player.addMessage("Syntax: " + getUsage());
					}
				}
			});
		}
		init();
	}

	public ModBase(String name, ModType type, boolean nocheat, boolean hidden) {
		this(name, type, nocheat, hidden, ModController.TOGGLE);
	}

	public abstract void init();

	public void onEnabled() {};

	public void onDisabled() {};

	public abstract void onEvent(Event event);

	public void toggle() {
		if(getController().equals(ModController.TOGGLE)) {
			setState(!(getState()));

			Wrapper.getGallium().getLogger().log(Level.INFO, getName() + " has been set to " + getState());

			switch(getState() ? 1 : 0) {
			case 0:
				onDisable();
				break;

			case 1:
				onEnable();
				break;
			}

			saveSettings();
		}
	}

	public void onEnable() {
		if(getController().equals(ModController.TOGGLE) && !(isHidden())) {
			Wrapper.getGallium().mods.getActiveMods().add(Wrapper.getGallium().mods.getActiveMods().size(), this);
		}

		onEnabled();
	}

	public void onDisable() {
		if(Wrapper.getGallium().mods.getActiveMods().contains(this)) {
			if(getController().equals(ModController.TOGGLE) && !(isHidden())) {
				Wrapper.getGallium().mods.getActiveMods().remove(Wrapper.getGallium().mods.getActiveMods().indexOf(this));
			}
		}

		onDisabled();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getArrayName() {
		return arrayName;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setArrayName(String arrayName) {
		this.arrayName = arrayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public boolean getNoCheat() {
		return nocheat;
	}

	public void setNoCheat(boolean nocheat) {
		this.nocheat = nocheat;
	}

	public ModType getType() {
		return type;
	}

	public void setType(ModType type) {
		this.type = type;
	}

	public ModController getController() {
		return controller;
	}

	public void setController(ModController controller) {
		this.controller = controller;
	}

	public HashMap<Integer, Object> getLoadedSettings() {
		return loadedSettings;
	}

	public void setSettings(HashMap<Integer, Object> settings) {
		this.loadedSettings = settings;
	}

	public Object getSetting(int id) {
		return getLoadedSettings().get(id);
	}

	/**
	 * Register <i>(Might change it from registerSetting to something else later on.)</i> a "setting" for the mod.
	 * The setting gets added to a HashMap <i>(With the generics: Integer, Object)</i>, which then get automagically
	 * saved to the mod's settings file.
	 * @param id The desired ID of the setting. <i>(Used for getting the setting.)</i>
	 * @param obj The "setting" you want to register.
	 * @param shouldSave If the setting should save to a the mod's settings file. 
	 *        <i>(If you're register the setting before loading the settings file, don't have this set as true.)</i>
	 * @return A instance of the referred mod.
	 * @see ModBase#registerSetting(int, Object, boolean, boolean)
	 **/
	public ModBase registerSetting(int id, Object obj, boolean shouldSave) {
		getLoadedSettings().put(id, obj);

		Wrapper.getGallium().getLogger().log(Level.INFO, "Registered the setting: " + obj + ":" + id + " for the mod: " + getName() + "!");

		if(shouldSave) {
			saveSettings();
		}

		return this;
	}

	/**
	 * Register <i>(Might change it from registerSetting to something else later on.)</i> a "setting" for the mod.
	 * The setting gets added to a HashMap <i>(With the generics: Integer, Object)</i>, which then get automagically
	 * saved to the mod's settings file.
	 * @param id The desired ID of the setting. <i>(Used for getting the setting.)</i>
	 * @param obj The "setting" you want to register.
	 * @param shouldSave If the setting should save to a the mod's settings file. 
	 *        <i>(If you're register the setting before loading the settings file, don't have this set as true.)</i>
	 * @param log If the client should log the setting each time or not. 
	 *        <i>(If you're registering a setting on each update, it's a good idea to not log, as it might cause lag issues.)</i> 
	 * @return A instance of the referred mod.
	 * @see ModBase#registerSetting(int, Object, boolean)
	 **/
	public ModBase registerSetting(int id, Object obj, boolean shouldSave, boolean log) {
		getLoadedSettings().put(id, obj);

		if(log) {
			Wrapper.getGallium().getLogger().log(Level.INFO, "Registered the setting: " + obj + ":" + id + " for the mod: " + getName() + "!");
		}

		if(shouldSave) {
			saveSettings();
		}

		return this;
	}

	public ModEvent getEvent() {
		return event;
	}

	public void setEvent(ModEvent event) {
		this.event = event;
	}

	/**
	 * Registers a event so you can use it and its function later on.
	 * @param event The desired event you want to register. <i>(It has to be a class instance of the event, example: EventUpdate.class)</i>
	 * @see ModBase#onEvent(Event)
	 **/
	public void registerEvent(Class<? extends Event> event) {
		Wrapper.getGallium().getLogger().log(Level.INFO, "Registered the event: " + event.getName() + " for the mod: " + getName() + "!");
		getEvent().registerEvent(event);
	}

	/**
	 * Registers a command for usage. <i>(Using polymorphism.)</i>
	 * @param command The desired command. <i>(Which has the arguments; ModBase mod, String command, String usage, String description)</i>
	 * @see ModCommand#onCommand(String, String...)
	 **/
	public void registerCommand(ModCommand command) {
		if(!(getLoadedCommands().contains(command))) {
			Wrapper.getGallium().getLogger().log(Level.INFO, "Registered the command: " + command.getCommand() + " for the mod: " + getName() + "!");
			getLoadedCommands().add(command);
		}
	}

	public ArrayList<ModCommand> getLoadedCommands() {
		return loadedCommands;
	}

	public void setLoadedCommands(ArrayList<ModCommand> loadedCommands) {
		this.loadedCommands = loadedCommands;
	}

	public void saveSettings() {
		try {
			File modFile = new File(Wrapper.getGallium().getModsDirectory() + "/" + getName() + ".gcfg/");
			PrintWriter settingsWriter = new PrintWriter(modFile);
			Set set = (Set)getLoadedSettings().entrySet();
			Iterator iterator = set.iterator();

			settingsWriter.println("Key:" + getKey());
			settingsWriter.println("State:" + getState());

			while(iterator.hasNext()) {
				Map.Entry entry = (Map.Entry)iterator.next();

				settingsWriter.println("SE" + entry.getKey() + ":" + setFixedObject(entry.getValue()));
			}

			settingsWriter.close();
		} catch(Exception exception) {
			exception.printStackTrace();
		}
	}

	public void loadSettings() {
		File modFile = new File(Wrapper.getGallium().getModsDirectory() + "/" + getName() + ".gcfg/");

		if(modFile.exists()) {
			try {
				BufferedReader settingsReader = new BufferedReader(new FileReader(modFile));

				try {
					for(String line = ""; (line = settingsReader.readLine()) != null;) {
						String[] split = line.split(":");
						Set set = (Set)getLoadedSettings().entrySet();
						Iterator iterator = set.iterator();

						if(split[0].equalsIgnoreCase("Key")) {
							setKey(split[1]);
						} if(split[0].equalsIgnoreCase("State")) {
							setState(Boolean.parseBoolean(split[1]));

							switch(getState() ? 1 : 0) {
							case 0:
								onDisable();
								break;

							case 1:
								onEnable();
								break;
							}
						}

						while(iterator.hasNext()) {
							Map.Entry entry = (Map.Entry)iterator.next();

							if(split[0].startsWith("SE")) {
								if(Integer.parseInt(split[0].substring(2)) == ((Integer)entry.getKey()).intValue()) {
									registerSetting(Integer.parseInt(split[0].substring(2)), getFixedObject(split[1]), false);
								}
							}
						}
					}
				} catch(Exception exception) {
					exception.printStackTrace();
				}
			} catch(Exception exception) {
				exception.printStackTrace();
			}
		} else {
			saveSettings();
		}
	}

	public Object getFixedObject(String var0) {
		Object var1 = null;

		try {
			if(var0.endsWith("F")) {
				var1 = Float.parseFloat(var0.replace("F", ""));
			} if(var0.endsWith("I")) {
				var1 = Integer.parseInt(var0.replace("I", ""));
			} if(var0.endsWith("B")) {
				var1 = Boolean.parseBoolean(var0.replace("B", ""));
			} if(var0.endsWith("L")) {
				var1 = Long.parseLong(var0.replace("L", ""));
			} if(var0.endsWith("D")) {
				var1 = Double.parseDouble(var0.replace("D", ""));
			} if(var0.endsWith("-STRING-")) {
				var1 = var0.replace("-STRING-", "");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return var1;
	}

	public String setFixedObject(Object var0) {
		String var1 = null;

		try {
			if(var0 instanceof Float) {
				var1 = var0 + "F";
			} if(var0 instanceof Integer) {
				var1 = var0 + "I";
			} if(var0 instanceof Boolean) {
				var1 = var0 + "B";
			} if(var0 instanceof Long) {
				var1 = var0 + "L";
			} if(var0 instanceof Double) {
				var1 = var0 + "D";
			} if(var0 instanceof String) {
				var1 = var0 + "-STRING-";
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return var1;
	}
}