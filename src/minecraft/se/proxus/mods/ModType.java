package se.proxus.mods;

import se.proxus.utils.*;

public enum ModType {

	COMBAT(0, "Combat", Colours.RED),
	WORLD(1, "World", Colours.GREY),
	PLAYER(2, "Player", Colours.GOLD),
	RENDER(3, "Render", Colours.YELLOW),
	SERVER(4, "Server", Colours.INDIGO),
	GUI(5, "Gui", Colours.WHITE),
	DERP(6, "Derp", Colours.PURPLE),
	MISC(7, "Misc", Colours.AQUA),
	NONE(8, "None", Colours.DARK_GREEN);
	
	private int id;
	
	private String name;
	
	private String colour;
	
	private ModType(int id, String name, String colour) {
		setId(id);
		setName(name);
		setColour(colour);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}